/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.dataaccess.database.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "Config")
@Table(name = "CONFIG")
public class DatabaseUIConfig extends DatabaseEntity {

	public static final int DEFAULT_ID = 1;

	@Id
	@Column(
		name = "ID",
		nullable = false,
		updatable = false
	)
	private int id;

	@Column(
		name = "RULE_SET",
		nullable = false
	)
	private int ruleSet;

	@Column(
		name = "DISPLAY_LANGUAGE",
		nullable = false
	)
	private int displayLanguage;

	@Column(
		name = "NATIONAL_MODE",
		nullable = false
	)
	private int nationalMode;

	@Column(
		name = "COLOR_PROFILE",
		nullable = false
	)
	private int colorProfile;

	@Column(
		name = "FONT_SIZE",
		nullable = false
	)
	private int fontSize;

	@Column(
		name = "TIMER_MODE",
		nullable = false
	)
	private int timerMode;

	@Column(
		name = "TIMER_DISPLAY_MODE",
		nullable = false
	)
	private int timerDisplayMode;

	@Column(
		name = "VOICE_LANGUAGE",
		nullable = false
	)
	private int voiceLanguage;

	@Column(
		name = "VOICE_GENDER",
		nullable = false
	)
	private int voiceGender;

	public DatabaseUIConfig() {
	}

	public DatabaseUIConfig(
		final int ruleSet,
		final int displayLanguage,
		final int nationalMode,
		final int colorProfile,
		final int fontSize,
		final int timerMode,
		final int timerDisplayMode,
		final int voiceLanguage,
		final int voiceGender
	) {
		id = DEFAULT_ID;
		this.ruleSet = ruleSet;
		this.displayLanguage = displayLanguage;
		this.nationalMode = nationalMode;
		this.colorProfile = colorProfile;
		this.fontSize = fontSize;
		this.timerMode = timerMode;
		this.timerDisplayMode = timerDisplayMode;
		this.voiceLanguage = voiceLanguage;
		this.voiceGender = voiceGender;
	}

	public int getId() {
		return id;
	}

	public int getRuleSet() {
		return ruleSet;
	}

	public int getDisplayLanguage() {
		return displayLanguage;
	}

	public int getNationalMode() {
		return nationalMode;
	}

	public int getColorProfile() {
		return colorProfile;
	}

	public int getFontSize() {
		return fontSize;
	}

	public int getTimerMode() {
		return timerMode;
	}

	public int getTimerDisplayMode() {
		return timerDisplayMode;
	}

	public int getVoiceLanguage() {
		return voiceLanguage;
	}

	public int getVoiceGender() {
		return voiceGender;
	}

	private static final String[] TITLES = {
		"ID",
		"RULE_SET",
		"DISPLAY_LANGUAGE",
		"NATIONAL_MODE",
		"COLOR_PROFILE",
		"FONT_SIZE",
		"TIMER_MODE",
		"TIMER_DISPLAY_MODE",
		"VOICE_LANGUAGE",
		"VOICE_GENDER"
	};

	public static String getTitleString() {
		return String.join(
			FIELD_SEPARATOR,
			TITLES
		);
	}

	public static String toFieldString(final DatabaseUIConfig config) {
		return String.join(
			FIELD_SEPARATOR,
			Integer.toString(config.id),
			Integer.toString(config.ruleSet),
			Integer.toString(config.displayLanguage),
			Integer.toString(config.nationalMode),
			Integer.toString(config.colorProfile),
			Integer.toString(config.fontSize),
			Integer.toString(config.timerMode),
			Integer.toString(config.timerDisplayMode),
			Integer.toString(config.voiceLanguage),
			Integer.toString(config.voiceGender)
		);
	}

	public static DatabaseUIConfig fromFieldString(final String fieldString) {
		final String fields[] = fieldString.split(
			FIELD_SEPARATOR,
			-1
		);
		return new DatabaseUIConfig(
			Integer.parseInt(fields[1]),
			Integer.parseInt(fields[2]),
			Integer.parseInt(fields[3]),
			Integer.parseInt(fields[4]),
			Integer.parseInt(fields[5]),
			Integer.parseInt(fields[6]),
			Integer.parseInt(fields[7]),
			Integer.parseInt(fields[8]),
			Integer.parseInt(fields[9])
		);
	}
}
