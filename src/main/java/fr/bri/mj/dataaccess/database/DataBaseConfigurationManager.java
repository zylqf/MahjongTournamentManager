/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.dataaccess.database;

import fr.bri.mj.dataaccess.ConfigurationManager;
import fr.bri.mj.dataaccess.database.entity.DatabaseUIConfig;
import fr.bri.mj.gui.config.UIConfig;

class DataBaseConfigurationManager implements ConfigurationManager {

	private final DataBaseDataAccess databaseDataAccess;

	DataBaseConfigurationManager(final DataBaseDataAccess databaseDataAccess) {
		this.databaseDataAccess = databaseDataAccess;
	}

	@Override
	public UIConfig getUIConfig() {
		final DatabaseUIConfig dbConfig = databaseDataAccess.getUIConfig();
		if (dbConfig != null) {
			final UIConfig config = new UIConfig(
				dbConfig.getRuleSet(),
				dbConfig.getDisplayLanguage(),
				dbConfig.getNationalMode(),
				dbConfig.getColorProfile(),
				dbConfig.getFontSize(),
				dbConfig.getTimerMode(),
				dbConfig.getTimerDisplayMode(),
				dbConfig.getVoiceLanguage(),
				dbConfig.getVoiceGender()
			);
			return config;
		} else {
			return new UIConfig();
		}
	}

	@Override
	public void saveUIConfig(final UIConfig config) {
		final DatabaseUIConfig dbConfig = new DatabaseUIConfig(
			config.getRuleSet().ordinal(),
			config.getDisplayLanguage().ordinal(),
			config.getNationalMode().ordinal(),
			config.getColorProfile().ordinal(),
			config.getFontSize().ordinal(),
			config.getTimerMode().ordinal(),
			config.getTimerDisplayMode().ordinal(),
			config.getVoiceLanguage().ordinal(),
			config.getVoiceGender().ordinal()
		);
		databaseDataAccess.saveUIConfig(dbConfig);
	}
}
