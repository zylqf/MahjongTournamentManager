/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.dataaccess.database;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import fr.bri.mj.data.player.Player;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.data.tournament.rcr.ScoreRCR;
import fr.bri.mj.data.tournament.rcr.TournamentRuleSetRCR;
import fr.bri.mj.data.tournament.rcr.TournamentScoreRCR;
import fr.bri.mj.dataaccess.PlayerManager;
import fr.bri.mj.dataaccess.TournamentManager;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentRCR;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentRCRPlanning;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentRCRPlayer;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentRCRTable;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentRCRTableId;

class DataBaseTournamentManagerRCR implements TournamentManager<TournamentScoreRCR, ScoreRCR> {

	private final DataBaseDataAccess databaseDataAccess;
	private final PlayerManager playerManager;

	private boolean tournamentCacheInitialized;
	private final SortedSet<Tournament<TournamentScoreRCR, ScoreRCR>> tournamentCacheSet;
	private final Set<Tournament<TournamentScoreRCR, ScoreRCR>> tournamentCacheSetExternal;

	DataBaseTournamentManagerRCR(
		final DataBaseDataAccess databaseDataAccess,
		final PlayerManager playerManager
	) {
		this.databaseDataAccess = databaseDataAccess;
		this.playerManager = playerManager;

		tournamentCacheInitialized = false;
		tournamentCacheSet = new TreeSet<Tournament<TournamentScoreRCR, ScoreRCR>>();
		tournamentCacheSetExternal = Collections.unmodifiableSet(tournamentCacheSet);
	}

	@Override
	public int getAvailableTournamentId() {
		initializeTournamentCache();
		int id = 1;
		if (tournamentCacheSet.size() > 0) {
			id = tournamentCacheSet.first().getId() + 1;
		}
		return id;
	}

	@Override
	public boolean addTournament(final Tournament<TournamentScoreRCR, ScoreRCR> tournament) {
		initializeTournamentCache();
		if (tournament != null && !tournamentCacheSet.contains(tournament)) {
			final DatabaseTournamentRCR dbTournament = new DatabaseTournamentRCR(
				tournament.getId(),
				tournament.getName(),
				tournament.getLeagueName(),
				tournament.getNbSessions(),
				tournament.getNbRoundRobinSessions(),
				tournament.getNbTables(),
				tournament.isWithQualification(),
				tournament.isArchived()
			);

			for (final Integer sessionId : tournament.getPlannings().keySet()) {
				final DatabaseTournamentRCRPlanning dbPlanning = new DatabaseTournamentRCRPlanning(
					dbTournament,
					sessionId,
					tournament.getPlannings().get(sessionId)
				);
				dbTournament.getPlannings().add(dbPlanning);
			}

			final Map<TournamentPlayer, DatabaseTournamentRCRPlayer> mapTournamentPlayerToDbTournamentPlayer = new HashMap<TournamentPlayer, DatabaseTournamentRCRPlayer>();
			for (final TournamentPlayer tournamentPlayer : tournament.getTournamentPlayers()) {
				final Integer playerId = tournamentPlayer.getPlayer() != null
					? tournamentPlayer.getPlayer().getId()
					: null;
				final DatabaseTournamentRCRPlayer dbTournamentPlayer = new DatabaseTournamentRCRPlayer(
					dbTournament,
					tournamentPlayer.getTournamentPlayerId(),
					playerId,
					tournamentPlayer.getTeamName()
				);
				dbTournament.getTournamentPlayers().add(dbTournamentPlayer);
				mapTournamentPlayerToDbTournamentPlayer.put(
					tournamentPlayer,
					dbTournamentPlayer
				);
			}

			for (final TournamentScoreRCR table : tournament.getTables()) {
				final DatabaseTournamentRCRTable dbTable = new DatabaseTournamentRCRTable(
					dbTournament,
					table.getSessionId(),
					table.getTableId(),
					table.getTablePlayerId(),
					mapTournamentPlayerToDbTournamentPlayer.get(table.getTournamentPlayer()),
					table.getSubstituted(),
					table.getRanking(),
					table.getGameScore(),
					table.getUmaScore(),
					table.getPenalty(),
					table.getFinalScore(),
					table.isScoreRecorded()
				);
				dbTournament.getTables().add(dbTable);
			}

			final boolean added = databaseDataAccess.addTournamentRCR(dbTournament);
			if (added) {
				tournamentCacheSet.add(tournament);
			}
			return added;
		} else {
			return false;
		}
	}

	@Override
	public Set<Tournament<TournamentScoreRCR, ScoreRCR>> getAllTournament() {
		initializeTournamentCache();
		return tournamentCacheSetExternal;
	}

	@Override
	public boolean updateTournament(final Tournament<TournamentScoreRCR, ScoreRCR> tournament) {
		if (tournament != null && tournamentCacheSet.contains(tournament)) {
			final DatabaseTournamentRCR dbTournament = databaseDataAccess.getTournamentRCR(tournament.getId());
			if (dbTournament != null) {
				final Map<Integer, TournamentPlayer> mapTournamentPlayerIdToTournamentPlayer = new HashMap<Integer, TournamentPlayer>();
				for (final TournamentPlayer tournamentPlayer : tournament.getTournamentPlayers()) {
					mapTournamentPlayerIdToTournamentPlayer.put(
						tournamentPlayer.getTournamentPlayerId(),
						tournamentPlayer
					);
				}

				final Map<Integer, DatabaseTournamentRCRPlayer> mapTournamentPlayerIdToDatabaseTournamentPlayer = new HashMap<Integer, DatabaseTournamentRCRPlayer>();
				for (final DatabaseTournamentRCRPlayer databaseTournamentPlayer : dbTournament.getTournamentPlayers()) {
					mapTournamentPlayerIdToDatabaseTournamentPlayer.put(
						databaseTournamentPlayer.getTournamentPlayerId(),
						databaseTournamentPlayer
					);
				}

				final Map<DatabaseTournamentRCRTableId, TournamentScoreRCR> mapScoreIdToTable = new HashMap<DatabaseTournamentRCRTableId, TournamentScoreRCR>();
				for (final TournamentScoreRCR table : tournament.getTables()) {
					final DatabaseTournamentRCRTableId id = new DatabaseTournamentRCRTableId(
						tournament.getId(),
						table.getSessionId(),
						table.getTableId(),
						table.getTablePlayerId()
					);
					mapScoreIdToTable.put(
						id,
						table
					);
				}

				final Map<DatabaseTournamentRCRTableId, DatabaseTournamentRCRTable> mapScoreIdToDatabaseTable = new HashMap<DatabaseTournamentRCRTableId, DatabaseTournamentRCRTable>();
				for (final DatabaseTournamentRCRTable table : dbTournament.getTables()) {
					final DatabaseTournamentRCRTableId id = new DatabaseTournamentRCRTableId(
						tournament.getId(),
						table.getSessionId(),
						table.getTableId(),
						table.getTablePlayerId()
					);
					mapScoreIdToDatabaseTable.put(
						id,
						table
					);
				}

				final Set<DatabaseTournamentRCRTableId> tableToDelete = new HashSet<DatabaseTournamentRCRTableId>(mapScoreIdToDatabaseTable.keySet());
				tableToDelete.removeAll(mapScoreIdToTable.keySet());

				final Set<DatabaseTournamentRCRTableId> tableToAdd = new HashSet<DatabaseTournamentRCRTableId>(mapScoreIdToTable.keySet());
				tableToAdd.removeAll(mapScoreIdToDatabaseTable.keySet());

				dbTournament.setName(tournament.getName());
				dbTournament.setLeagueName(tournament.getLeagueName());
				dbTournament.setArchived(tournament.isArchived());
				for (final DatabaseTournamentRCRPlanning dbPlanning : dbTournament.getPlannings()) {
					dbPlanning.setStartTime(tournament.getPlannings().get(dbPlanning.getSessionId()));
				}
				for (final DatabaseTournamentRCRPlayer dbTournamentPlayer : dbTournament.getTournamentPlayers()) {
					final TournamentPlayer tournamentPlayer = mapTournamentPlayerIdToTournamentPlayer.get(dbTournamentPlayer.getTournamentPlayerId());
					final Integer playerId = tournamentPlayer.getPlayer() != null
						? tournamentPlayer.getPlayer().getId()
						: null;
					dbTournamentPlayer.setPlayerId(playerId);
					dbTournamentPlayer.setTeamName(tournamentPlayer.getTeamName());
				}
				for (final DatabaseTournamentRCRTableId id : tableToDelete) {
					final DatabaseTournamentRCRTable databaseTable = mapScoreIdToDatabaseTable.get(id);
					databaseDataAccess.deleteTournamentRCRScore(databaseTable);
				}
				for (final DatabaseTournamentRCRTableId id : tableToAdd) {
					final TournamentScoreRCR table = mapScoreIdToTable.get(id);
					final DatabaseTournamentRCRTable dbTable = new DatabaseTournamentRCRTable(
						dbTournament,
						table.getSessionId(),
						table.getTableId(),
						table.getTablePlayerId(),
						mapTournamentPlayerIdToDatabaseTournamentPlayer.get(table.getTournamentPlayer().getTournamentPlayerId()),
						table.getSubstituted(),
						table.getRanking(),
						table.getGameScore(),
						table.getUmaScore(),
						table.getPenalty(),
						table.getFinalScore(),
						table.isScoreRecorded()
					);
					databaseDataAccess.addTournamentRCRScore(dbTable);
				}

				return databaseDataAccess.updateTournamentRCR(dbTournament);
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	@Override
	public boolean updateTournamentScore(final TournamentScoreRCR score) {
		final DatabaseTournamentRCRTableId dbTableId = new DatabaseTournamentRCRTableId(
			score.getTournament().getId(),
			score.getSessionId(),
			score.getTableId(),
			score.getTablePlayerId()
		);
		final DatabaseTournamentRCRTable dbTable = databaseDataAccess.getTournamentRCRScore(dbTableId);

		if (dbTable != null) {
			dbTable.setSubstituted(score.getSubstituted());
			dbTable.setRanking(score.getRanking());
			dbTable.setGameScore(score.getGameScore());
			dbTable.setUmaScore(score.getUmaScore());
			dbTable.setFinalScore(score.getFinalScore());
			dbTable.setPenalty(score.getPenalty());
			dbTable.setScoreRecorded(score.isScoreRecorded());
			return databaseDataAccess.updateTournamentRCRScore(dbTable);
		} else {
			return false;
		}
	}

	@Override
	public boolean deleteTournament(final Tournament<TournamentScoreRCR, ScoreRCR> tournament) {
		if (tournament != null && tournamentCacheSet.contains(tournament)) {
			final DatabaseTournamentRCR dbTournament = databaseDataAccess.getTournamentRCR(tournament.getId());
			if (dbTournament != null) {
				final boolean deleted = databaseDataAccess.deleteTournamentRCR(dbTournament);
				if (deleted) {
					tournamentCacheSet.remove(tournament);
				}
				return deleted;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	private void initializeTournamentCache() {
		if (!tournamentCacheInitialized) {
			final Map<Integer, Player> mapPlayerIdToPlayer = new HashMap<Integer, Player>();
			final Set<Player> players = playerManager.getAllPlayers();
			for (final Player player : players) {
				mapPlayerIdToPlayer.put(
					player.getId(),
					player
				);
			}

			final List<DatabaseTournamentRCR> dbTournaments = databaseDataAccess.getAllTournamentRCR();
			for (final DatabaseTournamentRCR dbTournament : dbTournaments) {
				final Tournament<TournamentScoreRCR, ScoreRCR> tournament = new TournamentRuleSetRCR(
					dbTournament.getId(),
					dbTournament.getName(),
					dbTournament.getLeagueName(),
					dbTournament.getNbSessions(),
					dbTournament.getNbRoundRobinSessions(),
					dbTournament.getNbTables(),
					dbTournament.isWithQualification(),
					dbTournament.isArchived()
				);
				for (final DatabaseTournamentRCRPlanning dbPlanning : dbTournament.getPlannings()) {
					tournament.getPlannings().put(
						dbPlanning.getSessionId(),
						dbPlanning.getStartTime()
					);
				}
				final Map<DatabaseTournamentRCRPlayer, TournamentPlayer> mapDbTournamentPlayerToTournamentPlayer = new HashMap<DatabaseTournamentRCRPlayer, TournamentPlayer>();
				for (final DatabaseTournamentRCRPlayer dbTournamentPlayer : dbTournament.getTournamentPlayers()) {
					final Player player = dbTournamentPlayer.getPlayerId() != null
						? mapPlayerIdToPlayer.get(dbTournamentPlayer.getPlayerId())
						: null;
					final TournamentPlayer tournamentPlayer = new TournamentPlayer(
						dbTournamentPlayer.getTournamentPlayerId(),
						player,
						dbTournamentPlayer.getTeamName()
					);
					tournament.getTournamentPlayers().add(tournamentPlayer);
					mapDbTournamentPlayerToTournamentPlayer.put(
						dbTournamentPlayer,
						tournamentPlayer
					);
				}
				for (final DatabaseTournamentRCRTable dbTable : dbTournament.getTables()) {
					final TournamentScoreRCR score = new TournamentScoreRCR(
						tournament,
						dbTable.getSessionId(),
						dbTable.getTableId(),
						dbTable.getTablePlayerId(),
						mapDbTournamentPlayerToTournamentPlayer.get(dbTable.getTournamentPlayer()),
						dbTable.getSubstituted(),
						dbTable.getRanking(),
						dbTable.getGameScore(),
						dbTable.getUmaScore(),
						dbTable.getPenalty(),
						dbTable.getFinalScore(),
						dbTable.isScoreRecorded()
					);
					tournament.getTables().add(score);
				}
				tournamentCacheSet.add(tournament);
			}
			tournamentCacheInitialized = true;
		}
	}
}
