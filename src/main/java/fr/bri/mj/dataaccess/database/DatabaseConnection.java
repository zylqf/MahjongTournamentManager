/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.dataaccess.database;

import java.io.File;
import java.net.URISyntaxException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.cfg.Configuration;

import fr.bri.mj.dataaccess.database.entity.DatabasePlayer;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentMCR;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentMCRPlanning;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentMCRPlayer;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentMCRTable;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentRCR;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentRCRPlanning;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentRCRPlayer;
import fr.bri.mj.dataaccess.database.entity.DatabaseTournamentRCRTable;
import fr.bri.mj.dataaccess.database.entity.DatabaseUIConfig;

abstract class DatabaseConnection {

	private static final String DATABASE_NAME = "DataBase";

	protected final File baseFolder;
	protected SessionFactory factory;
	protected Session session;

	DatabaseConnection() {
		File tempBaseFolder = null;
		try {
			tempBaseFolder = new File(getClass().getProtectionDomain().getCodeSource().getLocation().toURI()).getParentFile();
		} catch (final URISyntaxException e) {
			e.printStackTrace();
			tempBaseFolder = null;
		}
		baseFolder = tempBaseFolder;
	}

	// Connection, Disconnection
	boolean connect() {
		final File dataBaseFile = new File(
			baseFolder,
			DATABASE_NAME
		);

		final Configuration config = new Configuration();

		config.setProperty(
			AvailableSettings.DRIVER,
			"org.apache.derby.jdbc.EmbeddedDriver"
		);
		config.setProperty(
			AvailableSettings.DIALECT,
			"org.hibernate.dialect.DerbyTenSevenDialect"
		);
		if (dataBaseFile.exists()) {
			config.setProperty(
				AvailableSettings.URL,
				"jdbc:derby:"
					+ dataBaseFile.getAbsolutePath()
			);
		} else {
			config.setProperty(
				AvailableSettings.URL,
				"jdbc:derby:"
					+ dataBaseFile.getAbsolutePath()
					+ ";create=true"
			);
		}
		config.setProperty(
			AvailableSettings.USER,
			"munit"
		);
		config.setProperty(
			AvailableSettings.PASS,
			"munit"
		);
		config.setProperty(
			AvailableSettings.HBM2DDL_AUTO,
			"update"
		);

		config.setProperty(
			AvailableSettings.CONNECTION_PROVIDER,
			"org.hibernate.connection.C3P0ConnectionProvider"
		);
		config.setProperty(
			AvailableSettings.C3P0_MIN_SIZE,
			"1"
		);
		config.setProperty(
			AvailableSettings.C3P0_MAX_SIZE,
			"1"
		);

		config.addAnnotatedClass(DatabasePlayer.class);
		config.addAnnotatedClass(DatabaseTournamentMCR.class);
		config.addAnnotatedClass(DatabaseTournamentMCRPlanning.class);
		config.addAnnotatedClass(DatabaseTournamentMCRPlayer.class);
		config.addAnnotatedClass(DatabaseTournamentMCRTable.class);
		config.addAnnotatedClass(DatabaseTournamentRCR.class);
		config.addAnnotatedClass(DatabaseTournamentRCRPlanning.class);
		config.addAnnotatedClass(DatabaseTournamentRCRPlayer.class);
		config.addAnnotatedClass(DatabaseTournamentRCRTable.class);
		config.addAnnotatedClass(DatabaseUIConfig.class);

		factory = config.buildSessionFactory();
		session = factory.openSession();

		if (factory != null && session != null) {
			return true;
		} else {
			return false;
		}
	}

	void disconnect() {
		if (session != null) {
			try {
				session.close();
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
		if (factory != null) {
			try {
				factory.close();
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
	}
}
