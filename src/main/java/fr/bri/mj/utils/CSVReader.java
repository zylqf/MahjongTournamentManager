/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class CSVReader {

	private static final char[] COMMON_SEPARATORS = {
		'\t',
		',',
		';'
	};

	private enum FSMState {
		FSM_STATE_FIELD_BEGIN,
		FSM_STATE_NORMAL,
		FSM_STATE_ESCAPE,
		FSM_STATE_QUOTE,
		FSM_STATE_QUOTE_ESCAPE,
		FSM_STATE_FIELD_END,
		FSM_STATE_ERROR;
	}

	public static List<List<String>> read(
		final String filePath,
		final int expectedNumberOfColumns
	) {
		if (filePath != null) {
			try {
				return read(
					new FileInputStream(filePath),
					expectedNumberOfColumns
				);
			} catch (final Exception e) {
				return null;
			}
		} else {
			return null;
		}
	}

	public static List<List<String>> read(
		final File file,
		final int expectedNumberOfColumns
	) {
		if (file != null) {
			try {
				return read(
					new FileInputStream(file),
					expectedNumberOfColumns
				);
			} catch (final Exception e) {
				return null;
			}
		} else {
			return null;
		}
	}

	public static List<List<String>> read(
		final InputStream is,
		final int expectedNumberOfColumns
	) {
		final List<List<String>> data = new ArrayList<List<String>>();
		final BufferedReader reader = new BufferedReader(
			new InputStreamReader(
				is,
				StandardCharsets.UTF_8
			)
		);
		try {
			String line = reader.readLine();
			char separator = 0;
			if (line != null) {
				for (int separatorIndex = 0; separatorIndex < COMMON_SEPARATORS.length; separatorIndex++) {
					int nbColumns = 1;
					int lastSeparatorIndex = -1;
					int nextSeparatorIndex = 0;
					while (nextSeparatorIndex >= 0) {
						nextSeparatorIndex = line.indexOf(
							COMMON_SEPARATORS[separatorIndex],
							lastSeparatorIndex + 1
						);
						if (nextSeparatorIndex >= 0) {
							lastSeparatorIndex = nextSeparatorIndex;
							nbColumns++;
						}
					}
					if (nbColumns == expectedNumberOfColumns) {
						separator = COMMON_SEPARATORS[separatorIndex];
						break;
					}
				}
			}
			if (separator != 0) {
				while (line != null) {
					if (line.length() > 0) {
						final List<String> oneLineData = new ArrayList<String>();
						FSMState state = FSMState.FSM_STATE_FIELD_BEGIN;
						StringBuffer buffer = new StringBuffer();
						for (int index = 0; state != FSMState.FSM_STATE_ERROR && index < line.length(); index++) {
							final char c = line.charAt(index);
							switch (state) {
								case FSM_STATE_FIELD_BEGIN:
									if (c == '"') { // Enter quote stat
										state = FSMState.FSM_STATE_QUOTE;
									} else if (c == separator) { // New field
										oneLineData.add(buffer.toString());
										buffer = new StringBuffer();
									} else {
										state = FSMState.FSM_STATE_NORMAL;
										buffer.append(c);
									}
									break;
								case FSM_STATE_NORMAL:
									if (c == '"') { // Enter error state
										state = FSMState.FSM_STATE_ERROR;
									} else if (c == '\\') { // Enter escape state
										state = FSMState.FSM_STATE_ESCAPE;
									} else if (c == separator) { // New field
										state = FSMState.FSM_STATE_FIELD_BEGIN;
										oneLineData.add(buffer.toString());
										buffer = new StringBuffer();
									} else {
										buffer.append(c);
									}
									break;
								case FSM_STATE_ESCAPE:
									state = FSMState.FSM_STATE_NORMAL;
									if (c == 't') {
										buffer.append('\t');
									} else if (c == 'b') {
										buffer.append('\b');
									} else if (c == 'n') {
										buffer.append('\n');
									} else if (c == 'r') {
										buffer.append('\r');
									} else if (c == 'f') {
										buffer.append('\f');
									} else {
										buffer.append(c);
									}
									break;
								case FSM_STATE_QUOTE:
									if (c == '"') { // Enter error state
										state = FSMState.FSM_STATE_FIELD_END;
									} else if (c == '\\') { // Enter escape state
										state = FSMState.FSM_STATE_QUOTE_ESCAPE;
									} else {
										buffer.append(c);
									}
									break;
								case FSM_STATE_QUOTE_ESCAPE:
									state = FSMState.FSM_STATE_QUOTE;
									if (c == 't') {
										buffer.append('\t');
									} else if (c == 'b') {
										buffer.append('\b');
									} else if (c == 'n') {
										buffer.append('\n');
									} else if (c == 'r') {
										buffer.append('\r');
									} else if (c == 'f') {
										buffer.append('\f');
									} else {
										buffer.append(c);
									}
									break;
								case FSM_STATE_FIELD_END:
									if (c == separator) { // New field
										oneLineData.add(buffer.toString());
										buffer = new StringBuffer();
										state = FSMState.FSM_STATE_FIELD_BEGIN;
									} else {
										state = FSMState.FSM_STATE_ERROR;
									}
									break;
								case FSM_STATE_ERROR:
									break;
							}
						}
						if (state == FSMState.FSM_STATE_NORMAL || state == FSMState.FSM_STATE_FIELD_BEGIN || state == FSMState.FSM_STATE_FIELD_END) {
							oneLineData.add(buffer.toString());
							data.add(oneLineData);
						}
					}
					line = reader.readLine();
				}
			}
		} catch (final Exception e) {
			e.printStackTrace();
			data.clear();
		} finally {
			try {
				reader.close();
			} catch (final IOException e) {
			}
		}
		return data;
	}
}
