/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

public class CSVWriter {

	private static final char SEPARATOR = '\t';

	public static boolean write(
		final String filePath,
		final String[][] data,
		final String[] header
	) {
		if (filePath != null) {
			try {
				return write(
					new FileOutputStream(filePath),
					data,
					header
				);
			} catch (final Exception e) {
				return false;
			}
		} else {
			return false;
		}
	}

	public static boolean write(
		final File file,
		final String[][] data,
		final String[] header
	) {
		if (file != null) {
			try {
				return write(
					new FileOutputStream(file),
					data,
					header
				);
			} catch (final Exception e) {
				return false;
			}
		} else {
			return false;
		}
	}

	public static boolean write(
		final OutputStream os,
		final String[][] data,
		final String[] header
	) {
		if (data != null && data.length > 0 && os != null) {
			final BufferedWriter writer = new BufferedWriter(
				new OutputStreamWriter(
					os,
					StandardCharsets.UTF_8
				)
			);
			try {
				if (header != null && header.length > 0) {
					String oneHeader = header[0];
					if (oneHeader != null) {
						writer.write(oneHeader);
					}
					for (int headerIndex = 1; headerIndex < header.length; headerIndex++) {
						writer.write(SEPARATOR);
						oneHeader = header[headerIndex];
						if (oneHeader != null) {
							writer.write(oneHeader);
						}
					}
					writer.newLine();
				}

				for (int dataIndex = 0; dataIndex < data.length; dataIndex++) {
					final String[] oneLine = data[dataIndex];
					if (oneLine != null && oneLine.length > 0) {
						String oneData = oneLine[0];
						if (oneData != null) {
							writer.write(oneData);
						}
						for (int fieldIndex = 1; fieldIndex < oneLine.length; fieldIndex++) {
							writer.write(SEPARATOR);
							oneData = oneLine[fieldIndex];
							if (oneData != null) {
								writer.write(oneData);
							}
						}
						writer.newLine();
					}
				}
				return true;
			} catch (final Exception e) {
				e.printStackTrace();
				return false;
			} finally {
				try {
					writer.close();
				} catch (final IOException e) {
				}
			}
		} else {
			return false;
		}
	}
}
