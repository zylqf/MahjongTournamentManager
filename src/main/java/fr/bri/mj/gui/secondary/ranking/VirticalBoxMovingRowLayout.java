/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.ranking;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class VirticalBoxMovingRowLayout implements LayoutManager2 {

	private final Container container;
	private final int rows;
	private final int componentTopLeftY[];
	private final int targetComponentTopLeftY[];
	private final SortedMap<Integer, Component> mapIndexComponent;
	private final SortedMap<Integer, Component> targetMapIndexComponent;
	private final Map<Component, Integer> mapComponentIndex;
	private final Map<Component, Integer> targetMapComponentIndex;

	private boolean inUpdate;
	private boolean inTransition;

	public VirticalBoxMovingRowLayout(
		final Container container,
		final int rows
	) {
		this.container = container;
		this.rows = rows;
		componentTopLeftY = new int[rows];
		targetComponentTopLeftY = new int[rows];
		mapIndexComponent = new TreeMap<Integer, Component>();
		targetMapIndexComponent = new TreeMap<Integer, Component>();
		mapComponentIndex = new HashMap<Component, Integer>();
		targetMapComponentIndex = new HashMap<Component, Integer>();
		inUpdate = false;
		inTransition = false;
	}

	@Override
	public void addLayoutComponent(
		final String name,
		final Component comp
	) {
	}

	@Override
	public void addLayoutComponent(
		final Component comp,
		final Object constraints
	) {
		if (comp != null && constraints != null && constraints instanceof Integer) {
			final Integer c = (Integer) constraints;
			if (c < 0 || c >= rows) {
				throw new IllegalArgumentException("Invalid Row value");
			}

			if (mapIndexComponent.containsKey(c) || mapComponentIndex.containsKey(comp)) {
				mapIndexComponent.remove(c);
				mapComponentIndex.remove(comp);
			}
			mapIndexComponent.put(
				c,
				comp
			);
			mapComponentIndex.put(
				comp,
				c
			);
		} else {
			throw new IllegalArgumentException("Cannot add to layout: constraints must be a Integer");
		}
	}

	@Override
	public void removeLayoutComponent(final Component comp) {
		if (comp != null && mapComponentIndex.containsKey(comp)) {
			final Integer c = mapComponentIndex.get(comp);
			mapIndexComponent.remove(c);
			mapComponentIndex.remove(comp);
		}
	}

	@Override
	public Dimension minimumLayoutSize(final Container parent) {
		int minWidth = 0;
		int height = 0;
		for (final Component component : mapComponentIndex.keySet()) {
			if (component != null) {
				final Dimension minSize = component.getMinimumSize();
				if (minSize.width > minWidth) {
					minWidth = minSize.width;
				}
				height += minSize.height;
			}
		}
		return new Dimension(
			minWidth,
			height
		);
	}

	@Override
	public Dimension maximumLayoutSize(final Container target) {
		return new Dimension(
			Integer.MAX_VALUE,
			Integer.MAX_VALUE
		);
	}

	@Override
	public Dimension preferredLayoutSize(final Container parent) {
		int minWidth = 0;
		int height = 0;
		for (final Component component : mapComponentIndex.keySet()) {
			if (component != null) {
				final Dimension minSize = component.getPreferredSize();
				if (minSize.width > minWidth) {
					minWidth = minSize.width;
				}
				height += minSize.height;
			}
		}
		return new Dimension(
			minWidth,
			height
		);
	}

	@Override
	public void layoutContainer(final Container parent) {
		synchronized (container.getTreeLock()) {
			final Insets insets = container.getInsets();
			final int usableWidth = container.getWidth() - (insets.left + insets.right);

			int topLeftY = insets.top;
			int lastIndex = 0;
			for (final Integer index : mapIndexComponent.keySet()) {
				while (lastIndex < index) {
					componentTopLeftY[lastIndex] = topLeftY;
					lastIndex++;
				}

				componentTopLeftY[index] = topLeftY;
				topLeftY += mapIndexComponent.get(index).getPreferredSize().height;
				lastIndex = index + 1;
			}

			for (final Integer index : mapIndexComponent.keySet()) {
				final Component comp = mapIndexComponent.get(index);
				comp.setBounds(
					insets.left,
					componentTopLeftY[index],
					usableWidth,
					comp.getPreferredSize().height
				);
			}
		}
	}

	@Override
	public float getLayoutAlignmentX(final Container target) {
		return 0.5f;
	}

	@Override
	public float getLayoutAlignmentY(final Container target) {
		return 0.5f;
	}

	@Override
	public void invalidateLayout(final Container target) {
	}

	public void startUpdate() {
		inUpdate = true;
		inTransition = false;
		targetMapIndexComponent.clear();
		targetMapComponentIndex.clear();
	}

	public void updateRow(
		final Component comp,
		final Integer row
	) {

		if (inUpdate && comp != null && row != null) {
			if (row < 0 || row >= rows) {
				throw new IllegalArgumentException("Invalid Row value");
			}

			if (targetMapIndexComponent.containsKey(row)) {
				targetMapComponentIndex.remove(targetMapIndexComponent.get(row));
				targetMapIndexComponent.remove(row);
			}

			if (targetMapComponentIndex.containsKey(comp)) {
				targetMapIndexComponent.remove(targetMapComponentIndex.get(comp));
				targetMapComponentIndex.remove(comp);
			}

			targetMapIndexComponent.put(
				row,
				comp
			);
			targetMapComponentIndex.put(
				comp,
				row
			);
		}
	}

	public void startTransition() {
		if (targetMapIndexComponent.size() != mapIndexComponent.size()) {
			throw new RuntimeException("Must update all component before start transition");
		}
		inUpdate = false;
		inTransition = true;

		final Insets insets = container.getInsets();
		int topLeftY = insets.top;
		int lastIndex = 0;
		for (final Integer index : targetMapIndexComponent.keySet()) {
			while (lastIndex < index) {
				targetComponentTopLeftY[lastIndex] = topLeftY;
				lastIndex++;
			}

			targetComponentTopLeftY[index] = topLeftY;
			topLeftY += targetMapIndexComponent.get(index).getPreferredSize().height;
			lastIndex = index + 1;
		}
	}

	public void setProgress(final double progress) {
		if (inTransition && progress >= 0.0 && progress <= 1.0) {
			final Insets insets = container.getInsets();
			final int usableWidth = container.getWidth() - (insets.left + insets.right);
			for (final Integer index : mapIndexComponent.keySet()) {
				final Component comp = mapIndexComponent.get(index);
				final Integer targetIndex = targetMapComponentIndex.get(comp);
				comp.setBounds(
					insets.left,
					(int) (componentTopLeftY[index] + (targetComponentTopLeftY[targetIndex] - componentTopLeftY[index]) * progress),
					usableWidth,
					comp.getPreferredSize().height
				);
			}
		}
	}

	public void finishTransition() {
		setProgress(1.0);
		System.arraycopy(
			targetComponentTopLeftY,
			0,
			componentTopLeftY,
			0,
			rows
		);
		mapIndexComponent.clear();
		mapIndexComponent.putAll(targetMapIndexComponent);
		mapComponentIndex.clear();
		mapComponentIndex.putAll(targetMapComponentIndex);

		inUpdate = false;
		inTransition = false;
	}
}
