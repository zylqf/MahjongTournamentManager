/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.league.rcr;

import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.multi.RankingScoreMulti;
import fr.bri.mj.data.tournament.rcr.ScoreRCR;
import fr.bri.mj.data.tournament.rcr.TournamentScoreRCR;
import fr.bri.mj.gui.config.item.UIConfigNationalMode;
import fr.bri.mj.gui.icon.IconLoaderCountry;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.gui.secondary.league.UILeagueChartRow;
import fr.bri.mj.gui.secondary.league.UILeagueColorProfile;
import fr.bri.mj.gui.secondary.ranking.UIRankingMode;

public class UILeagueChartRowRCR extends UILeagueChartRow<TournamentScoreRCR, ScoreRCR> {

	private static final long serialVersionUID = -8255116736293853878L;

	private static final int SCORE_DIGITS = 6;
	private static final int SCORE_GROUP_WIDTH = 3;
	private static final char SCORE_GROUP_SEPARATOR = ' ';

	private final UIRankingMode<ScoreRCR> rankingMode;
	private final UIConfigNationalMode nationalMode;
	private final UILeagueColorProfile colorProfile;
	private final int fontSize;

	private final JLabel labelRanking;
	private final JLabel labelFlag;
	private final JLabel labelPlayerId;
	private final JLabel labelPlayerName;
	private final JPanel panelScore;
	private final JLabel labelScore1;
	private final JLabel labelScore2;

	public UILeagueChartRowRCR(
		final UIRankingMode<ScoreRCR> rankingMode,
		final UIConfigNationalMode nationalMode,
		final UILeagueColorProfile colorProfile,
		final Font labelFont,
		final int fontSize,
		final UITextTranslator translator
	) {
		super(translator);
		this.rankingMode = rankingMode;
		this.nationalMode = nationalMode;
		this.colorProfile = colorProfile;
		this.fontSize = fontSize;

		setOpaque(false);

		final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
			1,
			7,
			0,
			0
		);
		layout.setWeightX(
			2,
			3,
			3,
			3,
			10,
			14,
			2
		);
		setLayout(layout);

		final ProportionalGridLayoutConstraint constraints = new ProportionalGridLayoutConstraint(
			0,
			1,
			0,
			1
		);

		{
			constraints.y = 0;
			constraints.x = 1;
			labelRanking = new JLabel(
				"",
				SwingConstants.CENTER
			);
			labelRanking.setFont(labelFont);
			labelRanking.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			labelRanking.setBackground(colorProfile.getDarkenLineBackgroundColor());
			add(
				labelRanking,
				constraints
			);
		}
		{
			constraints.y = 0;
			constraints.x = 2;
			labelFlag = new JLabel(
				"",
				SwingConstants.CENTER
			);
			labelFlag.setFont(labelFont);
			labelFlag.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			labelFlag.setBackground(colorProfile.getDarkenLineBackgroundColor());
			add(
				labelFlag,
				constraints
			);
		}
		{
			constraints.y = 0;
			constraints.x = 3;
			labelPlayerId = new JLabel(
				"",
				SwingConstants.CENTER
			);
			labelPlayerId.setFont(labelFont);
			labelPlayerId.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			labelPlayerId.setBackground(colorProfile.getDarkenLineBackgroundColor());
			add(
				labelPlayerId,
				constraints
			);
		}
		{
			constraints.y = 0;
			constraints.x = 4;
			labelPlayerName = new JLabel(
				"",
				SwingConstants.LEADING
			);
			labelPlayerName.setFont(labelFont);
			labelPlayerName.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			labelPlayerName.setBackground(colorProfile.getDarkenLineBackgroundColor());
			add(
				labelPlayerName,
				constraints
			);
		}
		{
			constraints.y = 0;
			constraints.x = 5;
			panelScore = new JPanel();
			panelScore.setBackground(colorProfile.getDarkenLineBackgroundColor());

			labelScore1 = new JLabel(
				"",
				SwingConstants.CENTER
			);
			labelScore1.setFont(labelFont);

			labelScore2 = new JLabel(
				"",
				SwingConstants.CENTER
			);
			labelScore2.setFont(labelFont);

			final ProportionalGridLayoutConstraint panelScoreLayoutConstraints = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			switch (rankingMode.getScore()) {
				case FINAL_SCORE: {
					final ProportionalGridLayoutInteger panelScoreLayout = new ProportionalGridLayoutInteger(
						1,
						1,
						0,
						2
					);
					panelScoreLayout.setWeightX(1);
					panelScore.setLayout(panelScoreLayout);

					panelScoreLayoutConstraints.y = 0;
					panelScoreLayoutConstraints.x = 0;
					panelScoreLayoutConstraints.gridWidth = 1;
					panelScore.add(
						labelScore1,
						panelScoreLayoutConstraints
					);
				}
					break;
				case GAME_SCORE:
				case UMA_SCORE: {
					final ProportionalGridLayoutInteger panelScoreLayout = new ProportionalGridLayoutInteger(
						1,
						2,
						0,
						2
					);
					panelScore.setLayout(panelScoreLayout);

					panelScoreLayoutConstraints.y = 0;
					panelScoreLayoutConstraints.x = 0;
					panelScore.add(
						labelScore1,
						panelScoreLayoutConstraints
					);
					panelScoreLayoutConstraints.x = 1;
					panelScore.add(
						labelScore2,
						panelScoreLayoutConstraints
					);
				}
					break;
			}

			add(
				panelScore,
				constraints
			);
		}
	}

	@Override
	public void setScore(
		final RankingScoreMulti score,
		final int average,
		final boolean darken
	) {
		labelRanking.setText(Integer.toString(score.getRanking()));

		switch (rankingMode.getScore()) {
			case FINAL_SCORE:
			case GAME_SCORE:
			case UMA_SCORE:
				switch (nationalMode) {
					case NATIONALITY:
						if (score.getPlayer() != null) {
							final Image iconImage = IconLoaderCountry.getIconImageCountry(
								score.getPlayer().getPlayerNationality(),
								fontSize
							);
							labelFlag.setIcon(new ImageIcon(iconImage));
						} else {
							labelFlag.setIcon(null);
						}
						labelFlag.setText("");
						break;
					case CLUB_NAME:
						if (score.getPlayer() != null) {
							labelFlag.setText(score.getPlayer().getClub());
						} else {
							labelFlag.setText("");
						}
						labelFlag.setIcon(null);
						break;
				}
				break;
		}

		String playerName = null;
		switch (rankingMode.getScore()) {
			case FINAL_SCORE:
			case GAME_SCORE:
			case UMA_SCORE:
				playerName = score.getName(translator.translate(UIText.GUI_SECONDARY_LEAGUE_TITLE_PLAYER_EMPTY));
				break;
		}
		labelPlayerName.setText(playerName);

		switch (rankingMode.getScore()) {
			case FINAL_SCORE:
				labelScore1.setText(
					format(
						score.getTotalScore(),
						SCORE_DIGITS,
						SCORE_GROUP_WIDTH,
						SCORE_GROUP_SEPARATOR
					)
				);
				break;
			case GAME_SCORE:
			case UMA_SCORE:
				labelScore1.setText(
					format(
						score.getTotalScore(),
						SCORE_DIGITS,
						SCORE_GROUP_WIDTH,
						SCORE_GROUP_SEPARATOR
					)
				);
				labelScore2.setText(
					format(
						score.getTotalScore2(),
						SCORE_DIGITS,
						SCORE_GROUP_WIDTH,
						SCORE_GROUP_SEPARATOR
					)
				);
				break;
			default:
				break;
		}

		if (score.isUpToDate()) {
			if (score.getTotalScore() >= average) {
				labelScore1.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
				labelScore2.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			} else {
				labelScore1.setForeground(colorProfile.getUpToDateNegativeScoreFontColor());
				labelScore2.setForeground(colorProfile.getUpToDateNegativeScoreFontColor());
			}
		} else {
			if (score.getTotalScore() >= average) {
				labelScore1.setForeground(colorProfile.getDelayedPositiveScoreFontColor());
				labelScore2.setForeground(colorProfile.getDelayedPositiveScoreFontColor());
			} else {
				labelScore1.setForeground(colorProfile.getDelayedNegativeScoreFontColor());
				labelScore2.setForeground(colorProfile.getDelayedNegativeScoreFontColor());
			}
		}

		labelRanking.setOpaque(darken);
		labelFlag.setOpaque(darken);
		labelPlayerId.setOpaque(darken);
		labelPlayerName.setOpaque(darken);
		panelScore.setOpaque(darken);
	}

	private String format(
		final int number,
		final int width,
		final int groupWidth,
		final char groupSeparator
	) {
		final StringBuilder builder = new StringBuilder();
		if (number >= 0) {
			builder.append(' ');
		} else {
			builder.append('-');
		}

		final String raw = Integer.toString(Math.abs(number));
		final int rawWidth = raw.length();
		final int nbGroupSeparator = (width - 1) / groupWidth;
		final int nbRawGroupSeparator = (rawWidth - 1) / groupWidth;
		final int paddingWidth = width - rawWidth + nbGroupSeparator - nbRawGroupSeparator;
		for (int index = 0; index < paddingWidth; index++) {
			builder.append(' ');
		}

		for (int index = 0; index < rawWidth; index++) {
			builder.append(raw.charAt(index));
			final int numberIndex = rawWidth - 1 - index;
			if (numberIndex != 0 && numberIndex % groupWidth == 0) {
				builder.append(groupSeparator);
			}
		}
		return builder.toString();
	}
}
