/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.plan;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.data.tournament.TournamentScore;
import fr.bri.mj.dataaccess.TournamentManager;
import fr.bri.mj.gui.common.UITabPanel;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.dialog.UIMessageDialog;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogMessageType;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOption;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOptionsType;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.printing.PaperSizeType;
import fr.bri.mj.printing.PlanPrinter;

public abstract class UITabPanelDisplayPlan<ScoreClassType extends TournamentScore<ScoreClassType, ScoreEnumType>, ScoreEnumType extends Enum<ScoreEnumType>> extends UITabPanel {

	private static final long serialVersionUID = 7744810131498318930L;

	protected static final int LABEL_PADDING = 10;

	private final UIText PLACE[] = {
		UIText.GUI_SECONDARY_PLAN_TITLE_PLAYER_EAST,
		UIText.GUI_SECONDARY_PLAN_TITLE_PLAYER_SOUTH,
		UIText.GUI_SECONDARY_PLAN_TITLE_PLAYER_WEST,
		UIText.GUI_SECONDARY_PLAN_TITLE_PLAYER_NORTH
	};

	private final TournamentManager<ScoreClassType, ScoreEnumType> tournamentManager;

	private final JComboBox<String> comboTournament;
	private final JComboBox<String> comboSession;

	private final ActionListener tournamentComboBoxActionListener;
	private final ActionListener sessionComboBoxActionListener;

	private final JPanel panelCenterSupport;
	private final JPanel panelPlan;
	private final JScrollPane scrollRanking;

	private final JButton buttonExportScoreSheet;
	private final JButton buttonExportPlan;

	private final List<Tournament<ScoreClassType, ScoreEnumType>> listTournament;

	protected UITabPanelDisplayPlan(
		final TournamentManager<ScoreClassType, ScoreEnumType> tournamentManager,
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			config,
			translator
		);
		this.tournamentManager = tournamentManager;

		setLayout(new BorderLayout());
		{
			final JPanel panelNorth = new JPanel();
			final ProportionalGridLayoutInteger northLayout = new ProportionalGridLayoutInteger(
				NORTH_PANEL_ROWS_NUMBER,
				NORTH_PANEL_COLUMNS_NUMBER,
				NORTH_PANEL_HORIZONTAL_GAP,
				NORTH_PANEL_VERTICAL_GAP
			);
			northLayout.setWeightX(
				NORTH_PANEL_TOURNAMENT_TITLE_WEIGHT,
				NORTH_PANEL_TOURNAMENT_COMBOBOX_WEIGHT,
				NORTH_PANEL_MODE_TITLE_WEIGHT,
				NORTH_PANEL_MODE_COMBOBOX_WEIGHT,
				NORTH_PANEL_RANKING_TITLE_WEIGHT,
				NORTH_PANEL_RANKING_COMBOBOX_WEIGHT,
				NORTH_PANEL_SESSION_TITLE_WEIGHT,
				NORTH_PANEL_SESSION_COMBOBOX_WEIGHT,
				NORTH_PANEL_BUTTON_SPACER_WEIGHT,
				NORTH_PANEL_BUTTON_EXPORT_IMAGE_WEIGHT,
				NORTH_PANEL_BUTTON_EXPORT_CSV_WEIGHT
			);
			panelNorth.setLayout(northLayout);
			panelNorth.setBorder(BorderFactory.createLoweredBevelBorder());
			add(
				panelNorth,
				BorderLayout.NORTH
			);
			final ProportionalGridLayoutConstraint c = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			c.y = 0;
			c.x = NORTH_PANEL_TOURNAMENT_TITLE_INDEX;
			c.gridWidth = 1;
			panelNorth.add(
				new JLabel(
					translator.translate(UIText.GUI_SECONDARY_PLAN_TITLE_TOURNAMENT),
					SwingConstants.RIGHT
				),
				c
			);

			comboTournament = new JComboBox<String>();
			comboTournament.setEditable(false);

			c.x = NORTH_PANEL_TOURNAMENT_COMBOBOX_INDEX;
			c.gridWidth = 2;
			panelNorth.add(
				comboTournament,
				c
			);

			c.x = NORTH_PANEL_SESSION_TITLE_INDEX;
			c.gridWidth = 1;
			panelNorth.add(
				new JLabel(
					translator.translate(UIText.GUI_SECONDARY_PLAN_TITLE_SESSION),
					SwingConstants.RIGHT
				),
				c
			);
			comboSession = new JComboBox<String>();
			c.x = NORTH_PANEL_SESSION_COMBOBOX_INDEX;
			c.gridWidth = 1;
			panelNorth.add(
				comboSession,
				c
			);

			c.x = NORTH_PANEL_BUTTON_EXPORT_IMAGE_INDEX;
			c.gridWidth = 1;
			buttonExportScoreSheet = new JButton(translator.translate(UIText.GUI_SECONDARY_PLAN_BUTTON_EXPORT_SCORE_SHEET));
			panelNorth.add(
				buttonExportScoreSheet,
				c
			);

			c.x = NORTH_PANEL_BUTTON_EXPORT_CSV_INDEX;
			c.gridWidth = 1;
			buttonExportPlan = new JButton(translator.translate(UIText.GUI_SECONDARY_PLAN_BUTTON_EXPORT_PLAN));
			panelNorth.add(
				buttonExportPlan,
				c
			);
		}

		{
			panelCenterSupport = new JPanel(new GridBagLayout());
			panelCenterSupport.setOpaque(true);
			panelCenterSupport.setBackground(getColorProfile().getTabBackgroundColor());

			final GridBagConstraints constraints = new GridBagConstraints();
			constraints.gridx = 0;
			constraints.gridy = 0;
			constraints.gridwidth = 1;
			constraints.gridheight = 1;
			constraints.weightx = 1.0;
			constraints.weighty = 1.0;
			constraints.anchor = GridBagConstraints.NORTH;
			constraints.fill = GridBagConstraints.HORIZONTAL;

			panelPlan = new JPanel();
			panelPlan.setOpaque(false);
			panelCenterSupport.add(
				panelPlan,
				constraints
			);

			scrollRanking = new JScrollPane(
				panelCenterSupport,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED
			);
			scrollRanking.getVerticalScrollBar().setUnitIncrement(16);

			add(
				scrollRanking,
				BorderLayout.CENTER
			);
		}

		listTournament = new ArrayList<Tournament<ScoreClassType, ScoreEnumType>>();

		tournamentComboBoxActionListener = (final ActionEvent e) -> {
			changeTournament();
		};
		sessionComboBoxActionListener = (final ActionEvent e) -> {
			displayPlan();
		};
		buttonExportScoreSheet.addActionListener((final ActionEvent e) -> {
			exportScoreSheet();
		});
		buttonExportPlan.addActionListener((final ActionEvent e) -> {
			exportPlan();
		});

		try {
			lastChoosenFolder = new File(getClass().getProtectionDomain().getCodeSource().getLocation().toURI());
		} catch (final URISyntaxException e) {
			lastChoosenFolder = null;
		}
	}

	@Override
	public String getTabName() {
		return translator.translate(UIText.GUI_SECONDARY_PLAN_TAB_NAME);
	}

	@Override
	public void remoteRefresh() {
		refresh(false);
	}

	private UIPlanTableColorProfile getColorProfile() {
		switch (config.getColorProfile()) {
			case WHITE_ON_BLACK:
				return UIPlanTableColorProfile.PROFILE_WHITE_ON_BLACK;
			case BLACK_ON_WHITE:
				return UIPlanTableColorProfile.PROFILE_BLACK_ON_WHITE;
			default:
				return null;
		}
	}

	@Override
	public void refresh(final boolean hard) {
		int selectedTournamentIndex = comboTournament.getSelectedIndex();
		Tournament<ScoreClassType, ScoreEnumType> selectedTournament = null;
		if (selectedTournamentIndex >= 0) {
			selectedTournament = listTournament.get(selectedTournamentIndex);
		}

		comboTournament.removeActionListener(tournamentComboBoxActionListener);
		comboTournament.removeAllItems();
		listTournament.clear();
		for (final Tournament<ScoreClassType, ScoreEnumType> tournament : tournamentManager.getAllTournament()) {
			if (!tournament.isArchived()) {
				listTournament.add(tournament);
			}
		}

		if (listTournament.size() > 0) {
			Collections.sort(listTournament);

			selectedTournamentIndex = -1;
			for (int index = 0; index < listTournament.size(); index++) {
				final Tournament<ScoreClassType, ScoreEnumType> tournament = listTournament.get(index);
				if (tournament == selectedTournament) {
					selectedTournamentIndex = index;
				}
				comboTournament.addItem(tournament.getName());
			}

			if (selectedTournamentIndex >= 0) {
				comboTournament.setSelectedIndex(selectedTournamentIndex);
				if (hard) {
					changeTournament();
				} else {
					displayPlan();
				}
			} else {
				comboTournament.setSelectedIndex(0);
				changeTournament();
			}
		} else {
			changeTournament();
		}
		comboTournament.addActionListener(tournamentComboBoxActionListener);
	}

	private void changeTournament() {
		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		comboSession.removeActionListener(sessionComboBoxActionListener);
		comboSession.removeAllItems();
		int selectSessionIndex = -1;
		if (listTournament.size() > 0 && selectedTournamentIndex >= 0) {
			final Tournament<ScoreClassType, ScoreEnumType> tournament = listTournament.get(selectedTournamentIndex);
			for (int session = 1; session <= tournament.getNbExistingSessions(); session++) {
				comboSession.addItem(SESSION_ID_FORMAT.format(session));
			}
			selectSessionIndex = 0;
		}
		comboSession.addActionListener(sessionComboBoxActionListener);
		comboSession.setSelectedIndex(selectSessionIndex);
	}

	private void displayPlan() {
		final UIPlanTableColorProfile tablePlanColorProfile = getColorProfile();
		invalidate();
		panelPlan.removeAll();
		panelCenterSupport.setBackground(tablePlanColorProfile.getTabBackgroundColor());

		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		final int selectedSessionIndex = comboSession.getSelectedIndex();
		if (selectedTournamentIndex != -1 && selectedSessionIndex != -1) {
			final Tournament<ScoreClassType, ScoreEnumType> tournament = listTournament.get(selectedTournamentIndex);
			final TournamentPlayer[][] plan = new TournamentPlayer[tournament.getNbTables()][4];
			final int session = comboSession.getSelectedIndex() + 1;
			for (final ScoreClassType table : tournament.getTables()) {
				if (table.getSessionId() == session) {
					plan[table.getTableId() - 1][table.getTablePlayerId() - 1] = table.getTournamentPlayer();
				}
			}

			final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
				plan.length + 1,
				7,
				0,
				0
			);
			layout.setWeightX(
				1,
				1,
				2,
				2,
				2,
				2,
				1
			);
			panelPlan.setLayout(layout);

			final int fontSize = getConfigFontSize();
			final int labelHeight = fontSize + LABEL_PADDING * 2;
			final Font labelFont = (CUSTOM_FONT != null
				? CUSTOM_FONT
				: getFont()).deriveFont(
					Font.PLAIN,
					fontSize
				);

			final ProportionalGridLayoutConstraint c = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);
			c.y = 0;
			for (int place = 0; place < PLACE.length; place++) {
				c.x = place + 2;
				final JLabel labelPlace = new JLabel(
					translator.translate(PLACE[place]),
					SwingConstants.CENTER
				);
				labelPlace.setPreferredSize(
					new Dimension(
						0,
						labelHeight
					)
				);
				labelPlace.setBorder(BorderFactory.createLineBorder(tablePlanColorProfile.getFontColor()));
				labelPlace.setFont(labelFont);
				labelPlace.setForeground(tablePlanColorProfile.getFontColor());
				panelPlan.add(
					labelPlace,
					c
				);
			}

			for (int table = 0; table < plan.length; table++) {
				c.y = table + 1;

				c.x = 1;
				final JLabel labelTable = new JLabel(
					translator.translate(UIText.GUI_SECONDARY_PLAN_TITLE_TABLE)
						+ " "
						+ TABLE_ID_FORMAT.format(table + 1),
					SwingConstants.CENTER
				);
				labelTable.setPreferredSize(
					new Dimension(
						0,
						labelHeight
					)
				);
				labelTable.setBorder(BorderFactory.createLineBorder(tablePlanColorProfile.getFontColor()));
				labelTable.setFont(labelFont);
				labelTable.setForeground(tablePlanColorProfile.getFontColor());
				if (table % 2 == 0) {
					labelTable.setOpaque(true);
					labelTable.setBackground(tablePlanColorProfile.getDarkenLineBackgroundColor());
				}
				panelPlan.add(
					labelTable,
					c
				);

				for (int playerIndex = 0; playerIndex < 4; playerIndex++) {
					c.x = playerIndex + 2;
					final JLabel labelPlayer = new JLabel(plan[table][playerIndex].getIDPlayerName(translator.translate(UIText.GUI_SECONDARY_PLAN_TITLE_PLAYER_EMPTY)));
					labelPlayer.setPreferredSize(
						new Dimension(
							0,
							labelHeight
						)
					);

					labelPlayer.setBorder(
						BorderFactory.createCompoundBorder(
							BorderFactory.createLineBorder(tablePlanColorProfile.getFontColor()),
							BorderFactory.createEmptyBorder(
								0,
								4,
								0,
								4
							)
						)
					);
					labelPlayer.setFont(labelFont);
					labelPlayer.setForeground(tablePlanColorProfile.getFontColor());
					panelPlan.add(
						labelPlayer,
						c
					);
					if (table % 2 == 0) {
						labelPlayer.setOpaque(true);
						labelPlayer.setBackground(tablePlanColorProfile.getDarkenLineBackgroundColor());
					}
				}
			}
		}

		validate();
		repaint();
	}

	private void exportPlan() {
		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		if (listTournament.size() > 0 && selectedTournamentIndex >= 0) {
			final UINumberFormPerPageDialog numberFormPerPageDialog = new UINumberFormPerPageDialog(
				(JFrame) SwingUtilities.getWindowAncestor(this),
				translator
			);
			numberFormPerPageDialog.showDialog();
			final int nbFormsPerPage = numberFormPerPageDialog.getNbFormsPerPage();
			final boolean drawLineBetweenForms = numberFormPerPageDialog.isDrawLineBetweenForms();
			final PaperSizeType formSize = PaperSizeType.A6;
			PaperSizeType paperSize = null;
			switch (nbFormsPerPage) {
				case 1:
					paperSize = PaperSizeType.A6;
					break;
				case 2:
					paperSize = PaperSizeType.A5;
					break;
				case 4:
					paperSize = PaperSizeType.A4;
					break;
				case 8:
					paperSize = PaperSizeType.A3;
					break;
				default:
					paperSize = PaperSizeType.A4;
					break;
			}

			final Tournament<ScoreClassType, ScoreEnumType> tournament = listTournament.get(selectedTournamentIndex);
			final JFileChooser pdfFileChooser = new JFileChooser();
			pdfFileChooser.setMultiSelectionEnabled(false);
			pdfFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			final String saveFileName = "Plan_"
				+ regulateString(tournament.getName())
				+ ".pdf";
			if (lastChoosenFolder != null) {
				pdfFileChooser.setSelectedFile(
					new File(
						lastChoosenFolder,
						saveFileName
					)
				);
			} else {
				pdfFileChooser.setSelectedFile(new File(saveFileName));
			}
			File pdfFile = null;
			boolean toContinue = true;
			while (toContinue) {
				final int fileChooserAnswer = pdfFileChooser.showSaveDialog(this);
				if (fileChooserAnswer == JFileChooser.APPROVE_OPTION) {
					pdfFile = pdfFileChooser.getSelectedFile();
					if (pdfFile.exists()) {
						final MessageDialogOption overwriteAnswer = new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_SECONDARY_PLAN_EXPORT_OVERWRITE_CONFIRM_MESSAGE),
							translator.translate(UIText.GUI_SECONDARY_PLAN_EXPORT_OVERWRITE_CONFIRM_TITLE),
							MessageDialogMessageType.WARNING,
							MessageDialogOptionsType.YES_NO_CANCEL,
							MessageDialogOption.NO,
							translator
						).showOptionDialog();
						switch (overwriteAnswer) {
							case YES:
								toContinue = false;
								break;
							case NO:
								pdfFile = null;
								break;
							case CANCEL:
							case CLOSED:
								pdfFile = null;
								toContinue = false;
								break;
							default:
								break;
						}
					} else {
						toContinue = false;
					}
				} else {
					pdfFile = null;
					toContinue = false;
				}
			}

			if (pdfFile != null) {
				lastChoosenFolder = pdfFileChooser.getCurrentDirectory();
				if (PlanPrinter.savePlan(
					tournament,
					paperSize,
					formSize,
					drawLineBetweenForms,
					pdfFile,
					translator
				)) {
					new UIMessageDialog(
						(JFrame) SwingUtilities.getWindowAncestor(this),
						translator.translate(UIText.GUI_SECONDARY_PLAN_EXPORT_RESULT_FILE_SAVED_MESSAGE),
						translator.translate(UIText.GUI_SECONDARY_PLAN_EXPORT_RESULT_FILE_SAVED_TITLE),
						MessageDialogMessageType.INFORMATION,
						MessageDialogOptionsType.OK,
						MessageDialogOption.OK,
						translator
					).showOptionDialog();
				} else {
					new UIMessageDialog(
						(JFrame) SwingUtilities.getWindowAncestor(this),
						translator.translate(UIText.GUI_SECONDARY_PLAN_EXPORT_RESULT_FILE_NOT_SAVED_MESSAGE),
						translator.translate(UIText.GUI_SECONDARY_PLAN_EXPORT_RESULT_FILE_NOT_SAVED_TITLE),
						MessageDialogMessageType.ERROR,
						MessageDialogOptionsType.OK,
						MessageDialogOption.OK,
						translator
					).showOptionDialog();
				}
			}
		}
	}

	private void exportScoreSheet() {
		if (isWriteScoreSheetSupported()) {
			final int selectedTournamentIndex = comboTournament.getSelectedIndex();
			if (listTournament.size() > 0 && selectedTournamentIndex >= 0) {
				final UINumberFormPerPageDialog numberFormPerPageDialog = new UINumberFormPerPageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator
				);
				numberFormPerPageDialog.showDialog();
				final int nbFormsPerPage = numberFormPerPageDialog.getNbFormsPerPage();
				final boolean drawLineBetweenForms = numberFormPerPageDialog.isDrawLineBetweenForms();
				final PaperSizeType formSize = PaperSizeType.A6;
				PaperSizeType paperSize = null;
				switch (nbFormsPerPage) {
					case 1:
						paperSize = PaperSizeType.A6;
						break;
					case 2:
						paperSize = PaperSizeType.A5;
						break;
					case 4:
						paperSize = PaperSizeType.A4;
						break;
					case 8:
						paperSize = PaperSizeType.A3;
						break;
					default:
						paperSize = PaperSizeType.A4;
						break;
				}

				final Tournament<ScoreClassType, ScoreEnumType> tournament = listTournament.get(selectedTournamentIndex);
				final JFileChooser pdfFileChooser = new JFileChooser();
				pdfFileChooser.setMultiSelectionEnabled(false);
				pdfFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				final String saveFileName = "ScoreSheet_"
					+ regulateString(tournament.getName())
					+ ".pdf";
				if (lastChoosenFolder != null) {
					pdfFileChooser.setSelectedFile(
						new File(
							lastChoosenFolder,
							saveFileName
						)
					);
				} else {
					pdfFileChooser.setSelectedFile(new File(saveFileName));
				}
				File pdfFile = null;
				boolean toContinue = true;
				while (toContinue) {
					final int fileChooserAnswer = pdfFileChooser.showSaveDialog(this);
					if (fileChooserAnswer == JFileChooser.APPROVE_OPTION) {
						pdfFile = pdfFileChooser.getSelectedFile();
						if (pdfFile.exists()) {
							final MessageDialogOption overwriteAnswer = new UIMessageDialog(
								(JFrame) SwingUtilities.getWindowAncestor(this),
								translator.translate(UIText.GUI_SECONDARY_PLAN_EXPORT_OVERWRITE_CONFIRM_MESSAGE),
								translator.translate(UIText.GUI_SECONDARY_PLAN_EXPORT_OVERWRITE_CONFIRM_TITLE),
								MessageDialogMessageType.WARNING,
								MessageDialogOptionsType.YES_NO_CANCEL,
								MessageDialogOption.NO,
								translator
							).showOptionDialog();
							switch (overwriteAnswer) {
								case YES:
									toContinue = false;
									break;
								case NO:
									pdfFile = null;
									break;
								case CANCEL:
								case CLOSED:
									pdfFile = null;
									toContinue = false;
									break;
								default:
									break;
							}
						} else {
							toContinue = false;
						}
					} else {
						pdfFile = null;
						toContinue = false;
					}
				}

				if (pdfFile != null) {
					lastChoosenFolder = pdfFileChooser.getCurrentDirectory();
					if (writeScoreSheet(
						tournament,
						paperSize,
						formSize,
						drawLineBetweenForms,
						pdfFile
					)) {
						new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_SECONDARY_PLAN_EXPORT_RESULT_FILE_SAVED_MESSAGE),
							translator.translate(UIText.GUI_SECONDARY_PLAN_EXPORT_RESULT_FILE_SAVED_TITLE),
							MessageDialogMessageType.INFORMATION,
							MessageDialogOptionsType.OK,
							MessageDialogOption.OK,
							translator
						).showOptionDialog();
					} else {
						new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_SECONDARY_PLAN_EXPORT_RESULT_FILE_NOT_SAVED_MESSAGE),
							translator.translate(UIText.GUI_SECONDARY_PLAN_EXPORT_RESULT_FILE_NOT_SAVED_TITLE),
							MessageDialogMessageType.ERROR,
							MessageDialogOptionsType.OK,
							MessageDialogOption.OK,
							translator
						).showOptionDialog();
					}
				}
			}
		} else {
			new UIMessageDialog(
				(JFrame) SwingUtilities.getWindowAncestor(this),
				translator.translate(UIText.GUI_SECONDARY_PLAN_EXPORT_UNSUPPORTED_MESSAGE),
				translator.translate(UIText.GUI_SECONDARY_PLAN_EXPORT_UNSUPPORTED_TITLE),
				MessageDialogMessageType.INFORMATION,
				MessageDialogOptionsType.OK,
				MessageDialogOption.OK,
				translator
			).showOptionDialog();
		}
	}

	protected abstract boolean isWriteScoreSheetSupported();

	protected abstract boolean writeScoreSheet(
		Tournament<ScoreClassType, ScoreEnumType> tournament,
		final PaperSizeType paperSize,
		final PaperSizeType formSize,
		final boolean drawLineBetweenForms,
		File pdfFile
	);
}
