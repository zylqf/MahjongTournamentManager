/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.plan;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;

public class UINumberFormPerPageDialog extends JDialog {

	private static final long serialVersionUID = -7619202485464161848L;

	private final JFrame parent;

	private final JRadioButton buttonDrawLineYes;
	private final JRadioButton buttonDrawLineNo;

	private final JRadioButton buttonNbForms1;
	private final JRadioButton buttonNbForms2;
	private final JRadioButton buttonNbForms4;
	private final JRadioButton buttonNbForms8;

	private int nbFormsPerPage;
	private boolean drawLineBetweenForms;

	public UINumberFormPerPageDialog(
		final JFrame parent,
		final UITextTranslator translator
	) {
		super(
			parent,
			translator.translate(UIText.GUI_SECONDARY_PLAN_EXPORT_FORMAT_DIALOG_TITLE),
			true
		);
		this.parent = parent;

		final Container pane = getContentPane();
		pane.setLayout(new BorderLayout());

		{
			final JPanel centerPanel = new JPanel();
			final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
				2,
				5,
				8,
				8
			);
			centerPanel.setLayout(layout);
			centerPanel.setBorder(
				BorderFactory.createEmptyBorder(
					16,
					16,
					16,
					16
				)
			);

			final ProportionalGridLayoutConstraint constraint = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			{
				constraint.y = 0;

				final JLabel labelDrawLines = new JLabel(
					translator.translate(UIText.GUI_SECONDARY_PLAN_EXPORT_FORMAT_DIALOG_DRAW_LINES),
					SwingConstants.RIGHT
				);
				constraint.x = 0;
				centerPanel.add(
					labelDrawLines,
					constraint
				);

				buttonDrawLineYes = new JRadioButton(translator.translate(UIText.GUI_DIALOG_BUTTON_YES));
				constraint.x = 1;
				centerPanel.add(
					buttonDrawLineYes,
					constraint
				);

				buttonDrawLineNo = new JRadioButton(translator.translate(UIText.GUI_DIALOG_BUTTON_NO));
				constraint.x = 2;
				centerPanel.add(
					buttonDrawLineNo,
					constraint
				);

				final ButtonGroup buttonGroup = new ButtonGroup();
				buttonGroup.add(buttonDrawLineYes);
				buttonGroup.add(buttonDrawLineNo);
				buttonGroup.setSelected(
					buttonDrawLineYes.getModel(),
					true
				);
			}

			{
				constraint.y = 1;

				final JLabel labelNumberSheet = new JLabel(
					translator.translate(UIText.GUI_SECONDARY_PLAN_EXPORT_FORMAT_DIALOG_NUMBER_FORM),
					SwingConstants.RIGHT
				);
				constraint.x = 0;
				centerPanel.add(
					labelNumberSheet,
					constraint
				);

				buttonNbForms1 = new JRadioButton("1");
				constraint.x = 1;
				centerPanel.add(
					buttonNbForms1,
					constraint
				);

				buttonNbForms2 = new JRadioButton("2");
				constraint.x = 2;
				centerPanel.add(
					buttonNbForms2,
					constraint
				);

				buttonNbForms4 = new JRadioButton("4");
				constraint.x = 3;
				centerPanel.add(
					buttonNbForms4,
					constraint
				);

				buttonNbForms8 = new JRadioButton("8");
				constraint.x = 4;
				centerPanel.add(
					buttonNbForms8,
					constraint
				);

				final ButtonGroup buttonGroup = new ButtonGroup();
				buttonGroup.add(buttonNbForms1);
				buttonGroup.add(buttonNbForms2);
				buttonGroup.add(buttonNbForms4);
				buttonGroup.add(buttonNbForms8);
				buttonGroup.setSelected(
					buttonNbForms4.getModel(),
					true
				);
			}

			pane.add(
				centerPanel,
				BorderLayout.CENTER
			);
		}

		{
			final JPanel southPanel = new JPanel();
			final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
				3,
				5,
				0,
				0
			);
			layout.setWeightY(
				1,
				8,
				1
			);
			southPanel.setLayout(layout);

			final ProportionalGridLayoutConstraint constraint = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			constraint.y = 1;
			constraint.x = 2;
			final JButton buttonOk = new JButton(translator.translate(UIText.GUI_SECONDARY_PLAN_EXPORT_FORMAT_DIALOG_BUTTON_OK));
			southPanel.add(
				buttonOk,
				constraint
			);

			pane.add(
				southPanel,
				BorderLayout.SOUTH
			);

			buttonOk.addActionListener((final ActionEvent e) -> {
				setVisible(false);
			});
		}

		pack();
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setResizable(false);
	}

	public void showDialog() {
		final Dimension size = getSize();
		if (parent != null && (parent.getExtendedState() & Frame.ICONIFIED) == 0) {
			final Point parentLocation = parent.getLocationOnScreen();
			final Dimension parentSize = parent.getSize();
			final int x = parentLocation.x + (parentSize.width - size.width) / 2;
			final int y = parentLocation.y + (parentSize.height - size.height) / 2;
			setLocation(
				x,
				y
			);
		} else {
			final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			final int x = (screenSize.width - size.width) / 2;
			final int y = (screenSize.height - size.height) / 2;
			setLocation(
				x,
				y
			);
		}

		setVisible(true);
		dispose();

		nbFormsPerPage = 0;
		if (buttonNbForms1.isSelected()) {
			nbFormsPerPage = 1;
		} else if (buttonNbForms2.isSelected()) {
			nbFormsPerPage = 2;
		} else if (buttonNbForms4.isSelected()) {
			nbFormsPerPage = 4;
		} else if (buttonNbForms8.isSelected()) {
			nbFormsPerPage = 8;
		}

		drawLineBetweenForms = false;
		if (buttonDrawLineYes.isSelected()) {
			drawLineBetweenForms = true;
		} else if (buttonDrawLineNo.isSelected()) {
			drawLineBetweenForms = false;
		}
	}

	public int getNbFormsPerPage() {
		return nbFormsPerPage;
	}

	public boolean isDrawLineBetweenForms() {
		return drawLineBetweenForms;
	}
}
