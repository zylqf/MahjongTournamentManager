/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.timer;

import java.awt.Color;

public class UITimerAnalogColorProfile {

	private static final int ONE_HOUR_IN_SECONDS = 60 * 60;
	private static final int HALF_HOUR_IN_SECONDS = 30 * 60;

	public static final UITimerAnalogColorProfile PROFILE_BLACK_ON_WHITE = new UITimerAnalogColorProfile(
		HALF_HOUR_IN_SECONDS,
		new Color(
			255,
			255,
			255
		),
		new Color(
			0,
			0,
			0
		),
		new Color(
			0,
			0,
			0
		),
		new Color(
			255,
			255,
			255
		),
		new Color(
			255,
			63,
			63
		),
		new Color(
			63,
			255,
			63
		),
		new Color(
			63,
			63,
			255
		),
		new Color(
			0,
			0,
			0
		)
	);

	public static final UITimerAnalogColorProfile PROFILE_WHITE_ON_BLACK = new UITimerAnalogColorProfile(
		HALF_HOUR_IN_SECONDS,
		new Color(
			0,
			0,
			0
		),
		new Color(
			255,
			255,
			255
		),
		new Color(
			255,
			255,
			255
		),
		new Color(
			0,
			0,
			0
		),
		new Color(
			255,
			63,
			63
		),
		new Color(
			63,
			255,
			63
		),
		new Color(
			63,
			63,
			255
		),
		new Color(
			255,
			255,
			255
		)
	);

	private final Color clockPanelBGColor;
	private final Color contourColor;
	private final Color handColor;
	private final Color faceBackColor;
	private final Color faceColor1;
	private final Color faceColor2;
	private final Color strokeFGColor;
	private final int colorChangeSecond;

	private final float startHue;
	private final float diffHue;
	private final float startSaturation;
	private final float diffSaturation;
	private final float startBrightness;
	private final float diffBrightness;

	protected UITimerAnalogColorProfile(
		final int colorChangeSecond,
		final Color clockPanelBGColor,
		final Color contourColor,
		final Color handColor,
		final Color faceBackColor,
		final Color faceWarningColor,
		final Color faceColor1,
		final Color faceColor2,
		final Color strokeFGColor
	) {
		this.colorChangeSecond = colorChangeSecond;
		this.clockPanelBGColor = clockPanelBGColor;
		this.contourColor = contourColor;
		this.handColor = handColor;
		this.faceBackColor = faceBackColor;
		this.faceColor1 = faceColor1;
		this.faceColor2 = faceColor2;
		this.strokeFGColor = strokeFGColor;

		float[] hsb = new float[3];
		hsb = Color.RGBtoHSB(
			faceColor1.getRed(),
			faceColor1.getGreen(),
			faceColor1.getBlue(),
			hsb
		);
		startHue = hsb[0];
		startSaturation = hsb[1];
		startBrightness = hsb[2];
		hsb = Color.RGBtoHSB(
			faceWarningColor.getRed(),
			faceWarningColor.getGreen(),
			faceWarningColor.getBlue(),
			hsb
		);
		diffHue = hsb[0] - startHue;
		diffSaturation = hsb[1] - startSaturation;
		diffBrightness = hsb[2] - startBrightness;
	}

	public Color getClockPanelBGColor() {
		return clockPanelBGColor;
	}

	public Color getContourColor() {
		return contourColor;
	}

	public Color getHandColor() {
		return handColor;
	}

	public Color[] getFaceColors(
		final int remainingSeconds,
		final boolean isRunning
	) {
		final int remainingHours = remainingSeconds / ONE_HOUR_IN_SECONDS + 2;
		final Color[] faceColors = new Color[remainingHours];
		faceColors[0] = faceBackColor;
		if (isRunning && remainingSeconds <= colorChangeSecond) {
			final float ratio = 1f * (colorChangeSecond - remainingSeconds) / colorChangeSecond;
			faceColors[1] = new Color(
				Color.HSBtoRGB(
					startHue + ratio * diffHue,
					startSaturation + ratio * diffSaturation,
					startBrightness + ratio * diffBrightness
				)
			);
		} else {
			faceColors[1] = faceColor1;
		}
		for (int colorIndex = 2; colorIndex < remainingHours; colorIndex++) {
			if (colorIndex % 2 == 0) {
				faceColors[colorIndex] = faceColor2;
			} else {
				faceColors[colorIndex] = faceColor1;
			}
		}
		return faceColors;
	}

	public Color getStrokeFGColor() {
		return strokeFGColor;
	}
}
