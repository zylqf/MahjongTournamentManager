/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.timer;

import java.io.IOException;
import java.net.URL;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.SourceDataLine;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSpinner;

import fr.bri.mj.gui.common.UITabPanel;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;

public abstract class UITabPanelTimer extends UITabPanel {

	private static final long serialVersionUID = -7927830973717808225L;

	protected static final int DEFAULT_TIMER_MCR = 110;
	protected static final int DEFAULT_TIMER_RCR = 75;
	protected static final int DEFAULT_TIMER_OTHER = 120;

	protected static final int NB_SECONDS_IN_ONE_MINUTE = 60;
	protected static final int NB_SECONDS_IN_FIFTEEN_MINUTES = NB_SECONDS_IN_ONE_MINUTE * 15;
	protected static final int NB_SECONDS_IN_FIVE_MINUTES = NB_SECONDS_IN_ONE_MINUTE * 5;

	protected static final int NB_MILLISECONDS_IN_ONE_SECOND = 1000;
	protected static final long NB_NANOSECONDS_IN_ONE_SECOND = 1000000000L;
	protected static final long NB_NANOSECONDS_IN_HALF_SECOND = 500000000L;

	protected JSpinner spinnerMinute;
	protected JButton buttonStart;
	protected JButton buttonPause;
	protected JButton buttonSet;
	protected JPanel panelCenter;

	protected boolean timerSet;
	protected long remainingNanoSeconds;
	protected Thread threadTimer;
	protected volatile boolean toStop;

	protected volatile UITimerVoiceProfile timerVoiceProfile;

	protected UITabPanelTimer(
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			config,
			translator
		);
	}

	@Override
	public String getTabName() {
		return translator.translate(UIText.GUI_SECONDARY_TIMER_TAB_NAME);
	}

	@Override
	public void stopRunningThreads() {
		pauseTimer();
	}

	protected abstract void setTimer();

	protected void startTimer() {
		if (remainingNanoSeconds > 0) {
			buttonStart.setEnabled(false);
			buttonPause.setEnabled(true);
			buttonSet.setEnabled(false);
			toStop = false;
			final boolean currentTimerSet = timerSet;
			threadTimer = new Thread(() -> {
				timerRun(currentTimerSet);
			});
			threadTimer.start();
			timerSet = false;
		}
	}

	protected void pauseTimer() {
		if (threadTimer != null) {
			toStop = true;
		}
	}

	protected abstract void timerRun(final boolean startingGongSound);

	protected void soundPlayerRun(final String sound) {
		new Thread(() -> {
			AudioInputStream audioStream = null;
			SourceDataLine sourceLine = null;
			try {
				final URL urlSoundFile = ClassLoader.getSystemResource(sound);
				audioStream = AudioSystem.getAudioInputStream(urlSoundFile);
				final AudioFormat audioFormat = audioStream.getFormat();
				final DataLine.Info info = new DataLine.Info(
					SourceDataLine.class,
					audioFormat
				);
				sourceLine = (SourceDataLine) AudioSystem.getLine(info);
				sourceLine.open(audioFormat);
				sourceLine.start();
				final byte[] buffer = new byte[262144];
				int nbRead = audioStream.read(buffer);
				while (nbRead != -1) {
					sourceLine.write(
						buffer,
						0,
						nbRead
					);
					nbRead = audioStream.read(buffer);
				}
				sourceLine.drain();
			} catch (final Exception e) {
				e.printStackTrace();
			} finally {
				if (audioStream != null) {
					try {
						audioStream.close();
					} catch (final IOException e) {
					}
				}
				if (sourceLine != null) {
					sourceLine.close();
				}
			}
		}).start();
	}
}
