/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.ranking.mcr;

import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.tournament.mcr.ScoreMCR;
import fr.bri.mj.data.tournament.mcr.TournamentScoreMCR;
import fr.bri.mj.gui.config.item.UIConfigNationalMode;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.gui.secondary.ranking.UIRankingChartTitle;
import fr.bri.mj.gui.secondary.ranking.UIRankingColorProfile;
import fr.bri.mj.gui.secondary.ranking.UIRankingMode;

public class UIRankingChartTitleMCR extends UIRankingChartTitle<TournamentScoreMCR, ScoreMCR> {

	private static final long serialVersionUID = 86030034105548575L;

	public UIRankingChartTitleMCR(
		final UIRankingMode<ScoreMCR> rankingMode,
		final UIConfigNationalMode nationalMode,
		final UIRankingColorProfile colorProfile,
		final Font labelFont,
		final UITextTranslator translator
	) {
		super(translator);
		setOpaque(false);

		final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
			1,
			7,
			0,
			0
		);
		layout.setWeightX(
			2,
			3,
			3,
			3,
			10,
			14,
			2
		);
		setLayout(layout);

		final ProportionalGridLayoutConstraint constraints = new ProportionalGridLayoutConstraint(
			0,
			1,
			0,
			1
		);

		constraints.y = 0;
		{
			constraints.x = 1;
			final JLabel labelTitleRanking = new JLabel(
				translator.translate(UIText.GUI_SECONDARY_RANKING_MCR_ROW_TITLE_RANKING),
				SwingConstants.CENTER
			);
			labelTitleRanking.setFont(labelFont);
			labelTitleRanking.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			add(
				labelTitleRanking,
				constraints
			);
		}
		{
			constraints.x = 2;
			String titleNationality = null;
			switch (rankingMode.getRankingModeTeamMode()) {
				case INDIVIDUAL:
					switch (nationalMode) {
						case NATIONALITY:
							titleNationality = translator.translate(UIText.GUI_SECONDARY_RANKING_MCR_ROW_TITLE_NATIONALITY);
							break;
						case CLUB_NAME:
							titleNationality = translator.translate(UIText.GUI_SECONDARY_RANKING_MCR_ROW_TITLE_CLUB);
							break;
					}
					break;
				case TEAM:
					titleNationality = "";
					break;
			}
			final JLabel labelTitleFlag = new JLabel(
				titleNationality,
				SwingConstants.CENTER
			);
			labelTitleFlag.setFont(labelFont);
			labelTitleFlag.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			add(
				labelTitleFlag,
				constraints
			);
		}
		{
			constraints.x = 3;
			String titlePlayerId = null;
			switch (rankingMode.getRankingModeTeamMode()) {
				case INDIVIDUAL:
					titlePlayerId = translator.translate(UIText.GUI_SECONDARY_RANKING_MCR_ROW_TITLE_ID);
					break;
				case TEAM:
					titlePlayerId = "";
					break;
			}
			final JLabel labelTitleId = new JLabel(
				titlePlayerId,
				SwingConstants.CENTER
			);
			labelTitleId.setFont(labelFont);
			labelTitleId.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			add(
				labelTitleId,
				constraints
			);
		}
		{
			constraints.x = 4;
			String titlePlayerName = null;
			switch (rankingMode.getRankingModeTeamMode()) {
				case INDIVIDUAL:
					titlePlayerName = translator.translate(UIText.GUI_SECONDARY_RANKING_MCR_ROW_TITLE_PLAYER_NAME);
					break;
				case TEAM:
					titlePlayerName = translator.translate(UIText.GUI_SECONDARY_RANKING_MCR_ROW_TITLE_TEAM_NAME);
					break;
			}
			final JLabel labelTitlePlayerName = new JLabel(
				titlePlayerName,
				SwingConstants.LEADING
			);
			labelTitlePlayerName.setFont(labelFont);
			labelTitlePlayerName.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			add(
				labelTitlePlayerName,
				constraints
			);
		}
		{
			constraints.x = 5;
			final JPanel panelScore = new JPanel();
			panelScore.setOpaque(false);

			final JLabel labelScore1 = new JLabel(
				"",
				SwingConstants.CENTER
			);
			labelScore1.setFont(labelFont);

			final JLabel labelScore2 = new JLabel(
				"",
				SwingConstants.CENTER
			);
			labelScore2.setFont(labelFont);

			final JLabel labelScore3 = new JLabel(
				"",
				SwingConstants.CENTER
			);
			labelScore3.setFont(labelFont);

			final ProportionalGridLayoutConstraint panelScoreLayoutConstraints = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			switch (rankingMode.getScore()) {
				case MATCH_POINT: {
					final ProportionalGridLayoutInteger panelScoreLayout = new ProportionalGridLayoutInteger(
						1,
						2,
						0,
						2
					);
					panelScore.setLayout(panelScoreLayout);

					panelScoreLayoutConstraints.x = 0;
					labelScore1.setText(translator.translate(UIText.GUI_SECONDARY_RANKING_MCR_ROW_TITLE_MATCH_POINT));
					panelScore.add(
						labelScore1,
						panelScoreLayoutConstraints
					);
					panelScoreLayoutConstraints.x = 1;
					labelScore2.setText(translator.translate(UIText.GUI_SECONDARY_RANKING_MCR_ROW_TITLE_TABLE_POINT));
					panelScore.add(
						labelScore2,
						panelScoreLayoutConstraints
					);
				}
					break;
				case TABLE_POINT: {
					final ProportionalGridLayoutInteger panelScoreLayout = new ProportionalGridLayoutInteger(
						1,
						2,
						0,
						2
					);
					panelScore.setLayout(panelScoreLayout);

					panelScoreLayoutConstraints.x = 0;
					labelScore1.setText(translator.translate(UIText.GUI_SECONDARY_RANKING_MCR_ROW_TITLE_TABLE_POINT));
					panelScore.add(
						labelScore1,
						panelScoreLayoutConstraints
					);
					panelScoreLayoutConstraints.x = 1;
					labelScore2.setText(translator.translate(UIText.GUI_SECONDARY_RANKING_MCR_ROW_TITLE_MATCH_POINT));
					panelScore.add(
						labelScore2,
						panelScoreLayoutConstraints
					);
				}
					break;
			}

			labelScore1.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			labelScore2.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());
			labelScore3.setForeground(colorProfile.getUpToDatePositiveScoreFontColor());

			add(
				panelScore,
				constraints
			);
		}
	}
}
