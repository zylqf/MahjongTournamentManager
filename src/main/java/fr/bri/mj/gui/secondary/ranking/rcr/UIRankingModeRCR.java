/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.ranking.rcr;

import java.util.ArrayList;
import java.util.List;

import fr.bri.mj.data.score.aggregated.QualificationMode;
import fr.bri.mj.data.score.aggregated.TeamMode;
import fr.bri.mj.data.tournament.rcr.ScoreRCR;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.secondary.ranking.UIRankingMode;

public enum UIRankingModeRCR implements UIRankingMode<ScoreRCR> {

	FINAL_SCORE_INDIVIDUAL(UIText.GUI_RANKING_RCR_MODE_INDIVIDUAL_FINAL_SCORE, ScoreRCR.FINAL_SCORE, QualificationMode.WITHOUT, TeamMode.INDIVIDUAL),
	FINAL_SCORE_INDIVIDUAL_WITH_QUALIFICATION(UIText.GUI_RANKING_RCR_MODE_INDIVIDUAL_FINAL_SCORE, ScoreRCR.FINAL_SCORE, QualificationMode.WITH, TeamMode.INDIVIDUAL),
	GAME_SCORE_INDIVIDUAL(UIText.GUI_RANKING_RCR_MODE_INDIVIDUAL_GAME_SCORE, ScoreRCR.GAME_SCORE, QualificationMode.WITHOUT, TeamMode.INDIVIDUAL),
	UMA_SCORE_INDIVIDUAL(UIText.GUI_RANKING_RCR_MODE_INDIVIDUAL_UMA_SCORE, ScoreRCR.UMA_SCORE, QualificationMode.WITHOUT, TeamMode.INDIVIDUAL),

	FINAL_SCORE_TEAM(UIText.GUI_RANKING_RCR_MODE_TEAM_FINAL_SCORE, ScoreRCR.FINAL_SCORE, QualificationMode.WITHOUT, TeamMode.TEAM),
	GAME_SCORE_TEAM(UIText.GUI_RANKING_RCR_MODE_TEAM_GAME_SCORE, ScoreRCR.GAME_SCORE, QualificationMode.WITHOUT, TeamMode.TEAM),
	UMA_SCORE_TEAM(UIText.GUI_RANKING_RCR_MODE_TEAM_UMA_SCORE, ScoreRCR.UMA_SCORE, QualificationMode.WITHOUT, TeamMode.TEAM);

	public static List<UIRankingMode<ScoreRCR>> getRankingModes(
		final TeamMode teamMode,
		final QualificationMode qualificationMode
	) {
		final List<UIRankingMode<ScoreRCR>> listeMode = new ArrayList<UIRankingMode<ScoreRCR>>();
		for (final UIRankingModeRCR mode : UIRankingModeRCR.values()) {
			if ((teamMode == null || mode.teamMode == teamMode) && mode.qualificationMode == qualificationMode) {
				listeMode.add(mode);
			}
		}
		return listeMode;
	}

	private UIRankingModeRCR(
		final UIText uiText,
		final ScoreRCR score,
		final QualificationMode qualificationMode,
		final TeamMode teamMode
	) {
		this.uiText = uiText;
		this.score = score;
		this.qualificationMode = qualificationMode;
		this.teamMode = teamMode;
	}

	private final UIText uiText;
	private final ScoreRCR score;
	private final QualificationMode qualificationMode;
	private final TeamMode teamMode;

	@Override
	public UIText getUIText() {
		return uiText;
	}

	@Override
	public ScoreRCR getScore() {
		return score;
	}

	@Override
	public QualificationMode getQualificationMode() {
		return qualificationMode;
	}

	@Override
	public TeamMode getRankingModeTeamMode() {
		return teamMode;
	}
}
