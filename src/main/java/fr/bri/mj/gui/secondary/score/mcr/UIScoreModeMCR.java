/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.score.mcr;

import java.util.ArrayList;
import java.util.List;

import fr.bri.mj.data.score.aggregated.TeamMode;
import fr.bri.mj.data.tournament.mcr.ScoreMCR;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.secondary.score.UIScoreMode;

public enum UIScoreModeMCR implements UIScoreMode<ScoreMCR> {

	MATCH_POINT_INDIVIDUAL(UIText.GUI_RANKING_MCR_MODE_INDIVIDUAL_MATCH_POINT, ScoreMCR.MATCH_POINT, TeamMode.INDIVIDUAL),
	TABLE_POINT_INDIVIDUAL(UIText.GUI_RANKING_MCR_MODE_INDIVIDUAL_TABLE_POINT, ScoreMCR.TABLE_POINT, TeamMode.INDIVIDUAL),

	MATCH_POINT_TEAM(UIText.GUI_RANKING_MCR_MODE_TEAM_MATCH_POINT, ScoreMCR.MATCH_POINT, TeamMode.TEAM),
	TABLE_POINT_TEAM(UIText.GUI_RANKING_MCR_MODE_TEAM_TABLE_POINT, ScoreMCR.TABLE_POINT, TeamMode.TEAM),;

	public static List<UIScoreMode<ScoreMCR>> getRankingModes() {
		final List<UIScoreMode<ScoreMCR>> listeMode = new ArrayList<UIScoreMode<ScoreMCR>>();
		for (final UIScoreModeMCR mode : UIScoreModeMCR.values()) {
			listeMode.add(mode);
		}
		return listeMode;
	}

	private UIScoreModeMCR(
		final UIText uiText,
		final ScoreMCR scoreMode,
		final TeamMode teamType
	) {
		this.uiText = uiText;
		this.scoreMode = scoreMode;
		this.teamType = teamType;
	}

	private final UIText uiText;
	private final ScoreMCR scoreMode;
	private final TeamMode teamType;

	@Override
	public UIText getUIText() {
		return uiText;
	}

	@Override
	public ScoreMCR getScoreMode() {
		return scoreMode;
	}

	@Override
	public TeamMode getScoreTeamType() {
		return teamType;
	}
}
