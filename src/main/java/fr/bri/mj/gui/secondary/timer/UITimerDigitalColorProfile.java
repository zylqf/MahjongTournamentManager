/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.timer;

import java.awt.Color;

public class UITimerDigitalColorProfile {

	private static final int HALF_HOUR_IN_SECONDS = 30 * 60;

	public static final UITimerDigitalColorProfile PROFILE_BLACK_ON_WHITE = new UITimerDigitalColorProfile(
		HALF_HOUR_IN_SECONDS,
		new Color(
			255,
			255,
			255
		),
		new Color(
			223,
			223,
			223
		),
		new Color(
			0,
			0,
			0
		),
		new Color(
			255,
			0,
			0
		)
	);

	public static final UITimerDigitalColorProfile PROFILE_WHITE_ON_BLACK = new UITimerDigitalColorProfile(
		HALF_HOUR_IN_SECONDS,
		new Color(
			0,
			0,
			0
		),
		new Color(
			31,
			31,
			31
		),
		new Color(
			255,
			255,
			255
		),
		new Color(
			255,
			0,
			0
		)
	);

	private final Color clockPanelBGColor;
	private final Color strokeBGColor;
	private final Color strokeFGColor;
	private final Color strokeWarningColor;
	private final float colorChangeSecond;

	private final float startHue;
	private final float diffHue;
	private final float startSaturation;
	private final float diffSaturation;
	private final float startBrightness;
	private final float diffBrightness;

	private UITimerDigitalColorProfile(
		final int colorChangeSecond,
		final Color clockPanelBGColor,
		final Color strokeBGColor,
		final Color strokeFGColor,
		final Color strokeWarningColor
	) {
		this.colorChangeSecond = colorChangeSecond;
		this.clockPanelBGColor = clockPanelBGColor;
		this.strokeBGColor = strokeBGColor;
		this.strokeFGColor = strokeFGColor;
		this.strokeWarningColor = strokeWarningColor;

		float[] hsb = new float[3];
		hsb = Color.RGBtoHSB(
			strokeFGColor.getRed(),
			strokeFGColor.getGreen(),
			strokeFGColor.getBlue(),
			hsb
		);
		startHue = hsb[0];
		startSaturation = hsb[1];
		startBrightness = hsb[2];
		hsb = Color.RGBtoHSB(
			strokeWarningColor.getRed(),
			strokeWarningColor.getGreen(),
			strokeWarningColor.getBlue(),
			hsb
		);
		diffHue = hsb[0] - startHue;
		diffSaturation = hsb[1] - startSaturation;
		diffBrightness = hsb[2] - startBrightness;
	}

	public Color getClockPanelBGColor() {
		return clockPanelBGColor;
	}

	public Color getStrokeBGColor() {
		return strokeBGColor;
	}

	public Color getStrokeFGColor() {
		return strokeFGColor;
	}

	public Color getStrokeWarningColor() {
		return strokeWarningColor;
	}

	public float getColorChangeSecond() {
		return colorChangeSecond;
	}

	public Color getStrokeFGColor(final int remainingSeconds) {
		if (remainingSeconds <= colorChangeSecond) {
			final float ratio = (colorChangeSecond - remainingSeconds) / colorChangeSecond;
			return new Color(
				Color.HSBtoRGB(
					startHue + ratio * diffHue,
					startSaturation + ratio * diffSaturation,
					startBrightness + ratio * diffBrightness
				)
			);
		} else {
			return strokeFGColor;
		}
	}
}
