/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary;

import fr.bri.mj.dataaccess.ManagerFactory;
import fr.bri.mj.gui.common.UITabPanel;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.config.item.UIConfigDisplayLanguage;
import fr.bri.mj.gui.config.item.UIConfigRuleSet;
import fr.bri.mj.gui.config.item.UIConfigTimerMode;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.gui.secondary.league.mcr.UITabPanelLeagueMCR;
import fr.bri.mj.gui.secondary.league.rcr.UITabPanelLeagueRCR;
import fr.bri.mj.gui.secondary.plan.mcr.UITabPanelDisplayPlanMCR;
import fr.bri.mj.gui.secondary.plan.rcr.UITabPanelDisplayPlanRCR;
import fr.bri.mj.gui.secondary.ranking.mcr.UITabPanelRankingMCR;
import fr.bri.mj.gui.secondary.ranking.rcr.UITabPanelRankingRCR;
import fr.bri.mj.gui.secondary.score.mcr.UITabPanelScoreMCR;
import fr.bri.mj.gui.secondary.score.rcr.UITabPanelScoreRCR;
import fr.bri.mj.gui.secondary.timer.UITabPanelTimerAnalog;
import fr.bri.mj.gui.secondary.timer.UITabPanelTimerDigital;
import fr.bri.mj.gui.secondary.trend.mcr.UITabPanelTrendMCR;
import fr.bri.mj.gui.secondary.trend.rcr.UITabPanelTrendRCR;

public class UITabPanelSecondaryFactory {

	private static UIConfigDisplayLanguage lastDisplayLanguage = null;

	public static void checkDisplayLanguage(final UIConfigDisplayLanguage currentDisplayLanguage) {
		if (currentDisplayLanguage != null && currentDisplayLanguage != lastDisplayLanguage) {
			for (int index1 = 0; index1 < UIConfigRuleSet.values().length; index1++) {
				for (int index2 = 0; index2 < UIConfigTimerMode.values().length; index2++) {
					BUFFER_TIMER[index1][index2] = null;
				}
				BUFFER_PLAN[index1] = null;
				BUFFER_RANKING[index1] = null;
				BUFFER_LEAGUE[index1] = null;
				BUFFER_SCORE[index1] = null;
				BUFFER_TREND[index1] = null;
			}
			lastDisplayLanguage = currentDisplayLanguage;
		}
	}

	private static final UITabPanel BUFFER_TIMER[][] = new UITabPanel[UIConfigRuleSet.values().length][UIConfigTimerMode.values().length];

	public static UITabPanel getTabPanelTimer(
		final UIConfig config,
		final UITextTranslator translator
	) {
		checkDisplayLanguage(config.getDisplayLanguage());
		if (BUFFER_TIMER[config.getRuleSet().ordinal()][config.getTimerMode().ordinal()] == null) {
			switch (config.getRuleSet()) {
				case MCR:
					switch (config.getTimerMode()) {
						case ANALOG:
							BUFFER_TIMER[config.getRuleSet().ordinal()][config.getTimerMode().ordinal()] = new UITabPanelTimerAnalog(
								config,
								translator
							);
							break;
						case DIGITAL:
							BUFFER_TIMER[config.getRuleSet().ordinal()][config.getTimerMode().ordinal()] = new UITabPanelTimerDigital(
								config,
								translator
							);
							break;
					}
					break;
				case RCR:
					switch (config.getTimerMode()) {
						case ANALOG:
							BUFFER_TIMER[config.getRuleSet().ordinal()][config.getTimerMode().ordinal()] = new UITabPanelTimerAnalog(
								config,
								translator
							);
							break;
						case DIGITAL:
							BUFFER_TIMER[config.getRuleSet().ordinal()][config.getTimerMode().ordinal()] = new UITabPanelTimerDigital(
								config,
								translator
							);
							break;
					}
					break;
			}
		}
		return BUFFER_TIMER[config.getRuleSet().ordinal()][config.getTimerMode().ordinal()];
	}

	private static final UITabPanel BUFFER_PLAN[] = new UITabPanel[UIConfigRuleSet.values().length];

	public static UITabPanel getTabPanelPlan(
		final ManagerFactory managerFactory,
		final UIConfig config,
		final UITextTranslator translator
	) {
		checkDisplayLanguage(config.getDisplayLanguage());
		if (BUFFER_PLAN[config.getRuleSet().ordinal()] == null) {
			switch (config.getRuleSet()) {
				case MCR:
					BUFFER_PLAN[config.getRuleSet().ordinal()] = new UITabPanelDisplayPlanMCR(
						managerFactory.getTournamentManagerMCR(),
						config,
						translator
					);
					break;
				case RCR:
					BUFFER_PLAN[config.getRuleSet().ordinal()] = new UITabPanelDisplayPlanRCR(
						managerFactory.getTournamentManagerRCR(),
						config,
						translator
					);
					break;
			}
		}
		return BUFFER_PLAN[config.getRuleSet().ordinal()];
	}

	private static final UITabPanel BUFFER_RANKING[] = new UITabPanel[UIConfigRuleSet.values().length];

	public static UITabPanel getTabPanelRanking(
		final ManagerFactory managerFactory,
		final UIConfig config,
		final UITextTranslator translator
	) {
		checkDisplayLanguage(config.getDisplayLanguage());
		if (BUFFER_RANKING[config.getRuleSet().ordinal()] == null) {
			switch (config.getRuleSet()) {
				case MCR:
					BUFFER_RANKING[config.getRuleSet().ordinal()] = new UITabPanelRankingMCR(
						managerFactory.getTournamentManagerMCR(),
						config,
						translator
					);
					break;
				case RCR:
					BUFFER_RANKING[config.getRuleSet().ordinal()] = new UITabPanelRankingRCR(
						managerFactory.getTournamentManagerRCR(),
						config,
						translator
					);
					break;
			}
		}
		return BUFFER_RANKING[config.getRuleSet().ordinal()];
	}

	private static final UITabPanel BUFFER_LEAGUE[] = new UITabPanel[UIConfigRuleSet.values().length];

	public static UITabPanel getTabPanelLeague(
		final ManagerFactory managerFactory,
		final UIConfig config,
		final UITextTranslator translator
	) {
		checkDisplayLanguage(config.getDisplayLanguage());
		if (BUFFER_LEAGUE[config.getRuleSet().ordinal()] == null) {
			switch (config.getRuleSet()) {
				case MCR:
					BUFFER_LEAGUE[config.getRuleSet().ordinal()] = new UITabPanelLeagueMCR(
						managerFactory.getTournamentManagerMCR(),
						config,
						translator
					);

					break;
				case RCR:
					BUFFER_LEAGUE[config.getRuleSet().ordinal()] = new UITabPanelLeagueRCR(
						managerFactory.getTournamentManagerRCR(),
						config,
						translator
					);
					break;
			}
		}
		return BUFFER_LEAGUE[config.getRuleSet().ordinal()];
	}

	private static final UITabPanel BUFFER_SCORE[] = new UITabPanel[UIConfigRuleSet.values().length];

	public static UITabPanel getTabPanelScore(
		final ManagerFactory managerFactory,
		final UIConfig config,
		final UITextTranslator translator
	) {
		checkDisplayLanguage(config.getDisplayLanguage());
		if (BUFFER_SCORE[config.getRuleSet().ordinal()] == null) {
			switch (config.getRuleSet()) {
				case MCR:
					BUFFER_SCORE[config.getRuleSet().ordinal()] = new UITabPanelScoreMCR(
						managerFactory.getTournamentManagerMCR(),
						config,
						translator
					);
					break;
				case RCR:
					BUFFER_SCORE[config.getRuleSet().ordinal()] = new UITabPanelScoreRCR(
						managerFactory.getTournamentManagerRCR(),
						config,
						translator
					);

					break;
			}
		}
		return BUFFER_SCORE[config.getRuleSet().ordinal()];
	}

	private static final UITabPanel BUFFER_TREND[] = new UITabPanel[UIConfigRuleSet.values().length];

	public static UITabPanel getTabPanelTrend(
		final ManagerFactory managerFactory,
		final UIConfig config,
		final UITextTranslator translator
	) {
		checkDisplayLanguage(config.getDisplayLanguage());
		if (BUFFER_TREND[config.getRuleSet().ordinal()] == null) {
			switch (config.getRuleSet()) {
				case MCR:
					BUFFER_TREND[config.getRuleSet().ordinal()] = new UITabPanelTrendMCR(
						managerFactory.getTournamentManagerMCR(),
						config,
						translator
					);
					break;
				case RCR:
					BUFFER_TREND[config.getRuleSet().ordinal()] = new UITabPanelTrendRCR(
						managerFactory.getTournamentManagerRCR(),
						config,
						translator
					);

					break;
			}
		}
		return BUFFER_TREND[config.getRuleSet().ordinal()];
	}
}
