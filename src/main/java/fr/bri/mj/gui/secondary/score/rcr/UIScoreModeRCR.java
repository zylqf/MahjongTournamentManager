/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.secondary.score.rcr;

import java.util.ArrayList;
import java.util.List;

import fr.bri.mj.data.score.aggregated.TeamMode;
import fr.bri.mj.data.tournament.rcr.ScoreRCR;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.secondary.score.UIScoreMode;

public enum UIScoreModeRCR implements UIScoreMode<ScoreRCR> {

	FINAL_SCORE_INDIVIDUAL(UIText.GUI_RANKING_RCR_MODE_INDIVIDUAL_FINAL_SCORE, ScoreRCR.FINAL_SCORE, TeamMode.INDIVIDUAL),
	GAME_SCORE_INDIVIDUAL(UIText.GUI_RANKING_RCR_MODE_INDIVIDUAL_GAME_SCORE, ScoreRCR.GAME_SCORE, TeamMode.INDIVIDUAL),
	UMA_SCORE_INDIVIDUAL(UIText.GUI_RANKING_RCR_MODE_INDIVIDUAL_UMA_SCORE, ScoreRCR.UMA_SCORE, TeamMode.INDIVIDUAL),

	FINAL_SCORE_TEAM(UIText.GUI_RANKING_RCR_MODE_TEAM_FINAL_SCORE, ScoreRCR.FINAL_SCORE, TeamMode.TEAM),
	GAME_SCORE_TEAM(UIText.GUI_RANKING_RCR_MODE_TEAM_GAME_SCORE, ScoreRCR.GAME_SCORE, TeamMode.TEAM),
	UMA_SCORE_TEAM(UIText.GUI_RANKING_RCR_MODE_TEAM_UMA_SCORE, ScoreRCR.UMA_SCORE, TeamMode.TEAM),;

	public static List<UIScoreMode<ScoreRCR>> getScoreModes() {
		final List<UIScoreMode<ScoreRCR>> listeMode = new ArrayList<UIScoreMode<ScoreRCR>>();
		for (final UIScoreModeRCR mode : UIScoreModeRCR.values()) {
			listeMode.add(mode);
		}
		return listeMode;
	}

	private UIScoreModeRCR(
		final UIText uiText,
		final ScoreRCR scoreMode,
		final TeamMode teamType
	) {
		this.uiText = uiText;
		this.scoreMode = scoreMode;
		this.teamType = teamType;
	}

	private final UIText uiText;
	private final ScoreRCR scoreMode;
	private final TeamMode teamType;

	@Override
	public UIText getUIText() {
		return uiText;
	}

	@Override
	public ScoreRCR getScoreMode() {
		return scoreMode;
	}

	@Override
	public TeamMode getScoreTeamType() {
		return teamType;
	}
}
