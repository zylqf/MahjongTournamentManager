/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.common;

import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;

public class SelectAllTextFocusListener implements FocusListener {

	private final int nbCharsFromRight;

	public SelectAllTextFocusListener(final int nbCharsFromRight) {
		this.nbCharsFromRight = nbCharsFromRight;
	}

	@Override
	public void focusGained(final FocusEvent event) {
		final Component component = event.getComponent();
		if (component instanceof JTextComponent) {
			final JTextComponent textComponent = (JTextComponent) component;
			final int textLength = textComponent.getText().length();
			if (textLength > nbCharsFromRight) {
				SwingUtilities.invokeLater(() -> {
					try {
						Thread.sleep(50);
					} catch (final InterruptedException exception) {
					}
					textComponent.select(
						0,
						textLength - nbCharsFromRight
					);
				});
			}
		}
	}

	@Override
	public void focusLost(final FocusEvent e) {
	}
}
