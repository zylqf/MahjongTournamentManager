/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.config;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.gui.config.item.UIConfigColorProfile;
import fr.bri.mj.gui.config.item.UIConfigDisplayLanguage;
import fr.bri.mj.gui.config.item.UIConfigFontSize;
import fr.bri.mj.gui.config.item.UIConfigNationalMode;
import fr.bri.mj.gui.config.item.UIConfigRuleSet;
import fr.bri.mj.gui.config.item.UIConfigTimerDisplayMode;
import fr.bri.mj.gui.config.item.UIConfigTimerMode;
import fr.bri.mj.gui.config.item.UIConfigVoiceGender;
import fr.bri.mj.gui.config.item.UIConfigVoiceLanguage;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;

public class UIConfigDialog extends JDialog {

	private static final long serialVersionUID = 8992943215217638680L;

	private final UIConfig config;

	private final JRadioButton buttonRCR;
	private final JRadioButton buttonMCR;

	private final JRadioButton buttonDisplayEnglish;
	private final JRadioButton buttonDisplayFrench;

	private final JRadioButton buttonNational;
	private final JRadioButton buttonClub;

	private final JRadioButton buttonColorBOW;
	private final JRadioButton buttonColorWOB;

	private final JRadioButton buttonFontSmall;
	private final JRadioButton buttonFontMedium;
	private final JRadioButton buttonFontBig;

	private final JRadioButton buttonTimerDigital;
	private final JRadioButton buttonTimerAnalog;

	private final JRadioButton buttonTimerHMS;
	private final JRadioButton buttonTimerMS;

	private final JRadioButton buttonVoiceEnglish;
	private final JRadioButton buttonVoiceFrench;
	private final JRadioButton buttonVoiceJapanese;

	private final JRadioButton buttonVoiceMale;
	private final JRadioButton buttonVoiceFemale;

	private final JFrame parent;
	private boolean saveClicked;

	public UIConfigDialog(
		final JFrame parent,
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			parent,
			translator.translate(UIText.GUI_CONFIG_DIALOG_TITLE),
			true
		);
		this.parent = parent;
		this.config = config;

		final Container pane = getContentPane();
		pane.setLayout(new BorderLayout());

		{
			final JPanel centerPanel = new JPanel();
			final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
				14,
				4,
				8,
				8
			);
			centerPanel.setLayout(layout);
			centerPanel.setBorder(
				BorderFactory.createEmptyBorder(
					16,
					16,
					16,
					16
				)
			);

			final ProportionalGridLayoutConstraint constraint = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			{
				constraint.y = 0;
				constraint.x = 0;
				centerPanel.add(
					new JLabel(
						translator.translate(UIText.GUI_CONFIG_RULE_SET_NAME),
						SwingConstants.RIGHT
					),
					constraint
				);

				buttonRCR = new JRadioButton(translator.translate(UIText.GUI_CONFIG_RULE_SET_VALUE_RCR));
				constraint.x = 1;
				centerPanel.add(
					buttonRCR,
					constraint
				);

				buttonMCR = new JRadioButton(translator.translate(UIText.GUI_CONFIG_RULE_SET_VALUE_MCR));
				constraint.x = 2;
				centerPanel.add(
					buttonMCR,
					constraint
				);

				final ButtonGroup buttonRuleSetGroup = new ButtonGroup();
				buttonRuleSetGroup.add(buttonRCR);
				buttonRuleSetGroup.add(buttonMCR);
				switch (config.getRuleSet()) {
					case RCR:
						buttonRuleSetGroup.setSelected(
							buttonRCR.getModel(),
							true
						);
						break;
					case MCR:
						buttonRuleSetGroup.setSelected(
							buttonMCR.getModel(),
							true
						);
						break;
				}
			}

			{
				constraint.y = 2;
				constraint.x = 0;
				centerPanel.add(
					new JLabel(
						translator.translate(UIText.GUI_CONFIG_DISPLAY_LANGUAGE_NAME),
						SwingConstants.RIGHT
					),
					constraint
				);

				buttonDisplayEnglish = new JRadioButton(translator.translate(UIText.GUI_CONFIG_DISPLAY_LANGUAGE_VALUE_ENGLISH));
				constraint.x = 1;
				centerPanel.add(
					buttonDisplayEnglish,
					constraint
				);

				buttonDisplayFrench = new JRadioButton(translator.translate(UIText.GUI_CONFIG_DISPLAY_LANGUAGE_VALUE_FRENCH));
				constraint.x = 2;
				centerPanel.add(
					buttonDisplayFrench,
					constraint
				);

				final ButtonGroup buttonDisplayGroup = new ButtonGroup();
				buttonDisplayGroup.add(buttonDisplayEnglish);
				buttonDisplayGroup.add(buttonDisplayFrench);
				switch (config.getDisplayLanguage()) {
					case ENGLISH:
						buttonDisplayGroup.setSelected(
							buttonDisplayEnglish.getModel(),
							true
						);
						break;
					case FRENCH:
						buttonDisplayGroup.setSelected(
							buttonDisplayFrench.getModel(),
							true
						);
						break;
				}
			}

			{
				constraint.y = 3;
				constraint.x = 0;
				centerPanel.add(
					new JLabel(
						translator.translate(UIText.GUI_CONFIG_NATIONAL_MODE_NAME),
						SwingConstants.RIGHT
					),
					constraint
				);

				buttonNational = new JRadioButton(translator.translate(UIText.GUI_CONFIG_NATIONAL_MODE_VALUE_NATIONALITY));
				constraint.x = 1;
				centerPanel.add(
					buttonNational,
					constraint
				);

				buttonClub = new JRadioButton(translator.translate(UIText.GUI_CONFIG_NATIONAL_MODE_VALUE_CLUB));
				constraint.x = 2;
				centerPanel.add(
					buttonClub,
					constraint
				);

				final ButtonGroup buttonNationModeGroup = new ButtonGroup();
				buttonNationModeGroup.add(buttonNational);
				buttonNationModeGroup.add(buttonClub);
				switch (config.getNationalMode()) {
					case NATIONALITY:
						buttonNationModeGroup.setSelected(
							buttonNational.getModel(),
							true
						);
						break;
					case CLUB_NAME:
						buttonNationModeGroup.setSelected(
							buttonClub.getModel(),
							true
						);
						break;
				}
			}

			{
				constraint.y = 5;
				constraint.x = 0;
				centerPanel.add(
					new JLabel(
						translator.translate(UIText.GUI_CONFIG_COLOR_PROFILE_NAME),
						SwingConstants.RIGHT
					),
					constraint
				);

				buttonColorBOW = new JRadioButton(translator.translate(UIText.GUI_CONFIG_COLOR_PROFILE_VALUE_BLACKONWHITE));
				constraint.x = 1;
				centerPanel.add(
					buttonColorBOW,
					constraint
				);

				buttonColorWOB = new JRadioButton(translator.translate(UIText.GUI_CONFIG_COLOR_PROFILE_VALUE_WHITEONBLACK));
				constraint.x = 2;
				centerPanel.add(
					buttonColorWOB,
					constraint
				);

				final ButtonGroup buttonColorProfileGroup = new ButtonGroup();
				buttonColorProfileGroup.add(buttonColorBOW);
				buttonColorProfileGroup.add(buttonColorWOB);
				switch (config.getColorProfile()) {
					case BLACK_ON_WHITE:
						buttonColorProfileGroup.setSelected(
							buttonColorBOW.getModel(),
							true
						);
						break;
					case WHITE_ON_BLACK:
						buttonColorProfileGroup.setSelected(
							buttonColorWOB.getModel(),
							true
						);
						break;
				}
			}

			{
				constraint.y = 6;
				constraint.x = 0;
				centerPanel.add(
					new JLabel(
						translator.translate(UIText.GUI_CONFIG_FONT_SIZE_NAME),
						SwingConstants.RIGHT
					),
					constraint
				);

				buttonFontSmall = new JRadioButton(translator.translate(UIText.GUI_CONFIG_FONT_SIZE_VALUE_SMALL));
				constraint.x = 1;
				centerPanel.add(
					buttonFontSmall,
					constraint
				);

				buttonFontMedium = new JRadioButton(translator.translate(UIText.GUI_CONFIG_FONT_SIZE_VALUE_MEDIUM));
				constraint.x = 2;
				centerPanel.add(
					buttonFontMedium,
					constraint
				);

				buttonFontBig = new JRadioButton(translator.translate(UIText.GUI_CONFIG_FONT_SIZE_VALUE_BIG));
				constraint.x = 3;
				centerPanel.add(
					buttonFontBig,
					constraint
				);

				final ButtonGroup buttonFontSizeGroup = new ButtonGroup();
				buttonFontSizeGroup.add(buttonFontSmall);
				buttonFontSizeGroup.add(buttonFontMedium);
				buttonFontSizeGroup.add(buttonFontBig);
				switch (config.getFontSize()) {
					case SMALL:
						buttonFontSizeGroup.setSelected(
							buttonFontSmall.getModel(),
							true
						);
						break;
					case MEDIUM:
						buttonFontSizeGroup.setSelected(
							buttonFontMedium.getModel(),
							true
						);
						break;
					case BIG:
						buttonFontSizeGroup.setSelected(
							buttonFontBig.getModel(),
							true
						);
						break;
				}
			}

			{
				constraint.y = 8;
				constraint.x = 0;
				centerPanel.add(
					new JLabel(
						translator.translate(UIText.GUI_CONFIG_TIMER_MODE_NAME),
						SwingConstants.RIGHT
					),
					constraint
				);

				buttonTimerDigital = new JRadioButton(translator.translate(UIText.GUI_CONFIG_TIMER_MODE_VALUE_DIGITAL));
				constraint.x = 1;
				centerPanel.add(
					buttonTimerDigital,
					constraint
				);

				buttonTimerAnalog = new JRadioButton(translator.translate(UIText.GUI_CONFIG_TIMER_MODE_VALUE_ANALOG));
				constraint.x = 2;
				centerPanel.add(
					buttonTimerAnalog,
					constraint
				);

				final ButtonGroup buttonTimerModeGroup = new ButtonGroup();
				buttonTimerModeGroup.add(buttonTimerDigital);
				buttonTimerModeGroup.add(buttonTimerAnalog);
				switch (config.getTimerMode()) {
					case DIGITAL:
						buttonTimerModeGroup.setSelected(
							buttonTimerDigital.getModel(),
							true
						);
						break;
					case ANALOG:
						buttonTimerModeGroup.setSelected(
							buttonTimerAnalog.getModel(),
							true
						);
						break;
				}
			}

			{
				constraint.y = 9;
				constraint.x = 0;
				centerPanel.add(
					new JLabel(
						translator.translate(UIText.GUI_CONFIG_TIMER_DISPLAY_MODE_NAME),
						SwingConstants.RIGHT
					),
					constraint
				);

				buttonTimerHMS = new JRadioButton(translator.translate(UIText.GUI_CONFIG_TIMER_DISPLAY_MODE_VALUE_HMS));
				constraint.x = 1;
				centerPanel.add(
					buttonTimerHMS,
					constraint
				);

				buttonTimerMS = new JRadioButton(translator.translate(UIText.GUI_CONFIG_TIMER_DISPLAY_MODE_VALUE_MS));
				constraint.x = 2;
				centerPanel.add(
					buttonTimerMS,
					constraint
				);

				final ButtonGroup buttonTimerDisplayModeGroup = new ButtonGroup();
				buttonTimerDisplayModeGroup.add(buttonTimerHMS);
				buttonTimerDisplayModeGroup.add(buttonTimerMS);
				switch (config.getTimerDisplayMode()) {
					case HOUR_MINUTE_SECOND:
						buttonTimerDisplayModeGroup.setSelected(
							buttonTimerHMS.getModel(),
							true
						);
						break;
					case MINUTE_SECOND:
						buttonTimerDisplayModeGroup.setSelected(
							buttonTimerMS.getModel(),
							true
						);
						break;
				}
			}

			{
				constraint.y = 10;
				constraint.x = 0;
				centerPanel.add(
					new JLabel(
						translator.translate(UIText.GUI_CONFIG_VOICE_LANGUAGE_NAME),
						SwingConstants.RIGHT
					),
					constraint
				);

				buttonVoiceEnglish = new JRadioButton(translator.translate(UIText.GUI_CONFIG_VOICE_LANGUAGE_VALUE_ENGLISH));
				constraint.x = 1;
				centerPanel.add(
					buttonVoiceEnglish,
					constraint
				);

				buttonVoiceFrench = new JRadioButton(translator.translate(UIText.GUI_CONFIG_VOICE_LANGUAGE_VALUE_FRENCH));
				constraint.x = 2;
				centerPanel.add(
					buttonVoiceFrench,
					constraint
				);

				buttonVoiceJapanese = new JRadioButton(translator.translate(UIText.GUI_CONFIG_VOICE_LANGUAGE_VALUE_JAPANESE));
				constraint.x = 3;
				centerPanel.add(
					buttonVoiceJapanese,
					constraint
				);

				final ButtonGroup buttonVoiceLanguageGroup = new ButtonGroup();
				buttonVoiceLanguageGroup.add(buttonVoiceEnglish);
				buttonVoiceLanguageGroup.add(buttonVoiceFrench);
				buttonVoiceLanguageGroup.add(buttonVoiceJapanese);
				switch (config.getVoiceLanguage()) {
					case ENGLISH:
						buttonVoiceLanguageGroup.setSelected(
							buttonVoiceEnglish.getModel(),
							true
						);
						break;
					case FRENCH:
						buttonVoiceLanguageGroup.setSelected(
							buttonVoiceFrench.getModel(),
							true
						);
						break;
					case JAPANESE:
						buttonVoiceLanguageGroup.setSelected(
							buttonVoiceJapanese.getModel(),
							true
						);
						break;
				}
			}

			{
				constraint.y = 11;
				constraint.x = 0;
				centerPanel.add(
					new JLabel(
						translator.translate(UIText.GUI_CONFIG_VOICE_GENDER_NAME),
						SwingConstants.RIGHT
					),
					constraint
				);

				buttonVoiceMale = new JRadioButton(translator.translate(UIText.GUI_CONFIG_VOICE_GENDER_VALUE_MALE));
				constraint.x = 1;
				centerPanel.add(
					buttonVoiceMale,
					constraint
				);

				buttonVoiceFemale = new JRadioButton(translator.translate(UIText.GUI_CONFIG_VOICE_GENDER_VALUE_FEMALE));
				constraint.x = 2;
				centerPanel.add(
					buttonVoiceFemale,
					constraint
				);

				final ButtonGroup buttonVoiceGenderGroup = new ButtonGroup();
				buttonVoiceGenderGroup.add(buttonVoiceMale);
				buttonVoiceGenderGroup.add(buttonVoiceFemale);
				switch (config.getVoiceGender()) {
					case MALE:
						buttonVoiceGenderGroup.setSelected(
							buttonVoiceMale.getModel(),
							true
						);
						break;
					case FEMALE:
						buttonVoiceGenderGroup.setSelected(
							buttonVoiceFemale.getModel(),
							true
						);
						break;
				}
			}

			pane.add(
				centerPanel,
				BorderLayout.CENTER
			);

		}

		{
			final JPanel southPanel = new JPanel();
			final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
				3,
				5,
				0,
				0
			);
			layout.setWeightY(
				1,
				8,
				1
			);
			southPanel.setLayout(layout);

			final ProportionalGridLayoutConstraint constraint = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			constraint.y = 1;
			constraint.x = 1;
			final JButton buttonSave = new JButton(translator.translate(UIText.GUI_CONFIG_DIALOG_BUTTON_SAVE));
			southPanel.add(
				buttonSave,
				constraint
			);

			constraint.x = 3;
			final JButton buttonCancel = new JButton(translator.translate(UIText.GUI_CONFIG_DIALOG_BUTTON_CANCEL));
			southPanel.add(
				buttonCancel,
				constraint
			);

			pane.add(
				southPanel,
				BorderLayout.SOUTH
			);

			buttonSave.addActionListener((final ActionEvent e) -> {
				saveClicked = true;
				setVisible(false);
			});

			buttonCancel.addActionListener((final ActionEvent e) -> {
				setVisible(false);
			});
		}

		pack();
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setResizable(false);
	}

	public void showDialog() {
		final Dimension size = getSize();
		if (parent != null && (parent.getExtendedState() & Frame.ICONIFIED) == 0) {
			final Point parentLocation = parent.getLocationOnScreen();
			final Dimension parentSize = parent.getSize();
			final int x = parentLocation.x + (parentSize.width - size.width) / 2;
			final int y = parentLocation.y + (parentSize.height - size.height) / 2;
			setLocation(
				x,
				y
			);
		} else {
			final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			final int x = (screenSize.width - size.width) / 2;
			final int y = (screenSize.height - size.height) / 2;
			setLocation(
				x,
				y
			);
		}

		saveClicked = false;
		setVisible(true);
		dispose();
		if (saveClicked) {
			if (buttonDisplayEnglish.isSelected()) {
				config.setDisplayLanguage(UIConfigDisplayLanguage.ENGLISH);
			} else if (buttonDisplayFrench.isSelected()) {
				config.setDisplayLanguage(UIConfigDisplayLanguage.FRENCH);
			}

			if (buttonRCR.isSelected()) {
				config.setRuleSet(UIConfigRuleSet.RCR);
			} else if (buttonMCR.isSelected()) {
				config.setRuleSet(UIConfigRuleSet.MCR);
			}

			if (buttonNational.isSelected()) {
				config.setNationalMode(UIConfigNationalMode.NATIONALITY);
			} else if (buttonClub.isSelected()) {
				config.setNationalMode(UIConfigNationalMode.CLUB_NAME);
			}

			if (buttonTimerDigital.isSelected()) {
				config.setTimerMode(UIConfigTimerMode.DIGITAL);
			} else if (buttonTimerAnalog.isSelected()) {
				config.setTimerMode(UIConfigTimerMode.ANALOG);
			}

			if (buttonTimerHMS.isSelected()) {
				config.setTimerDisplayMode(UIConfigTimerDisplayMode.HOUR_MINUTE_SECOND);
			} else if (buttonTimerMS.isSelected()) {
				config.setTimerDisplayMode(UIConfigTimerDisplayMode.MINUTE_SECOND);
			}

			if (buttonColorBOW.isSelected()) {
				config.setColorProfile(UIConfigColorProfile.BLACK_ON_WHITE);
			} else if (buttonColorWOB.isSelected()) {
				config.setColorProfile(UIConfigColorProfile.WHITE_ON_BLACK);
			}

			if (buttonFontSmall.isSelected()) {
				config.setFontSize(UIConfigFontSize.SMALL);
			} else if (buttonFontMedium.isSelected()) {
				config.setFontSize(UIConfigFontSize.MEDIUM);
			} else if (buttonFontBig.isSelected()) {
				config.setFontSize(UIConfigFontSize.BIG);
			}

			if (buttonVoiceEnglish.isSelected()) {
				config.setVoiceLanguage(UIConfigVoiceLanguage.ENGLISH);
			} else if (buttonVoiceFrench.isSelected()) {
				config.setVoiceLanguage(UIConfigVoiceLanguage.FRENCH);
			} else if (buttonVoiceJapanese.isSelected()) {
				config.setVoiceLanguage(UIConfigVoiceLanguage.JAPANESE);
			}

			if (buttonVoiceMale.isSelected()) {
				config.setVoiceGender(UIConfigVoiceGender.MALE);
			} else if (buttonVoiceFemale.isSelected()) {
				config.setVoiceGender(UIConfigVoiceGender.FEMALE);
			}
		}
	}
}
