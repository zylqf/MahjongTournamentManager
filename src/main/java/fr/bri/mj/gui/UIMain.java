/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.SourceDataLine;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.bri.mj.dataaccess.ManagerFactory;
import fr.bri.mj.gui.common.UITabPanel;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.config.UIConfigDialog;
import fr.bri.mj.gui.dialog.UIMessageDialog;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogMessageType;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOption;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOptionsType;
import fr.bri.mj.gui.icon.ImageLoaderFrameLogo;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.gui.primary.UITabPanelPrimaryFactory;
import fr.bri.mj.gui.secondary.UITabPanelSecondaryFactory;

public class UIMain extends WindowAdapter {

	private static final String WINDOW_TITLE = "M-Unit Mahjong Tournament Manager";

	private static final int WINDOW_WIDTH = 1600;
	private static final int WINDOW_HEIGHT = 900;

	private final ManagerFactory managerFactory;

	private final UIConfig config;
	private final UITextTranslator translator;

	private static final int MAIN_TAB_NUMBERS = 6;

	private final JFrame mainFrame;
	private JMenuItem menuFileExit;
	private JMenuItem menuConfigConfig;
	private final JTabbedPane mainTabbedPane;
	private UITabPanel[] mainTabs;
	private int currentMainTabIndex;

	private static final int EXTRA_TAB_NUMBERS = 6;

	private final JFrame extraFrame;
	private final JTabbedPane extraTabbedPane;
	private UITabPanel[] extraTabs;
	private int currentExtraTabIndex;

	private final ChangeListener mainPanelTabbedPaneChangeListener;
	private final ChangeListener extraPanelTabbedPaneChangeListener;
	private final ChangeListener refreshSoftExtraPanelChangeListener;
	private final ChangeListener refreshHardAllPanelChangeListener;
	private final ChangeListener remoteRefreshExtraPanelChangeListener;
	private boolean isChanging;

	private volatile Thread backgroundNoiseThread;
	private volatile boolean stopBackgroundNoiseThread;

	public UIMain(final ManagerFactory managerFactory) {
		this.managerFactory = managerFactory;

		managerFactory.getConnectionManager().connect();
		config = managerFactory.getConfigurationManager().getUIConfig();
		translator = new UITextTranslator(config);

		{
			mainFrame = new JFrame(WINDOW_TITLE);
			final Container mainPane = mainFrame.getContentPane();
			mainPane.setLayout(new BorderLayout());
			mainTabbedPane = new JTabbedPane(
				SwingConstants.TOP,
				JTabbedPane.SCROLL_TAB_LAYOUT
			);
			mainPane.add(
				mainTabbedPane,
				BorderLayout.CENTER
			);

			mainTabs = new UITabPanel[MAIN_TAB_NUMBERS];

			mainFrame.addWindowListener(this);
			mainFrame.setMinimumSize(
				new Dimension(
					WINDOW_WIDTH,
					WINDOW_HEIGHT
				)
			);
			mainFrame.setPreferredSize(
				new Dimension(
					WINDOW_WIDTH,
					WINDOW_HEIGHT
				)
			);
			mainFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		}

		{
			extraFrame = new JFrame(WINDOW_TITLE);
			final Container extraContentPane = extraFrame.getContentPane();
			extraContentPane.setLayout(new BorderLayout());
			extraTabbedPane = new JTabbedPane(
				SwingConstants.TOP,
				JTabbedPane.SCROLL_TAB_LAYOUT
			);
			extraContentPane.add(
				extraTabbedPane,
				BorderLayout.CENTER
			);

			extraTabs = new UITabPanel[EXTRA_TAB_NUMBERS];

			extraFrame.addWindowListener(this);
			extraFrame.setMinimumSize(
				new Dimension(
					WINDOW_WIDTH,
					WINDOW_HEIGHT
				)
			);
			extraFrame.setPreferredSize(
				new Dimension(
					WINDOW_WIDTH,
					WINDOW_HEIGHT
				)
			);
			extraFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		}

		{
			mainPanelTabbedPaneChangeListener = (final ChangeEvent e) -> {
				changeMainTab();
			};
			extraPanelTabbedPaneChangeListener = (final ChangeEvent e) -> {
				changeExtraTab();
			};
			refreshSoftExtraPanelChangeListener = (final ChangeEvent e) -> {
				refreshExtraPanel(false);
			};
			refreshHardAllPanelChangeListener = (final ChangeEvent e) -> {
				refreshMainPanel(true);
				refreshExtraPanel(true);
			};
			remoteRefreshExtraPanelChangeListener = (final ChangeEvent e) -> {
				remoteRefreshExtraPanel();
			};
		}

		mainFrame.setIconImages(ImageLoaderFrameLogo.getIconImageFrameLogo());
		extraFrame.setIconImages(ImageLoaderFrameLogo.getIconImageFrameLogo());

		currentMainTabIndex = 0;
		currentExtraTabIndex = 0;
		isChanging = false;
		changePanels();

		mainFrame.pack();
		extraFrame.pack();

		startBackgroundNoise();

		mainFrame.setVisible(true);
		extraFrame.setVisible(true);
		extraFrame.setLocation(
			32,
			32
		);
	}

	private void changeMainTab() {
		if (!isChanging) {
			isChanging = true;
			boolean changePanel = true;
			if (mainTabs[currentMainTabIndex].hasUnsavedChanges()) {
				final MessageDialogOption answer = new UIMessageDialog(
					mainFrame,
					translator.translate(UIText.GUI_MAIN_MESSAGE_CHANGE_TAB_CONFIRM_MESSAGE),
					translator.translate(UIText.GUI_MAIN_MESSAGE_CHANGE_TAB_CONFIRM_TITLE),
					MessageDialogMessageType.QUESTION,
					MessageDialogOptionsType.YES_NO,
					MessageDialogOption.NO,
					translator
				).showOptionDialog();
				changePanel = answer == MessageDialogOption.YES;
			}
			if (changePanel) {
				currentMainTabIndex = mainTabbedPane.getSelectedIndex();
				refreshMainPanel(false);
			} else {
				mainTabbedPane.setSelectedIndex(currentMainTabIndex);
			}
			isChanging = false;
		}
	}

	private void refreshMainPanel(final boolean hard) {
		if (hard) {
			for (int index = 0; index < mainTabs.length; index++) {
				mainTabs[index].refresh(hard);
			}
		} else {
			mainTabs[currentMainTabIndex].refresh(hard);
		}
	}

	private void changeExtraTab() {
		if (!isChanging) {
			isChanging = true;
			boolean changePanel = true;
			if (extraTabs[currentExtraTabIndex].hasUnsavedChanges()) {
				final MessageDialogOption answer = new UIMessageDialog(
					extraFrame,
					translator.translate(UIText.GUI_MAIN_MESSAGE_CHANGE_TAB_CONFIRM_MESSAGE),
					translator.translate(UIText.GUI_MAIN_MESSAGE_CHANGE_TAB_CONFIRM_TITLE),
					MessageDialogMessageType.QUESTION,
					MessageDialogOptionsType.YES_NO,
					MessageDialogOption.NO,
					translator
				).showOptionDialog();
				changePanel = answer == MessageDialogOption.YES;
			}
			if (changePanel) {
				currentExtraTabIndex = extraTabbedPane.getSelectedIndex();
				refreshExtraPanel(false);
			} else {
				extraTabbedPane.setSelectedIndex(currentExtraTabIndex);
			}
			isChanging = false;
		}
	}

	private void refreshExtraPanel(final boolean hard) {
		if (hard) {
			for (int index = 0; index < extraTabs.length; index++) {
				extraTabs[index].refresh(hard);
			}
		} else {
			currentExtraTabIndex = extraTabbedPane.getSelectedIndex();
			extraTabs[currentExtraTabIndex].refresh(hard);
		}
	}

	private void remoteRefreshExtraPanel() {
		final int selectedExtraTabIndex = extraTabbedPane.getSelectedIndex();
		if (selectedExtraTabIndex >= 0 && selectedExtraTabIndex < extraTabs.length) {
			extraTabs[selectedExtraTabIndex].remoteRefresh();
		}
	}

	private void changeConfig() {
		config.resetChanges();
		final UIConfigDialog dialog = new UIConfigDialog(
			mainFrame,
			config,
			translator
		);
		dialog.showDialog();
		changePanels();
	}

	private void changePanels() {
		if (config.isMajorChange()) {
			{
				final JMenuBar menuBar = new JMenuBar();
				mainFrame.setJMenuBar(menuBar);

				final JMenu menuFile = new JMenu(translator.translate(UIText.GUI_MAIN_MENU_FILE));
				menuBar.add(menuFile);

				menuFileExit = new JMenuItem(translator.translate(UIText.GUI_MAIN_MENU_FILE_EXIT));
				menuFile.add(menuFileExit);

				final JMenu menuConfig = new JMenu(translator.translate(UIText.GUI_MAIN_MENU_CONFIG));
				menuBar.add(menuConfig);

				menuConfigConfig = new JMenuItem(translator.translate(UIText.GUI_MAIN_MENU_CONFIG_CONFIG));
				menuConfig.add(menuConfigConfig);

				menuFileExit.addActionListener((final ActionEvent e) -> {
					exit(mainFrame);
				});
				menuConfigConfig.addActionListener((final ActionEvent e) -> {
					changeConfig();
				});
			}

			{
				if (mainTabs[0] != null) {
					mainTabs[0].removeChangeListener(refreshHardAllPanelChangeListener);
				}
				if (mainTabs[1] != null) {
					mainTabs[1].removeChangeListener(refreshSoftExtraPanelChangeListener);
				}
				if (mainTabs[2] != null) {
					mainTabs[2].removeChangeListener(refreshHardAllPanelChangeListener);
				}
				if (mainTabs[3] != null) {
					mainTabs[3].removeChangeListener(refreshSoftExtraPanelChangeListener);
				}
				if (mainTabs[4] != null) {
					mainTabs[4].removeChangeListener(remoteRefreshExtraPanelChangeListener);
				}

				for (int extraTabIndex = 0; extraTabIndex < EXTRA_TAB_NUMBERS; extraTabIndex++) {
					if (extraTabs[extraTabIndex] != null) {
						extraTabs[extraTabIndex].stopRunningThreads();
					}
				}
			}

			{
				mainTabs[0] = UITabPanelPrimaryFactory.getTabPanelPlayer(
					managerFactory,
					config,
					translator
				);
				mainTabs[1] = UITabPanelPrimaryFactory.getTabPanelNewTournament(
					managerFactory,
					config,
					translator
				);
				mainTabs[2] = UITabPanelPrimaryFactory.getTabPanelManagementTournament(
					managerFactory,
					config,
					translator
				);
				mainTabs[3] = UITabPanelPrimaryFactory.getTabPanelManagementTournamentPlayer(
					managerFactory,
					config,
					translator
				);
				mainTabs[4] = UITabPanelPrimaryFactory.getTabPanelInputScore(
					managerFactory,
					config,
					translator
				);
				mainTabs[5] = UITabPanelPrimaryFactory.getTabPanelDisplayScore(
					managerFactory,
					config,
					translator
				);

				mainTabs[0].addChangeListener(refreshSoftExtraPanelChangeListener);
				mainTabs[1].addChangeListener(refreshSoftExtraPanelChangeListener);
				mainTabs[2].addChangeListener(refreshHardAllPanelChangeListener);
				mainTabs[3].addChangeListener(refreshSoftExtraPanelChangeListener);
				mainTabs[4].addChangeListener(remoteRefreshExtraPanelChangeListener);
			}

			{
				extraTabs[0] = UITabPanelSecondaryFactory.getTabPanelTimer(
					config,
					translator
				);
				extraTabs[1] = UITabPanelSecondaryFactory.getTabPanelPlan(
					managerFactory,
					config,
					translator
				);
				extraTabs[2] = UITabPanelSecondaryFactory.getTabPanelRanking(
					managerFactory,
					config,
					translator
				);
				extraTabs[3] = UITabPanelSecondaryFactory.getTabPanelLeague(
					managerFactory,
					config,
					translator
				);
				extraTabs[4] = UITabPanelSecondaryFactory.getTabPanelScore(
					managerFactory,
					config,
					translator
				);
				extraTabs[5] = UITabPanelSecondaryFactory.getTabPanelTrend(
					managerFactory,
					config,
					translator
				);
			}

			mainTabbedPane.removeChangeListener(mainPanelTabbedPaneChangeListener);
			mainTabbedPane.removeAll();

			extraTabbedPane.removeChangeListener(extraPanelTabbedPaneChangeListener);
			extraTabbedPane.removeAll();

			for (int index = 0; index < mainTabs.length; index++) {
				mainTabbedPane.insertTab(
					mainTabs[index].getTabName(),
					null,
					mainTabs[index],
					null,
					index
				);
			}

			for (int index = 0; index < extraTabs.length; index++) {
				extraTabbedPane.insertTab(
					extraTabs[index].getTabName(),
					null,
					extraTabs[index],
					null,
					index
				);
			}

			mainTabbedPane.addChangeListener(mainPanelTabbedPaneChangeListener);
			mainTabbedPane.setSelectedIndex(currentMainTabIndex);
			if (currentMainTabIndex == 0) {
				mainTabs[currentMainTabIndex].refresh(false);
			}

			extraTabbedPane.addChangeListener(extraPanelTabbedPaneChangeListener);
			extraTabbedPane.setSelectedIndex(currentExtraTabIndex);
			if (currentExtraTabIndex == 0) {
				extraTabs[currentExtraTabIndex].refresh(false);
			}
		}

		if (config.isMinorChange()) {
			for (int indexTab = 0; indexTab < extraTabs.length; indexTab++) {
				extraTabs[indexTab].refresh(false);
			}
		}
	}

	private static final int SAMPLE_FREQUENCY = 44100;
	private static final int SAMPLE_DEPTH = 16;
	private static final int SAMPLE_IN_BYTE = SAMPLE_DEPTH / 8;
	private static final int SAMPLE_CHANNEL = 1;

	private static final int BASE_FREQUENCY = 20;
	private static final int BASE_SAMPLE_PER_CYCLE = SAMPLE_FREQUENCY / BASE_FREQUENCY;

	private void startBackgroundNoise() {
		if (backgroundNoiseThread == null) {
			stopBackgroundNoiseThread = false;
			backgroundNoiseThread = new Thread(() -> {
				final ByteBuffer byteBuffer = ByteBuffer.allocate(SAMPLE_FREQUENCY * SAMPLE_IN_BYTE * SAMPLE_CHANNEL);
				for (int sampleIndex = 0; sampleIndex < SAMPLE_FREQUENCY; sampleIndex++) {
					final int basePositionInCycle = sampleIndex % BASE_SAMPLE_PER_CYCLE;
					final double basePositionInCycleRadius = Math.PI * 2.0 * basePositionInCycle / BASE_SAMPLE_PER_CYCLE;
					byteBuffer.putShort((short) Math.round(10.0 * Math.sin(basePositionInCycleRadius)));
				}
				final byte[] buffer = byteBuffer.array();
				try {
					final AudioFormat audioFormat = new AudioFormat(
						SAMPLE_FREQUENCY,
						SAMPLE_DEPTH,
						SAMPLE_CHANNEL,
						true,
						byteBuffer.order() == ByteOrder.BIG_ENDIAN
					);
					final DataLine.Info info = new DataLine.Info(
						SourceDataLine.class,
						audioFormat
					);
					final SourceDataLine sourceLine = (SourceDataLine) AudioSystem.getLine(info);
					sourceLine.open(audioFormat);
					sourceLine.start();
					while (!stopBackgroundNoiseThread) {
						sourceLine.write(
							buffer,
							0,
							buffer.length
						);
					}
					sourceLine.stop();
					sourceLine.close();
				} catch (final Exception e) {
					e.printStackTrace();
				}
				backgroundNoiseThread = null;
			});
			backgroundNoiseThread.start();
		}
	}

	private void stopBackgroundNoise() {
		if (backgroundNoiseThread != null) {
			stopBackgroundNoiseThread = true;
		}
	}

	private void exit(final Component component) {
		JFrame parentWindow = null;
		if (component == mainFrame) {
			parentWindow = mainFrame;
		} else if (component == extraFrame) {
			parentWindow = extraFrame;
		}
		final MessageDialogOption answer = new UIMessageDialog(
			parentWindow,
			translator.translate(UIText.GUI_MAIN_MESSAGE_EXIT_CONFIRM_MESSAGE),
			translator.translate(UIText.GUI_MAIN_MESSAGE_EXIT_CONFIRM_TITLE),
			MessageDialogMessageType.QUESTION,
			MessageDialogOptionsType.YES_NO,
			MessageDialogOption.NO,
			translator
		).showOptionDialog();
		if (answer == MessageDialogOption.YES) {
			stopBackgroundNoise();

			for (int indexTab = 0; indexTab < mainTabs.length; indexTab++) {
				mainTabs[indexTab].stopRunningThreads();
			}
			for (int indexTab = 0; indexTab < extraTabs.length; indexTab++) {
				extraTabs[indexTab].stopRunningThreads();
			}

			managerFactory.getConfigurationManager().saveUIConfig(config);
			managerFactory.getConnectionManager().disconnect();
			extraFrame.dispose();
			mainFrame.dispose();
		}
	}

	@Override
	public void windowClosing(final WindowEvent e) {
		exit(e.getComponent());
	}
}
