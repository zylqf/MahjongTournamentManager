/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.primary.inputscore.rcr;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.data.tournament.rcr.ScoreInfoRCR;
import fr.bri.mj.data.tournament.rcr.ScoreRCR;
import fr.bri.mj.data.tournament.rcr.TournamentScoreRCR;
import fr.bri.mj.dataaccess.TournamentManager;
import fr.bri.mj.gui.common.SelectAllTextFocusListener;
import fr.bri.mj.gui.common.UITabPanelWithWaterMark;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.dialog.UIMessageDialog;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogMessageType;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOption;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOptionsType;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;

public class UITabPanelInputScoreRCR extends UITabPanelWithWaterMark {

	private static final long serialVersionUID = -7034346983120181130L;

	private static final Color UNSAVED_BACKGROUND_COLOR = new Color(
		255,
		191,
		191
	);

	private static final int NUMBER_OF_PLAYERS = 4;
	private static final UIText STRING_SUBSTITUTION[] = {
		UIText.GUI_PRIMARY_INPUT_SCORE_RCR_SUBSTITUTION_NORMAL,
		UIText.GUI_PRIMARY_INPUT_SCORE_RCR_SUBSTITUTION_PARTIAL,
		UIText.GUI_PRIMARY_INPUT_SCORE_RCR_SUBSTITUTION_COMPLETE
	};

	private static final ScoreInfoRCR SCORE_INFO = ScoreInfoRCR.SCORE_INFO_RCR_15K;

	private final TournamentManager<TournamentScoreRCR, ScoreRCR> tournamentManager;

	private final JComboBox<String> comboTournament;
	private final JSpinner spinnerInitialGameScore;
	private final JComboBox<String> comboSession;
	private final JComboBox<String> comboTable;

	private final JPanel southPanel;

	private final List<JLabel> labelPlayerRankings;
	private final List<JLabel> labelPlayers;
	private final List<JComboBox<String>> comboBoxSubstitutions;
	private final List<JSpinner> spinnerPlayerScores;
	private final List<JSpinner> spinnerPlayerBorrows;
	private final List<JLabel> labelPlayerNetScores;
	private final List<JLabel> labelPlayerUmaScores;
	private final List<JSpinner> spinnerPlayerPenalties;
	private final List<JLabel> labelPlayerFinalScores;

	private final ActionListener tournamentComboBoxActionListener;
	private final ActionListener sessionTableComboBoxActionListener;
	private final List<ActionListener> substitutionComboBoxActionListeners;

	private final JButton buttonCalculate;
	private final JButton buttonReset;
	private final JButton buttonSave;
	private final JLabel labelScoreError;

	private final List<Tournament<TournamentScoreRCR, ScoreRCR>> listTournament;
	private final List<TournamentScoreRCR> listCurrentTables;
	private final List<TournamentScoreRCR> listCurrentTablesTemp;

	private boolean unsaved;

	public UITabPanelInputScoreRCR(
		final TournamentManager<TournamentScoreRCR, ScoreRCR> tournamentManager,
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			config,
			translator
		);
		this.tournamentManager = tournamentManager;

		final JPanel innerPanel = new JPanel();
		innerPanel.setLayout(
			new BorderLayout(
				12,
				12
			)
		);
		{
			final JPanel northPanel = new JPanel(
				new ProportionalGridLayoutInteger(
					2,
					8,
					4,
					4
				)
			);
			final ProportionalGridLayoutConstraint northPanelConstraint = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			northPanelConstraint.y = 0;
			northPanelConstraint.x = 2;
			northPanelConstraint.gridWidth = 1;
			northPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_TITLE_TOURNAMENT),
					SwingConstants.RIGHT
				),
				northPanelConstraint
			);
			comboTournament = new JComboBox<String>();
			northPanelConstraint.x = 3;
			northPanelConstraint.gridWidth = 3;
			northPanel.add(
				comboTournament,
				northPanelConstraint
			);

			northPanelConstraint.y = 1;
			northPanelConstraint.x = 1;
			northPanelConstraint.gridWidth = 1;
			northPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_TITLE_INITIAL_SCORE),
					SwingConstants.RIGHT
				),
				northPanelConstraint
			);
			northPanelConstraint.x = 2;
			northPanelConstraint.gridWidth = 1;
			spinnerInitialGameScore = new JSpinner(
				new SpinnerNumberModel(
					30000,
					0,
					40000,
					5000
				)
			);
			spinnerInitialGameScore.setEditor(
				new JSpinner.NumberEditor(
					spinnerInitialGameScore,
					"000"
				)
			);
			northPanel.add(
				spinnerInitialGameScore,
				northPanelConstraint
			);

			northPanelConstraint.x = 3;
			northPanelConstraint.gridWidth = 1;
			northPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_TITLE_SESSION),
					SwingConstants.RIGHT
				),
				northPanelConstraint
			);
			comboSession = new JComboBox<String>();
			northPanelConstraint.x = 4;
			northPanelConstraint.gridWidth = 1;
			northPanel.add(
				comboSession,
				northPanelConstraint
			);

			northPanelConstraint.x = 5;
			northPanelConstraint.gridWidth = 1;
			northPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_TITLE_TABLE),
					SwingConstants.RIGHT
				),
				northPanelConstraint
			);
			comboTable = new JComboBox<String>();
			northPanelConstraint.x = 6;
			northPanelConstraint.gridWidth = 1;
			northPanel.add(
				comboTable,
				northPanelConstraint
			);

			innerPanel.add(
				northPanel,
				BorderLayout.NORTH
			);
		}

		{
			final JPanel centerPanel = new JPanel();
			centerPanel.setLayout(
				new ProportionalGridLayoutInteger(
					6,
					10,
					4,
					4
				)
			);
			final ProportionalGridLayoutConstraint centerPanelConstraint = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);
			final Dimension labelSize = new Dimension(
				112,
				32
			);

			centerPanelConstraint.y = 0;
			centerPanelConstraint.x = 0;
			centerPanelConstraint.gridWidth = 1;
			centerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_RCR_TITLE_RANKING),
					SwingConstants.CENTER
				),
				centerPanelConstraint
			);

			centerPanelConstraint.x = 1;
			centerPanelConstraint.gridWidth = 2;
			centerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_RCR_TITLE_PLAYER),
					SwingConstants.CENTER
				),
				centerPanelConstraint
			);

			centerPanelConstraint.x = 3;
			centerPanelConstraint.gridWidth = 1;
			centerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_RCR_TITLE_SUBSTITUTED),
					SwingConstants.CENTER
				),
				centerPanelConstraint
			);

			centerPanelConstraint.x = 4;
			centerPanelConstraint.gridWidth = 1;
			centerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_RCR_TITLE_GAME_SCORE),
					SwingConstants.CENTER
				),
				centerPanelConstraint
			);

			centerPanelConstraint.x = 5;
			centerPanelConstraint.gridWidth = 1;
			centerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_RCR_TITLE_BORROW),
					SwingConstants.CENTER
				),
				centerPanelConstraint
			);

			centerPanelConstraint.x = 6;
			centerPanelConstraint.gridWidth = 1;
			centerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_RCR_TITLE_NET_SCORE),
					SwingConstants.CENTER
				),
				centerPanelConstraint
			);

			centerPanelConstraint.x = 7;
			centerPanelConstraint.gridWidth = 1;
			centerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_RCR_TITLE_UMA),
					SwingConstants.CENTER
				),
				centerPanelConstraint
			);

			centerPanelConstraint.x = 8;
			centerPanelConstraint.gridWidth = 1;
			centerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_RCR_TITLE_PENALTY),
					SwingConstants.CENTER
				),
				centerPanelConstraint
			);

			centerPanelConstraint.x = 9;
			centerPanelConstraint.gridWidth = 1;
			centerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_RCR_TITLE_FINAL_SCORE),
					SwingConstants.CENTER
				),
				centerPanelConstraint
			);

			labelPlayerRankings = new ArrayList<JLabel>(NUMBER_OF_PLAYERS);
			labelPlayers = new ArrayList<JLabel>(NUMBER_OF_PLAYERS);
			comboBoxSubstitutions = new ArrayList<JComboBox<String>>(NUMBER_OF_PLAYERS);
			spinnerPlayerScores = new ArrayList<JSpinner>(NUMBER_OF_PLAYERS);
			spinnerPlayerBorrows = new ArrayList<JSpinner>(NUMBER_OF_PLAYERS);
			labelPlayerNetScores = new ArrayList<JLabel>(NUMBER_OF_PLAYERS);
			labelPlayerUmaScores = new ArrayList<JLabel>(NUMBER_OF_PLAYERS);
			spinnerPlayerPenalties = new ArrayList<JSpinner>(NUMBER_OF_PLAYERS);
			labelPlayerFinalScores = new ArrayList<JLabel>(NUMBER_OF_PLAYERS);
			final ChangeListener scoreSpinnerChangeListener = (final ChangeEvent e) -> {
				hideSaveButton();
			};
			final FocusListener spinnerPlayerScoreFocusListener = new SelectAllTextFocusListener(2);
			final FocusListener spinnerPlayerBorrowFocusListener = new SelectAllTextFocusListener(4);
			final FocusListener spinnerPlayerPenaltyFocusListener = new SelectAllTextFocusListener(3);

			for (int playerIndex = 0; playerIndex < NUMBER_OF_PLAYERS; playerIndex++) {
				centerPanelConstraint.y = 1 + playerIndex;

				final JLabel labelPlayerRanking = new JLabel(
					"",
					SwingConstants.CENTER
				);
				labelPlayerRanking.setBorder(
					BorderFactory.createCompoundBorder(
						BorderFactory.createLoweredSoftBevelBorder(),
						BorderFactory.createEmptyBorder(
							0,
							4,
							0,
							4
						)
					)
				);
				labelPlayerRanking.setPreferredSize(labelSize);
				labelPlayerRankings.add(
					playerIndex,
					labelPlayerRanking
				);
				centerPanelConstraint.x = 0;
				centerPanelConstraint.gridWidth = 1;
				centerPanel.add(
					labelPlayerRanking,
					centerPanelConstraint
				);

				final JLabel labelPlayer = new JLabel();
				labelPlayers.add(
					playerIndex,
					labelPlayer
				);
				labelPlayer.setBorder(
					BorderFactory.createCompoundBorder(
						BorderFactory.createLoweredSoftBevelBorder(),
						BorderFactory.createEmptyBorder(
							0,
							4,
							0,
							4
						)
					)
				);
				centerPanelConstraint.x = 1;
				centerPanelConstraint.gridWidth = 2;
				centerPanel.add(
					labelPlayer,
					centerPanelConstraint
				);

				final String[] substitutions = new String[STRING_SUBSTITUTION.length];
				for (int index = 0; index < STRING_SUBSTITUTION.length; index++) {
					substitutions[index] = translator.translate(STRING_SUBSTITUTION[index]);
				}
				final JComboBox<String> comboBoxSubstitution = new JComboBox<String>(substitutions);
				comboBoxSubstitutions.add(
					playerIndex,
					comboBoxSubstitution
				);
				centerPanelConstraint.x = 3;
				centerPanelConstraint.gridWidth = 1;
				centerPanel.add(
					comboBoxSubstitution,
					centerPanelConstraint
				);

				final JSpinner spinnerPlayerScore = new JSpinner(
					new SpinnerNumberModel(
						0,
						-150000,
						150000,
						100
					)
				);
				final JSpinner.NumberEditor scoreNumberEditor = new JSpinner.NumberEditor(
					spinnerPlayerScore,
					"00"
				);
				scoreNumberEditor.getTextField().addFocusListener(spinnerPlayerScoreFocusListener);
				spinnerPlayerScore.setEditor(scoreNumberEditor);
				spinnerPlayerScores.add(
					playerIndex,
					spinnerPlayerScore
				);
				spinnerPlayerScore.addChangeListener(scoreSpinnerChangeListener);
				centerPanelConstraint.x = 4;
				centerPanelConstraint.gridWidth = 1;
				centerPanel.add(
					spinnerPlayerScore,
					centerPanelConstraint
				);

				final JSpinner spinnerPlayerBorrow = new JSpinner(
					new SpinnerNumberModel(
						0,
						0,
						100000,
						20000
					)
				);
				final JSpinner.NumberEditor borrowNumberEditor = new JSpinner.NumberEditor(
					spinnerPlayerBorrow,
					"0000"
				);
				borrowNumberEditor.getTextField().addFocusListener(spinnerPlayerBorrowFocusListener);
				spinnerPlayerBorrow.setEditor(borrowNumberEditor);
				spinnerPlayerBorrows.add(
					playerIndex,
					spinnerPlayerBorrow
				);
				spinnerPlayerBorrow.addChangeListener(scoreSpinnerChangeListener);
				centerPanelConstraint.x = 5;
				centerPanelConstraint.gridWidth = 1;
				centerPanel.add(
					spinnerPlayerBorrow,
					centerPanelConstraint
				);

				final JLabel labelPlayerNetScore = new JLabel(
					"",
					SwingConstants.RIGHT
				);
				labelPlayerNetScore.setBorder(
					BorderFactory.createCompoundBorder(
						BorderFactory.createLoweredSoftBevelBorder(),
						BorderFactory.createEmptyBorder(
							0,
							4,
							0,
							4
						)
					)
				);
				labelPlayerNetScore.setPreferredSize(labelSize);
				labelPlayerNetScores.add(
					playerIndex,
					labelPlayerNetScore
				);
				centerPanelConstraint.x = 6;
				centerPanelConstraint.gridWidth = 1;
				centerPanel.add(
					labelPlayerNetScore,
					centerPanelConstraint
				);

				final JLabel labelPlayerUmaScore = new JLabel(
					"",
					SwingConstants.RIGHT
				);
				labelPlayerUmaScore.setBorder(
					BorderFactory.createCompoundBorder(
						BorderFactory.createLoweredSoftBevelBorder(),
						BorderFactory.createEmptyBorder(
							0,
							4,
							0,
							4
						)
					)
				);
				labelPlayerUmaScore.setPreferredSize(labelSize);
				labelPlayerUmaScores.add(
					playerIndex,
					labelPlayerUmaScore
				);
				centerPanelConstraint.x = 7;
				centerPanelConstraint.gridWidth = 1;
				centerPanel.add(
					labelPlayerUmaScore,
					centerPanelConstraint
				);

				final JSpinner spinnerPlayerPenalty = new JSpinner(
					new SpinnerNumberModel(
						0,
						0,
						100000,
						1000
					)
				);
				final JSpinner.NumberEditor penaltyNumberEditor = new JSpinner.NumberEditor(
					spinnerPlayerPenalty,
					"000"
				);
				penaltyNumberEditor.getTextField().addFocusListener(spinnerPlayerPenaltyFocusListener);
				spinnerPlayerPenalty.setEditor(penaltyNumberEditor);
				spinnerPlayerPenalties.add(
					playerIndex,
					spinnerPlayerPenalty
				);
				spinnerPlayerPenalty.addChangeListener(scoreSpinnerChangeListener);
				centerPanelConstraint.x = 8;
				centerPanelConstraint.gridWidth = 1;
				centerPanel.add(
					spinnerPlayerPenalty,
					centerPanelConstraint
				);

				final JLabel labelPlayerFinalScore = new JLabel(
					"",
					SwingConstants.RIGHT
				);
				labelPlayerFinalScore.setBorder(
					BorderFactory.createCompoundBorder(
						BorderFactory.createLoweredSoftBevelBorder(),
						BorderFactory.createEmptyBorder(
							0,
							4,
							0,
							4
						)
					)
				);
				labelPlayerFinalScore.setPreferredSize(labelSize);
				labelPlayerFinalScores.add(
					playerIndex,
					labelPlayerFinalScore
				);
				centerPanelConstraint.x = 9;
				centerPanelConstraint.gridWidth = 1;
				centerPanel.add(
					labelPlayerFinalScore,
					centerPanelConstraint
				);
			}

			labelScoreError = new JLabel(
				"",
				SwingConstants.RIGHT
			);
			labelScoreError.setBorder(
				BorderFactory.createCompoundBorder(
					BorderFactory.createLineBorder(Color.BLACK),
					BorderFactory.createEmptyBorder(
						0,
						4,
						0,
						4
					)
				)
			);
			labelScoreError.setForeground(Color.RED);
			centerPanelConstraint.y = 5;
			centerPanelConstraint.x = 4;
			centerPanelConstraint.gridWidth = 1;
			centerPanel.add(
				labelScoreError,
				centerPanelConstraint
			);

			innerPanel.add(
				centerPanel,
				BorderLayout.CENTER
			);
		}

		{
			southPanel = new JPanel();
			southPanel.setOpaque(false);
			southPanel.setBackground(UNSAVED_BACKGROUND_COLOR);
			southPanel.setLayout(
				new ProportionalGridLayoutInteger(
					1,
					7,
					0,
					0
				)
			);
			final ProportionalGridLayoutConstraint southPanelConstraint = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			buttonCalculate = new JButton(translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_BUTTON_CALCULATE));
			southPanelConstraint.x = 1;
			southPanel.add(
				buttonCalculate,
				southPanelConstraint
			);

			buttonSave = new JButton(translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_BUTTON_SAVE));
			southPanelConstraint.x = 3;
			southPanel.add(
				buttonSave,
				southPanelConstraint
			);

			buttonReset = new JButton(translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_BUTTON_RESET));
			southPanelConstraint.x = 5;
			southPanel.add(
				buttonReset,
				southPanelConstraint
			);

			innerPanel.add(
				southPanel,
				BorderLayout.SOUTH
			);
		}

		setLayout(new GridBagLayout());
		final GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.anchor = GridBagConstraints.NORTH;
		c.fill = GridBagConstraints.NONE;
		add(
			innerPanel,
			c
		);

		listTournament = new ArrayList<Tournament<TournamentScoreRCR, ScoreRCR>>();
		listCurrentTables = new ArrayList<TournamentScoreRCR>();
		listCurrentTablesTemp = new ArrayList<TournamentScoreRCR>();

		tournamentComboBoxActionListener = (final ActionEvent e) -> {
			changeTournament();
		};
		sessionTableComboBoxActionListener = (final ActionEvent e) -> {
			changeTable();
		};
		substitutionComboBoxActionListeners = new ArrayList<ActionListener>(4);

		for (int index = 0; index < comboBoxSubstitutions.size(); index++) {
			final int playerIndex = index;
			final ActionListener substitutionComboBoxActionListener = (final ActionEvent e) -> {
				SwingUtilities.invokeLater(() -> {
					changePlayerSubstitution(playerIndex);
				});
			};
			substitutionComboBoxActionListeners.add(substitutionComboBoxActionListener);
		}

		buttonCalculate.addActionListener((final ActionEvent e) -> {
			calculateFinalScore();
		});
		buttonSave.addActionListener((final ActionEvent e) -> {
			saveScore();
		});
		buttonReset.addActionListener((final ActionEvent e) -> {
			reset();
		});
	}

	@Override
	public String getTabName() {
		return translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_TAB_NAME);
	}

	@Override
	public void remoteRefresh() {
		refresh(false);
	}

	@Override
	public boolean hasUnsavedChanges() {
		return unsaved;
	}

	@Override
	public void refresh(final boolean hard) {
		int selectedTournamentIndex = comboTournament.getSelectedIndex();
		Tournament<TournamentScoreRCR, ScoreRCR> selectedTournament = null;
		if (selectedTournamentIndex >= 0) {
			selectedTournament = listTournament.get(selectedTournamentIndex);
		}
		comboTournament.removeActionListener(tournamentComboBoxActionListener);
		comboTournament.removeAllItems();
		listTournament.clear();
		for (final Tournament<TournamentScoreRCR, ScoreRCR> tournament : tournamentManager.getAllTournament()) {
			if (!tournament.isArchived()) {
				listTournament.add(tournament);
			}
		}

		if (listTournament.size() > 0) {
			Collections.sort(listTournament);

			selectedTournamentIndex = -1;
			for (int index = 0; index < listTournament.size(); index++) {
				final Tournament<TournamentScoreRCR, ScoreRCR> tournament = listTournament.get(index);
				if (tournament == selectedTournament) {
					selectedTournamentIndex = index;
				}
				comboTournament.addItem(tournament.getName());
			}

			if (selectedTournamentIndex >= 0) {
				comboTournament.setSelectedIndex(selectedTournamentIndex);
				if (hard) {
					changeTournament();
				} else {
					changeTable();
				}
			} else {
				comboTournament.setSelectedIndex(0);
				changeTournament();
			}
		} else {
			changeTournament();
		}
		comboTournament.addActionListener(tournamentComboBoxActionListener);
	}

	private void changePlayerSubstitution(final int playerIndex) {
		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		final int selectedSessionIndex = comboSession.getSelectedIndex();
		if (selectedTournamentIndex >= 0 && selectedSessionIndex >= 0) {
			final int selectedSession = (int) comboSession.getSelectedItem();
			final MessageDialogOption answer = new UIMessageDialog(
				(JFrame) SwingUtilities.getWindowAncestor(this),
				translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_SUBSTITUTION_QUESTION_MESSAGE),
				translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_SUBSTITUTION_QUESTION_TITLE),
				MessageDialogMessageType.QUESTION,
				MessageDialogOptionsType.YES_NO,
				MessageDialogOption.YES,
				translator
			).showOptionDialog();
			if (answer == MessageDialogOption.YES) {
				final TournamentPlayer tournamentPlayer = listCurrentTables.get(playerIndex).getTournamentPlayer();
				boolean result = true;
				for (final TournamentScoreRCR table : listTournament.get(selectedTournamentIndex).getTables()) {
					if (table.getSessionId() >= selectedSession && table.getTournamentPlayer() == tournamentPlayer) {
						table.setSubstituted(comboBoxSubstitutions.get(playerIndex).getSelectedIndex());
						result = result && tournamentManager.updateTournamentScore(table);
					}
				}

				if (result) {
					new UIMessageDialog(
						(JFrame) SwingUtilities.getWindowAncestor(this),
						translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_SUBSTITUTION_SUBSTITUTION_CHANGED_MESSAGE),
						translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_SUBSTITUTION_SUBSTITUTION_CHANGED_TITLE),
						MessageDialogMessageType.INFORMATION,
						MessageDialogOptionsType.OK,
						MessageDialogOption.OK,
						translator
					).showOptionDialog();
					informChange();
				} else {
					new UIMessageDialog(
						(JFrame) SwingUtilities.getWindowAncestor(this),
						translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_SUBSTITUTION_SUBSTITUTION_NOT_CHANGED_MESSAGE),
						translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_SUBSTITUTION_SUBSTITUTION_NOT_CHANGED_TITLE),
						MessageDialogMessageType.ERROR,
						MessageDialogOptionsType.OK,
						MessageDialogOption.OK,
						translator
					).showOptionDialog();
				}
			}
		}
	}

	private void changeTournament() {
		comboSession.removeActionListener(sessionTableComboBoxActionListener);
		comboTable.removeActionListener(sessionTableComboBoxActionListener);
		comboSession.removeAllItems();
		comboTable.removeAllItems();

		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		if (selectedTournamentIndex >= 0) {
			final Tournament<TournamentScoreRCR, ScoreRCR> selectedTournament = listTournament.get(selectedTournamentIndex);
			for (int session = 1; session <= selectedTournament.getNbExistingSessions(); session++) {
				comboSession.addItem(SESSION_ID_FORMAT.format(session));
			}
			for (int table = 1; table <= selectedTournament.getNbTables(); table++) {
				comboTable.addItem(TABLE_ID_FORMAT.format(table));
			}
			comboTable.setSelectedIndex(0);
			comboSession.setSelectedIndex(0);
		}

		changeTable();
		comboSession.addActionListener(sessionTableComboBoxActionListener);
		comboTable.addActionListener(sessionTableComboBoxActionListener);
	}

	private void changeTable() {
		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		final int selectedSession = comboSession.getSelectedIndex() + 1;
		final int selectedTable = comboTable.getSelectedIndex() + 1;
		if (selectedTournamentIndex >= 0 && selectedSession >= 1 && selectedTable >= 1) {
			for (int index = 0; index < comboBoxSubstitutions.size(); index++) {
				comboBoxSubstitutions.get(index).removeActionListener(substitutionComboBoxActionListeners.get(index));
			}

			final int initialScore = (int) spinnerInitialGameScore.getValue();
			final Tournament<TournamentScoreRCR, ScoreRCR> tournament = listTournament.get(selectedTournamentIndex);
			listCurrentTables.clear();
			listCurrentTablesTemp.clear();
			for (final TournamentScoreRCR table : tournament.getTables()) {
				if (table.getSessionId() == selectedSession && table.getTableId() == selectedTable) {
					listCurrentTables.add(table);
					listCurrentTablesTemp.add(
						new TournamentScoreRCR(
							tournament,
							0,
							0,
							0,
							null,
							0,
							0,
							0,
							0,
							0,
							0,
							false
						)
					);
				}
			}
			Collections.sort(listCurrentTables);

			for (int seatIndex = 0; seatIndex < listCurrentTables.size(); seatIndex++) {
				final TournamentScoreRCR table = listCurrentTables.get(seatIndex);
				final TournamentPlayer tournamentPlayer = table.getTournamentPlayer();

				labelPlayerRankings.get(seatIndex).setText(Integer.toString(table.getRanking()));
				labelPlayers.get(seatIndex).setText(tournamentPlayer.getIDPlayerName(translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_TITLE_PLAYER_EMPTY)));
				comboBoxSubstitutions.get(seatIndex).setSelectedIndex(table.getSubstituted());
				spinnerPlayerScores.get(seatIndex).setValue(table.getGameScore() + initialScore);
				spinnerPlayerBorrows.get(seatIndex).setValue(0);
				labelPlayerNetScores.get(seatIndex).setText(Integer.toString(table.getGameScore()));
				labelPlayerUmaScores.get(seatIndex).setText(Integer.toString(table.getUmaScore()));
				spinnerPlayerPenalties.get(seatIndex).setValue(table.getPenalty());
				labelPlayerFinalScores.get(seatIndex).setText(Integer.toString(table.getFinalScore()));
			}
			labelScoreError.setText("");

			for (int index = 0; index < comboBoxSubstitutions.size(); index++) {
				comboBoxSubstitutions.get(index).addActionListener(substitutionComboBoxActionListeners.get(index));
			}
		} else {
			for (int seatIndex = 0; seatIndex < NUMBER_OF_PLAYERS; seatIndex++) {
				labelPlayerRankings.get(seatIndex).setText("");
				labelPlayers.get(seatIndex).setText("");
				comboBoxSubstitutions.get(seatIndex).setSelectedIndex(0);
				spinnerPlayerScores.get(seatIndex).setValue(0);
				spinnerPlayerBorrows.get(seatIndex).setValue(0);
				labelPlayerNetScores.get(seatIndex).setText("");
				labelPlayerUmaScores.get(seatIndex).setText("");
				spinnerPlayerPenalties.get(seatIndex).setValue(0);
				labelPlayerFinalScores.get(seatIndex).setText("");
			}
			labelScoreError.setText("");
		}

		unsaved = false;
		southPanel.setOpaque(false);
		hideSaveButton();
		repaint();
	}

	private void calculateFinalScore() {
		hideSaveButton();

		final List<TournamentScoreRCR> completellySubstitutedPlayerScoreList = new ArrayList<TournamentScoreRCR>();
		final List<TournamentScoreRCR> partiallySubstitutedPlayerScoreList = new ArrayList<TournamentScoreRCR>();
		final List<TournamentScoreRCR> normalPlayerScoreList = new ArrayList<TournamentScoreRCR>();

		final int initialScore = (int) spinnerInitialGameScore.getValue();
		int totalScore = 0;
		int playerIndex;

		for (playerIndex = 0; playerIndex < NUMBER_OF_PLAYERS; playerIndex++) {
			final TournamentScoreRCR table = listCurrentTablesTemp.get(playerIndex);
			table.setSubstituted(comboBoxSubstitutions.get(playerIndex).getSelectedIndex());

			switch (table.getSubstituted()) {
				case 0:
					normalPlayerScoreList.add(table);
					break;
				case 1:
					partiallySubstitutedPlayerScoreList.add(table);
					break;
				case 2:
					completellySubstitutedPlayerScoreList.add(table);
					normalPlayerScoreList.add(table);
			}

			// Check game score
			table.setGameScore((int) spinnerPlayerScores.get(playerIndex).getValue() - (int) spinnerPlayerBorrows.get(playerIndex).getValue() - initialScore);
			totalScore += table.getGameScore();

			// Check penalty
			table.setPenalty((int) spinnerPlayerPenalties.get(playerIndex).getValue());
		}

		// Check total score
		if (totalScore != 0) {
			new UIMessageDialog(
				(JFrame) SwingUtilities.getWindowAncestor(this),
				translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_RCR_CHECK_SCORE_GAME_SCORE_SUM_MESSAGE),
				translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_RCR_CHECK_SCORE_GAME_SCORE_SUM_TITLE),
				MessageDialogMessageType.WARNING,
				MessageDialogOptionsType.OK,
				MessageDialogOption.OK,
				translator
			).showOptionDialog();
			labelScoreError.setText(Integer.toString(totalScore));
		} else {
			labelScoreError.setText("");
		}

		// Sort score list
		Collections.sort(
			normalPlayerScoreList,
			(
				final TournamentScoreRCR o1,
				final TournamentScoreRCR o2
			) -> {
				return -Integer.compare(
					o1.getGameScore(),
					o2.getGameScore()
				);
			}
		);

		final List<Integer> umaSet = SCORE_INFO.getUma();
		playerIndex = 0;
		while (playerIndex < normalPlayerScoreList.size()) {
			int equalityPlayerIndex = playerIndex + 1;
			int totalUma = umaSet.get(playerIndex);
			while (equalityPlayerIndex < normalPlayerScoreList.size() && normalPlayerScoreList.get(playerIndex).getGameScore() == normalPlayerScoreList.get(equalityPlayerIndex).getGameScore()) {
				totalUma += umaSet.get(equalityPlayerIndex);
				equalityPlayerIndex++;
			}

			totalUma /= equalityPlayerIndex - playerIndex;
			for (int index = playerIndex; index < equalityPlayerIndex; index++) {
				final TournamentScoreRCR playerScore = normalPlayerScoreList.get(index);
				playerScore.setUmaScore(totalUma);
				playerScore.setFinalScore(playerScore.getGameScore() + playerScore.getUmaScore() - playerScore.getPenalty());
				playerScore.setRanking(playerIndex + 1);
			}
			playerIndex = equalityPlayerIndex;
		}

		for (playerIndex = 0; playerIndex < completellySubstitutedPlayerScoreList.size(); playerIndex++) {
			final TournamentScoreRCR playerScore = completellySubstitutedPlayerScoreList.get(playerIndex);
			// playerScore.setRanking(SUBSTITUTED_PLAYER_UMA_INDEX + 1);
			playerScore.setUmaScore(SCORE_INFO.getSubstitutedPlayerUMA());
			playerScore.setFinalScore(SCORE_INFO.getSubstitutedPlayerScore() + SCORE_INFO.getSubstitutedPlayerUMA());
		}

		for (playerIndex = 0; playerIndex < partiallySubstitutedPlayerScoreList.size(); playerIndex++) {
			final TournamentScoreRCR playerScore = partiallySubstitutedPlayerScoreList.get(playerIndex);
			playerScore.setRanking(NUMBER_OF_PLAYERS);
			playerScore.setUmaScore(SCORE_INFO.getSubstitutedPlayerUMA());
			playerScore.setFinalScore(SCORE_INFO.getSubstitutedPlayerScore() + SCORE_INFO.getSubstitutedPlayerUMA());
		}

		// Update display
		for (int index = 0; index < NUMBER_OF_PLAYERS; index++) {
			final TournamentScoreRCR playerScore = listCurrentTablesTemp.get(index);
			labelPlayerRankings.get(index).setText(Integer.toString(playerScore.getRanking()));
			labelPlayerNetScores.get(index).setText(Integer.toString(playerScore.getGameScore()));
			labelPlayerUmaScores.get(index).setText(Integer.toString(playerScore.getUmaScore()));
			labelPlayerFinalScores.get(index).setText(Integer.toString(playerScore.getFinalScore()));
		}

		unsaved = true;
		southPanel.setOpaque(true);
		showSaveButton();
		repaint();
	}

	private void hideSaveButton() {
		buttonSave.setEnabled(false);
	}

	private void showSaveButton() {
		buttonSave.setEnabled(true);
	}

	private void saveScore() {
		boolean result = true;
		for (int playerIndex = 0; playerIndex < listCurrentTables.size(); playerIndex++) {
			final TournamentScoreRCR newTable = listCurrentTablesTemp.get(playerIndex);
			final TournamentScoreRCR table = listCurrentTables.get(playerIndex);
			table.setSubstituted(newTable.getSubstituted());
			table.setRanking(newTable.getRanking());
			table.setGameScore(newTable.getGameScore());
			table.setUmaScore(newTable.getUmaScore());
			table.setPenalty(newTable.getPenalty());
			table.setFinalScore(newTable.getFinalScore());
			table.setScoreRecorded(true);
			result = result && tournamentManager.updateTournamentScore(table);
		}

		if (result) {
			new UIMessageDialog(
				(JFrame) SwingUtilities.getWindowAncestor(this),
				translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_SAVE_RESULT_SCORE_SAVED_MESSAGE),
				translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_SAVE_RESULT_SCORE_SAVED_TITLE),
				MessageDialogMessageType.INFORMATION,
				MessageDialogOptionsType.OK,
				MessageDialogOption.OK,
				translator
			).showOptionDialog();
			informChange();
			unsaved = false;
			southPanel.setOpaque(false);
			repaint();
		} else {
			new UIMessageDialog(
				(JFrame) SwingUtilities.getWindowAncestor(this),
				translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_SAVE_RESULT_SCORE_NOT_SAVED_MESSAGE),
				translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_SAVE_RESULT_SCORE_NOT_SAVED_TITLE),
				MessageDialogMessageType.ERROR,
				MessageDialogOptionsType.OK,
				MessageDialogOption.OK,
				translator
			).showOptionDialog();
		}
	}

	private void reset() {
		final MessageDialogOption answer = new UIMessageDialog(
			(JFrame) SwingUtilities.getWindowAncestor(this),
			translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_RESET_SCORE_CONFIRM_MESSAGE),
			translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_RESET_SCORE_CONFIRM_TITLE),
			MessageDialogMessageType.QUESTION,
			MessageDialogOptionsType.YES_NO,
			MessageDialogOption.NO,
			translator
		).showOptionDialog();
		if (answer == MessageDialogOption.YES) {
			final Object initialScore = spinnerInitialGameScore.getValue();
			boolean result = true;
			for (int playerIndex = 0; playerIndex < listCurrentTables.size(); playerIndex++) {
				final TournamentScoreRCR table = listCurrentTables.get(playerIndex);
				table.setSubstituted(0);
				table.setRanking(1);
				table.setGameScore(0);
				table.setUmaScore(0);
				table.setPenalty(0);
				table.setFinalScore(0);
				table.setScoreRecorded(false);
				result = result && tournamentManager.updateTournamentScore(table);

				labelPlayerRankings.get(playerIndex).setText("");
				spinnerPlayerScores.get(playerIndex).setValue(initialScore);
				spinnerPlayerBorrows.get(playerIndex).setValue(0);
				labelPlayerUmaScores.get(playerIndex).setText("");
				spinnerPlayerPenalties.get(playerIndex).setValue(0);
				labelPlayerFinalScores.get(playerIndex).setText("");
			}
			labelScoreError.setText("");
			hideSaveButton();
			informChange();
			unsaved = false;
			southPanel.setOpaque(false);
			repaint();
		}
	}
}
