/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.primary.score.rcr;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import fr.bri.mj.data.tournament.rcr.ScoreRCR;
import fr.bri.mj.data.tournament.rcr.TournamentScoreRCR;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.gui.primary.score.UIDisplayTableScoreChart;

public class UIDisplayTableScoreChartRCR extends UIDisplayTableScoreChart<TournamentScoreRCR, ScoreRCR> {

	private static final long serialVersionUID = 1725427733070489942L;

	private static final int NUMBER_OF_PLAYERS = 4;
	private static final int NUMBER_COLUMNS = 6;

	private static final UIText COLUMN_TITLE[] = new UIText[] {
		UIText.GUI_PRIMARY_DISPLAY_SCORE_RCR_TITLE_RANKING,
		UIText.GUI_PRIMARY_DISPLAY_SCORE_RCR_TITLE_PLAYER_NAME,
		UIText.GUI_PRIMARY_DISPLAY_SCORE_RCR_TITLE_SCORE,
		UIText.GUI_PRIMARY_DISPLAY_SCORE_RCR_TITLE_UMA,
		UIText.GUI_PRIMARY_DISPLAY_SCORE_RCR_TITLE_PENALTY,
		UIText.GUI_PRIMARY_DISPLAY_SCORE_RCR_TITLE_FINAL_SCORE,
		UIText.GUI_PRIMARY_DISPLAY_SCORE_RCR_TITLE_WIN,
		UIText.GUI_PRIMARY_DISPLAY_SCORE_RCR_TITLE_SELF_DRAW,
		UIText.GUI_PRIMARY_DISPLAY_SCORE_RCR_TITLE_DEAL_IN
	};

	private static final int COLUMN_WIDTH[] = new int[] {
		96,
		192,
		96,
		96,
		96,
		96,
		96,
		96,
		96
	};

	private static final int LABEL_HEIGHT = 18;

	private final JLabel labelTableNumber;
	private final JLabel labelGameInfos[][];
	private final DecimalFormat decimalFormat;

	public UIDisplayTableScoreChartRCR(
		final TournamentScoreRCR scoreHighest,
		final TournamentScoreRCR scoreLowest,
		final UITextTranslator translator
	) {
		super(
			scoreHighest,
			scoreLowest,
			translator
		);

		;
		decimalFormat = new DecimalFormat("#,###");
		final DecimalFormatSymbols symbols = decimalFormat.getDecimalFormatSymbols();
		symbols.setGroupingSeparator(' ');
		decimalFormat.setDecimalFormatSymbols(symbols);

		setLayout(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints(
			0,
			0,
			1,
			1,
			0.0,
			0.0,
			GridBagConstraints.CENTER,
			GridBagConstraints.BOTH,
			new Insets(
				0,
				0,
				0,
				0
			),
			8,
			8
		);

		final Dimension labelSizes[] = new Dimension[NUMBER_COLUMNS];
		for (int index = 0; index < NUMBER_COLUMNS; index++) {
			labelSizes[index] = new Dimension(
				COLUMN_WIDTH[index],
				LABEL_HEIGHT
			);
		}

		constraints.gridy = 0;
		labelTableNumber = new JLabel(
			"",
			SwingConstants.CENTER
		);
		labelTableNumber.setOpaque(true);
		add(
			labelTableNumber,
			constraints
		);

		constraints.gridy = 1;
		for (int index = 0; index < NUMBER_COLUMNS; index++) {
			constraints.gridx = index;
			final JLabel labelTitle = new JLabel(
				translator.translate(COLUMN_TITLE[index]),
				SwingConstants.CENTER
			);
			labelTitle.setBorder(BorderFactory.createLineBorder(Color.BLACK));
			labelTitle.setPreferredSize(labelSizes[index]);
			add(
				labelTitle,
				constraints
			);
		}

		labelGameInfos = new JLabel[NUMBER_OF_PLAYERS][NUMBER_COLUMNS];
		for (int playerIndex = 0; playerIndex < NUMBER_OF_PLAYERS; playerIndex++) {
			constraints.gridy = playerIndex + 2;
			for (int columnIndex = 0; columnIndex < NUMBER_COLUMNS; columnIndex++) {
				constraints.gridx = columnIndex;
				switch (columnIndex) {
					case 1:
						labelGameInfos[playerIndex][columnIndex] = new JLabel("");
						labelGameInfos[playerIndex][columnIndex].setBorder(
							BorderFactory.createCompoundBorder(
								BorderFactory.createLineBorder(Color.BLACK),
								BorderFactory.createEmptyBorder(
									0,
									4,
									0,
									4
								)
							)
						);
						break;
					default:
						labelGameInfos[playerIndex][columnIndex] = new JLabel(
							"",
							SwingConstants.CENTER
						);
						labelGameInfos[playerIndex][columnIndex].setBorder(BorderFactory.createLineBorder(Color.BLACK));
						break;
				}
				labelGameInfos[playerIndex][columnIndex].setPreferredSize(labelSizes[columnIndex]);
				add(
					labelGameInfos[playerIndex][columnIndex],
					constraints
				);
			}
		}
	}

	@Override
	public void displayTable() {
		final int numPlayers = Math.min(
			NUMBER_OF_PLAYERS,
			tableScores.size()
		);
		if (numPlayers > 0) {
			labelTableNumber.setText(
				translator.translate(UIText.GUI_PRIMARY_DISPLAY_SCORE_TITLE_TABLE)
					+ " "
					+ TABLE_ID_FORMAT.format(tableScores.first().getTableId())
			);

			boolean allRecorded = true;
			boolean oneRecorded = false;
			int index = 0;
			for (final TournamentScoreRCR table : tableScores) {
				allRecorded = allRecorded && table.isScoreRecorded();
				oneRecorded = oneRecorded || table.isScoreRecorded();
				labelGameInfos[index][0].setText(decimalFormat.format(table.getRanking()));
				labelGameInfos[index][1].setText(table.getTournamentPlayer().getIDPlayerName(translator.translate(UIText.GUI_PRIMARY_DISPLAY_SCORE_RCR_TITLE_PLAYER_EMPTY)));
				labelGameInfos[index][2].setText(decimalFormat.format(table.getGameScore()));
				labelGameInfos[index][3].setText(decimalFormat.format(table.getUmaScore()));
				labelGameInfos[index][4].setText(decimalFormat.format(table.getPenalty()));
				if (table.getPenalty() > 0) {
					labelGameInfos[index][4].setOpaque(true);
					labelGameInfos[index][4].setBackground(COLOR_SCORE_LOWEST);
				}
				labelGameInfos[index][5].setText(decimalFormat.format(table.getFinalScore()));
				if (scoreHighest.getFinalScore() != scoreLowest.getFinalScore()) {
					if (table.getFinalScore() == scoreHighest.getFinalScore()) {
						labelGameInfos[index][5].setOpaque(true);
						labelGameInfos[index][5].setBackground(COLOR_SCORE_HIGHEST);
					} else if (table.getFinalScore() == scoreLowest.getFinalScore()) {
						labelGameInfos[index][5].setOpaque(true);
						labelGameInfos[index][5].setBackground(COLOR_SCORE_LOWEST);
					}
				}
				index++;
			}

			if (allRecorded) {
				labelTableNumber.setBackground(COLOR_SCORE_PRESENT);
			} else {
				if (oneRecorded) {
					labelTableNumber.setBackground(COLOR_SCORE_ERROR);
				} else {
					labelTableNumber.setBackground(COLOR_SCORE_ABSENT);
				}
			}
		}
	}
}
