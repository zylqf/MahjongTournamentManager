/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.primary.managetournamentplayer;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.player.Player;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.data.tournament.TournamentScore;
import fr.bri.mj.dataaccess.PlayerManager;
import fr.bri.mj.dataaccess.TournamentManager;
import fr.bri.mj.gui.common.MouseClickToActionListener;
import fr.bri.mj.gui.common.UITabPanelWithWaterMark;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.dialog.UIMessageDialog;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogMessageType;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOption;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOptionsType;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.utils.StringNormalizer;

public class UITabPanelManageTournamentPlayer<ScoreClassType extends TournamentScore<ScoreClassType, ScoreEnumType>, ScoreEnumType extends Enum<ScoreEnumType>> extends UITabPanelWithWaterMark {

	private static final long serialVersionUID = 7937229822617161664L;

	private static final Color UNSAVED_BACKGROUND_COLOR = new Color(
		255,
		191,
		191
	);

	private static final Color TRANSPARENT_TEXT_BACKGROUD_COLOR = new Color(
		255,
		255,
		255,
		127
	);

	private static final Color DUPLICATE_COLOR_EMPTY = new Color(
		255,
		255,
		255
	);
	private static final Color DUPLICATE_COLOR_OK = new Color(
		127,
		255,
		127
	);
	private static final Color DUPLICATE_COLOR_KO = new Color(
		255,
		127,
		127
	);

	private final PlayerManager playerManager;
	private final TournamentManager<ScoreClassType, ScoreEnumType> tournamentManager;

	private final JComboBox<String> comboTournament;
	private final JButton buttonSaveAll;
	private final JPanel centerPanel;

	private final List<JLabel> listLabelId;
	private final List<JLabel> listLabelLicence;
	private final List<JLabel> listLabelPlayerName;
	private final List<JTextField> listTextTeamName;

	private final ActionListener tournamentComboBoxActionListener;

	private final List<Player> listPlayer;
	private final List<String> listPlayerName;
	private final List<String> listDisplayString;
	private final List<String> listNormalizedDisplayString;
	private final List<Player> listCurrentTournamentPlayer;

	private final List<TournamentPlayer> listTournamentPlayer;
	private final List<Tournament<ScoreClassType, ScoreEnumType>> listTournament;
	private Tournament<ScoreClassType, ScoreEnumType> selectedTournament;

	private boolean unsaved;

	public UITabPanelManageTournamentPlayer(
		final PlayerManager playerManager,
		final TournamentManager<ScoreClassType, ScoreEnumType> tournamentManager,
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			config,
			translator
		);
		this.playerManager = playerManager;
		this.tournamentManager = tournamentManager;

		setLayout(new BorderLayout());
		{
			final JPanel panelNorth = new JPanel();
			final ProportionalGridLayoutInteger northLayout = new ProportionalGridLayoutInteger(
				1,
				8,
				8,
				2
			);
			panelNorth.setLayout(northLayout);
			panelNorth.setBorder(BorderFactory.createLoweredBevelBorder());
			add(
				panelNorth,
				BorderLayout.NORTH
			);
			final ProportionalGridLayoutConstraint c = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			c.y = 0;
			c.x = 2;
			c.gridWidth = 1;
			panelNorth.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_PLAYER_TITLE_TOURNAMENT),
					SwingConstants.RIGHT
				),
				c
			);

			comboTournament = new JComboBox<String>();
			comboTournament.setEditable(false);

			c.x = 3;
			c.gridWidth = 3;
			panelNorth.add(
				comboTournament,
				c
			);

			c.x = 7;
			c.gridWidth = 1;
			buttonSaveAll = new JButton(translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_PLAYER_BUTTON_SAVE));
			panelNorth.add(
				buttonSaveAll,
				c
			);
		}

		{
			final JPanel panelCenterSupport = new JPanel(new GridBagLayout());
			panelCenterSupport.setOpaque(false);
			final GridBagConstraints constraints = new GridBagConstraints();
			constraints.gridx = 0;
			constraints.gridy = 0;
			constraints.gridwidth = 1;
			constraints.gridheight = 1;
			constraints.weightx = 1.0;
			constraints.weighty = 1.0;
			constraints.anchor = GridBagConstraints.NORTH;
			constraints.fill = GridBagConstraints.HORIZONTAL;

			centerPanel = new JPanel();
			centerPanel.setOpaque(false);
			centerPanel.setBackground(UNSAVED_BACKGROUND_COLOR);
			panelCenterSupport.add(
				centerPanel,
				constraints
			);

			final JScrollPane scroll = new JScrollPane(
				panelCenterSupport,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER
			);
			scroll.getVerticalScrollBar().setUnitIncrement(16);
			scroll.setOpaque(false);
			scroll.getViewport().setOpaque(false);

			add(
				scroll,
				BorderLayout.CENTER
			);
		}

		listLabelId = new ArrayList<JLabel>();
		listLabelLicence = new ArrayList<JLabel>();
		listLabelPlayerName = new ArrayList<JLabel>();
		listTextTeamName = new ArrayList<JTextField>();

		tournamentComboBoxActionListener = (final ActionEvent e) -> {
			refreshPlayer();
		};
		buttonSaveAll.addActionListener((final ActionEvent e) -> {
			saveAll();
		});

		listPlayer = new ArrayList<Player>();
		listPlayerName = new ArrayList<String>();
		listDisplayString = new ArrayList<String>();
		listNormalizedDisplayString = new ArrayList<String>();
		listCurrentTournamentPlayer = new ArrayList<Player>();

		listTournament = new ArrayList<Tournament<ScoreClassType, ScoreEnumType>>();
		listTournamentPlayer = new ArrayList<TournamentPlayer>();
	}

	@Override
	public String getTabName() {
		return translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_PLAYER_TAB_NAME);
	}

	@Override
	public void remoteRefresh() {
		refresh(false);
	}

	@Override
	public boolean hasUnsavedChanges() {
		return unsaved;
	}

	@Override
	public void refresh(final boolean hard) {
		if (hard || listPlayer.isEmpty()) {
			final List<Player> tempListPlayer = new ArrayList<Player>(playerManager.getAllPlayers());
			Collections.sort(
				tempListPlayer,
				(
					final Player p1,
					final Player p2
				) -> {
					return p1.getPlayerFirstName().compareTo(p2.getPlayerFirstName());
				}
			);
			listPlayer.clear();
			listPlayer.add(null);
			listPlayer.addAll(tempListPlayer);

			listPlayerName.clear();
			listDisplayString.clear();
			listNormalizedDisplayString.clear();

			final String playerNoneName = translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_PLAYER_TITLE_PLAYER_NONE);
			listPlayerName.add(playerNoneName);
			listDisplayString.add(playerNoneName);
			listNormalizedDisplayString.add(playerNoneName);

			for (int index = 1; index < listPlayer.size(); index++) {
				final Player player = listPlayer.get(index);
				final String playerName = player.getPlayerFirstName()
					+ " "
					+ player.getPlayerLastName();
				listPlayerName.add(playerName);
				final String displayString = PLAYER_LICENSE_FORMAT.format(player.getId())
					+ " - "
					+ playerName;
				final String normalizedDisplayString = StringNormalizer.normalize(displayString).toLowerCase();
				listDisplayString.add(displayString);
				listNormalizedDisplayString.add(normalizedDisplayString);
			}
		}

		comboTournament.removeActionListener(tournamentComboBoxActionListener);
		comboTournament.removeAllItems();
		listTournament.clear();

		final Set<Tournament<ScoreClassType, ScoreEnumType>> existingTournaments = tournamentManager.getAllTournament();
		for (final Tournament<ScoreClassType, ScoreEnumType> tournament : existingTournaments) {
			if (!tournament.isArchived()) {
				listTournament.add(tournament);
			}
		}
		if (listTournament.size() > 0) {
			Collections.sort(listTournament);
			int currentTournamentIndex = 0;
			for (int index = 0; index < listTournament.size(); index++) {
				final Tournament<ScoreClassType, ScoreEnumType> tournament = listTournament.get(index);
				if (tournament.equals(selectedTournament)) {
					currentTournamentIndex = index;
				}
				comboTournament.addItem(tournament.getName());
			}
			comboTournament.setSelectedIndex(currentTournamentIndex);
		}

		refreshPlayer();
		comboTournament.addActionListener(tournamentComboBoxActionListener);
	}

	private void refreshPlayer() {
		invalidate();
		centerPanel.setOpaque(false);
		centerPanel.removeAll();

		listLabelId.clear();
		listLabelLicence.clear();
		listLabelPlayerName.clear();
		listTextTeamName.clear();
		listTournamentPlayer.clear();
		listCurrentTournamentPlayer.clear();
		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		if (listTournament.size() > 0 && selectedTournamentIndex >= 0) {
			selectedTournament = listTournament.get(selectedTournamentIndex);
			listTournamentPlayer.addAll(selectedTournament.getTournamentPlayers());

			final ProportionalGridLayoutInteger layout = new ProportionalGridLayoutInteger(
				listTournamentPlayer.size() + 1,
				6,
				4,
				8
			);
			layout.setWeightX(
				1,
				1,
				1,
				3,
				3,
				1
			);
			centerPanel.setLayout(layout);
			final ProportionalGridLayoutConstraint constraints = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			constraints.y = 0;
			{
				constraints.x = 1;
				centerPanel.add(
					new JLabel(
						translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_PLAYER_TITLE_PLAYER_ID),
						SwingConstants.CENTER
					),
					constraints
				);
				constraints.x = 2;
				centerPanel.add(
					new JLabel(
						translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_PLAYER_TITLE_PLAYER_LICENSE),
						SwingConstants.CENTER
					),
					constraints
				);
				constraints.x = 3;
				centerPanel.add(
					new JLabel(
						translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_PLAYER_TITLE_PLAYER_NAME),
						SwingConstants.CENTER
					),
					constraints
				);
				constraints.x = 4;
				centerPanel.add(
					new JLabel(
						translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_PLAYER_TITLE_TEAM_NAME),
						SwingConstants.CENTER
					),
					constraints
				);
			}

			for (int index = 0; index < listTournamentPlayer.size(); index++) {
				final TournamentPlayer tournamentPlayer = listTournamentPlayer.get(index);

				constraints.y = index + 1;

				final JLabel labelTournamentPlayerID = new JLabel(
					PLAYER_ID_FORMAT.format(tournamentPlayer.getTournamentPlayerId()),
					SwingConstants.CENTER
				);
				labelTournamentPlayerID.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
				labelTournamentPlayerID.setOpaque(true);
				final int tournamentPlayerIndex = index;
				labelTournamentPlayerID.addMouseListener(new MouseClickToActionListener((final ActionEvent event) -> {
					modifyPlayer(tournamentPlayerIndex);
				}));
				constraints.x = 1;
				centerPanel.add(
					labelTournamentPlayerID,
					constraints
				);
				listLabelId.add(labelTournamentPlayerID);

				final Player player = tournamentPlayer.getPlayer();
				final int playerIndex = listPlayer.indexOf(player);
				listCurrentTournamentPlayer.add(player);

				final JLabel labelPlayerLicense = new JLabel(
					player != null
						? player.getLicense()
						: "",
					SwingConstants.CENTER
				);
				labelPlayerLicense.setBorder(
					BorderFactory.createCompoundBorder(
						BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
						BorderFactory.createEmptyBorder(
							0,
							4,
							0,
							4
						)
					)
				);
				listLabelLicence.add(labelPlayerLicense);
				constraints.x = 2;
				centerPanel.add(
					labelPlayerLicense,
					constraints
				);

				final JLabel labelPlayerName = new JLabel(listPlayerName.get(playerIndex));
				labelPlayerName.setBorder(
					BorderFactory.createCompoundBorder(
						BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
						BorderFactory.createEmptyBorder(
							0,
							4,
							0,
							4
						)
					)
				);
				listLabelPlayerName.add(labelPlayerName);
				constraints.x = 3;
				centerPanel.add(
					labelPlayerName,
					constraints
				);

				final JTextField textPlayerTeam = new JTextField(tournamentPlayer.getTeamName());
				textPlayerTeam.setBackground(TRANSPARENT_TEXT_BACKGROUD_COLOR);
				constraints.x = 4;
				centerPanel.add(
					textPlayerTeam,
					constraints
				);
				listTextTeamName.add(textPlayerTeam);
			}
		} else {
			selectedTournament = null;
		}
		unsaved = false;
		validate();
		checkDuplicatePlayerChoices();
	}

	private void modifyPlayer(final int tournamentPlayerIndex) {
		final UISelectPlayerDialog dialog = new UISelectPlayerDialog(
			(JFrame) SwingUtilities.getWindowAncestor(this),
			PLAYER_ID_FORMAT.format(tournamentPlayerIndex + 1),
			listPlayer,
			listDisplayString,
			listNormalizedDisplayString,
			translator
		);
		final boolean playerSelected = dialog.showDialog(listCurrentTournamentPlayer.get(tournamentPlayerIndex));
		if (playerSelected) {
			final Player selectedPlayer = dialog.getSelectedPlayer();
			final Player currentSelectedPlayer = listCurrentTournamentPlayer.get(tournamentPlayerIndex);
			if (selectedPlayer != currentSelectedPlayer) {
				final int playerIndex = listPlayer.indexOf(selectedPlayer);
				listCurrentTournamentPlayer.set(
					tournamentPlayerIndex,
					selectedPlayer
				);
				listLabelLicence.get(tournamentPlayerIndex).setText(
					selectedPlayer != null
						? selectedPlayer.getLicense()
						: ""
				);
				listLabelPlayerName.get(tournamentPlayerIndex).setText(listPlayerName.get(playerIndex));
				unsaved = true;
				centerPanel.setOpaque(true);
				checkDuplicatePlayerChoices();
			}
		}
	}

	private void checkDuplicatePlayerChoices() {
		for (int index = 0; index < listLabelId.size(); index++) {
			listLabelId.get(index).setBackground(DUPLICATE_COLOR_OK);
		}
		boolean noDuplicate = true;
		for (int index1 = 0; index1 < listCurrentTournamentPlayer.size(); index1++) {
			final Player player1 = listCurrentTournamentPlayer.get(index1);
			if (player1 != null) {
				for (int index2 = index1 + 1; index2 < listCurrentTournamentPlayer.size(); index2++) {
					final Player player2 = listCurrentTournamentPlayer.get(index2);
					if (player2 != null && player1 == player2) {
						listLabelId.get(index1).setBackground(DUPLICATE_COLOR_KO);
						listLabelId.get(index2).setBackground(DUPLICATE_COLOR_KO);
						noDuplicate = false;
						break;
					}
				}
			} else {
				listLabelId.get(index1).setBackground(DUPLICATE_COLOR_EMPTY);
			}
		}
		buttonSaveAll.setEnabled(noDuplicate);
		repaint();
	}

	private void saveAll() {
		if (selectedTournament != null) {
			for (int index = 0; index < listTournamentPlayer.size(); index++) {
				final TournamentPlayer tournamentPlayer = listTournamentPlayer.get(index);

				final Player player = listCurrentTournamentPlayer.get(index);
				tournamentPlayer.setPlayer(player);

				final JTextField textTeamName = listTextTeamName.get(index);
				tournamentPlayer.setTeamName(textTeamName.getText().trim());
			}

			final boolean result = tournamentManager.updateTournament(selectedTournament);
			if (result) {
				new UIMessageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_PLAYER_SAVE_RESULT_TOURNAMENT_SAVED_MESSAGE),
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_PLAYER_SAVE_RESULT_TOURNAMENT_SAVED_TITLE),
					MessageDialogMessageType.INFORMATION,
					MessageDialogOptionsType.OK,
					MessageDialogOption.OK,
					translator
				).showOptionDialog();
				informChange();
				unsaved = false;
				centerPanel.setOpaque(false);
				repaint();
			} else {
				new UIMessageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_PLAYER_SAVE_RESULT_TOURNAMENT_NOT_SAVED_MESSAGE),
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_PLAYER_SAVE_RESULT_TOURNAMENT_NOT_SAVED_TITLE),
					MessageDialogMessageType.ERROR,
					MessageDialogOptionsType.OK,
					MessageDialogOption.OK,
					translator
				).showOptionDialog();
			}
		}
	}
}
