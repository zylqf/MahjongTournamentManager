/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.primary.score;

import java.awt.Color;
import java.text.DecimalFormat;
import java.util.SortedSet;

import javax.swing.JPanel;

import fr.bri.mj.data.tournament.TournamentScore;
import fr.bri.mj.gui.language.UITextTranslator;

public abstract class UIDisplayTableScoreChart<ScoreClassType extends TournamentScore<ScoreClassType, ScoreEnumType>, ScoreEnumType extends Enum<ScoreEnumType>> extends JPanel {

	protected static final DecimalFormat TABLE_ID_FORMAT = new DecimalFormat("00");

	private static final long serialVersionUID = 2842002761732162433L;

	protected static final Color COLOR_SCORE_PRESENT = new Color(
		127,
		255,
		127,
		255
	);
	protected static final Color COLOR_SCORE_ABSENT = new Color(
		255,
		255,
		127,
		255
	);
	protected static final Color COLOR_SCORE_ERROR = new Color(
		255,
		127,
		127,
		255
	);
	protected static final Color COLOR_SCORE_HIGHEST = new Color(
		127,
		255,
		127,
		255
	);
	protected static final Color COLOR_SCORE_LOWEST = new Color(
		255,
		127,
		127,
		255
	);

	protected final UITextTranslator translator;
	protected final ScoreClassType scoreHighest;
	protected final ScoreClassType scoreLowest;

	protected SortedSet<ScoreClassType> tableScores;

	protected UIDisplayTableScoreChart(
		final ScoreClassType scoreHighest,
		final ScoreClassType scoreLowest,
		final UITextTranslator translator
	) {
		this.scoreHighest = scoreHighest;
		this.scoreLowest = scoreLowest;
		this.translator = translator;
	}

	public void setTableScores(final SortedSet<ScoreClassType> tableScores) {
		this.tableScores = tableScores;
		displayTable();
	}

	public abstract void displayTable();
}
