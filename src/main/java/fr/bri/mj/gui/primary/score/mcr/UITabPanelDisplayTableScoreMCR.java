/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.primary.score.mcr;

import java.util.EnumMap;
import java.util.Map;

import fr.bri.mj.data.score.raw.comparator.ScoreComparator;
import fr.bri.mj.data.score.raw.comparator.ScoreComparatorMCRFactory;
import fr.bri.mj.data.score.raw.getter.ScoreGetter;
import fr.bri.mj.data.score.raw.getter.ScoreGetterMCRFactory;
import fr.bri.mj.data.score.raw.setter.ScoreSetter;
import fr.bri.mj.data.score.raw.setter.ScoreSetterMCRFactory;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.mcr.ScoreMCR;
import fr.bri.mj.data.tournament.mcr.TournamentScoreMCR;
import fr.bri.mj.dataaccess.TournamentManager;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.gui.primary.score.UIDisplayTableScoreChart;
import fr.bri.mj.gui.primary.score.UITabPanelDisplayTableScore;

public class UITabPanelDisplayTableScoreMCR extends UITabPanelDisplayTableScore<TournamentScoreMCR, ScoreMCR> {

	private static final long serialVersionUID = 3317547816364242325L;

	private static final ScoreMCR SCORES_TO_COMPARE[] = {
		ScoreMCR.TABLE_POINT,
	};

	private final Map<ScoreMCR, ScoreComparator<TournamentScoreMCR, ScoreMCR>> mapComparators;
	private final Map<ScoreMCR, ScoreGetter<TournamentScoreMCR, ScoreMCR>> mapGetters;
	private final Map<ScoreMCR, ScoreSetter<TournamentScoreMCR, ScoreMCR>> mapSetters;

	public UITabPanelDisplayTableScoreMCR(
		final TournamentManager<TournamentScoreMCR, ScoreMCR> dataAccessAdaptor,
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			dataAccessAdaptor,
			config,
			translator
		);

		mapComparators = new EnumMap<ScoreMCR, ScoreComparator<TournamentScoreMCR, ScoreMCR>>(ScoreMCR.class);
		mapGetters = new EnumMap<ScoreMCR, ScoreGetter<TournamentScoreMCR, ScoreMCR>>(ScoreMCR.class);
		mapSetters = new EnumMap<ScoreMCR, ScoreSetter<TournamentScoreMCR, ScoreMCR>>(ScoreMCR.class);

		for (final ScoreMCR score : SCORES_TO_COMPARE) {
			mapComparators.put(
				score,
				ScoreComparatorMCRFactory.getScoreAccessorMCR(score)
			);
			mapGetters.put(
				score,
				ScoreGetterMCRFactory.getScoreGetterMCR(score)
			);
			mapSetters.put(
				score,
				ScoreSetterMCRFactory.getScoreSetterMCR(score)
			);
		}
	}

	@Override
	protected void updateHighestLowestScore(
		final Tournament<TournamentScoreMCR, ScoreMCR> tournament,
		final int session
	) {
		scoreHighest = new TournamentScoreMCR(
			null,
			session,
			0,
			0,
			null,
			0,
			0,
			Integer.MIN_VALUE,
			Integer.MIN_VALUE,
			Integer.MIN_VALUE,
			Integer.MIN_VALUE,
			false
		);
		scoreLowest = new TournamentScoreMCR(
			null,
			session,
			0,
			0,
			null,
			0,
			0,
			Integer.MAX_VALUE,
			Integer.MAX_VALUE,
			Integer.MAX_VALUE,
			Integer.MAX_VALUE,
			false
		);

		for (final TournamentScoreMCR table : tournament.getTables()) {
			if (table.getSessionId() == session) {
				for (final ScoreMCR score : SCORES_TO_COMPARE) {
					if (mapComparators.get(score).compare(
						table,
						scoreHighest
					) > 0) {
						mapSetters.get(score).set(
							scoreHighest,
							mapGetters.get(score).get(table)
						);
					}
					if (mapComparators.get(score).compare(
						table,
						scoreLowest
					) < 0) {
						mapSetters.get(score).set(
							scoreLowest,
							mapGetters.get(score).get(table)
						);
					}
				}
			}
		}
	}

	@Override
	protected UIDisplayTableScoreChart<TournamentScoreMCR, ScoreMCR> createDisplay() {
		return new UIDisplayTableScoreChartMCR(
			scoreHighest,
			scoreLowest,
			translator
		);
	}
}
