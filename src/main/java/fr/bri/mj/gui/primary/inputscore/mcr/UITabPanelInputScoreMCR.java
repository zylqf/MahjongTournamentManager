/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.primary.inputscore.mcr;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.data.tournament.mcr.ScoreMCR;
import fr.bri.mj.data.tournament.mcr.TournamentScoreMCR;
import fr.bri.mj.dataaccess.TournamentManager;
import fr.bri.mj.gui.common.SelectAllTextFocusListener;
import fr.bri.mj.gui.common.UITabPanelWithWaterMark;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.dialog.UIMessageDialog;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogMessageType;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOption;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOptionsType;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;

public class UITabPanelInputScoreMCR extends UITabPanelWithWaterMark {

	private static final long serialVersionUID = 1458740122980813519L;

	private static final Color UNSAVED_BACKGROUND_COLOR = new Color(
		255,
		191,
		191
	);

	private static final int NUMBER_OF_PLAYERS = 4;
	private static final UIText STRING_SUBSTITUTION[] = {
		UIText.GUI_PRIMARY_INPUT_SCORE_MCR_SUBSTITUTION_NORMAL,
		UIText.GUI_PRIMARY_INPUT_SCORE_MCR_SUBSTITUTION_SUBSTITUTED
	};
	private static final int MATCH_POINTS[] = {
		48,
		24,
		12,
		0
	};
	private static final int SUBSTITUTED_MATCH_POINT_INDEX = 3;

	private final TournamentManager<TournamentScoreMCR, ScoreMCR> tournamentManager;

	private final JComboBox<String> comboTournament;
	private final JComboBox<String> comboSession;
	private final JComboBox<String> comboTable;

	private final JPanel southPanel;

	private final List<JLabel> labelPlayerRankings;
	private final List<JLabel> labelPlayers;
	private final List<JComboBox<String>> comboBoxSubstitutions;
	private final List<JSpinner> spinnerPlayerTablePoints;
	private final List<JSpinner> spinnerPlayerPenalties;
	private final List<JLabel> labelPlayerTotalPoints;
	private final List<JLabel> labelPlayerMatchPoints;

	private final ActionListener tournamentComboBoxActionListener;
	private final ActionListener sessionTableComboBoxActionListener;

	private final JButton buttonCalculate;
	private final JButton buttonReset;
	private final JButton buttonSave;
	private final JLabel labelScoreError;

	private final List<Tournament<TournamentScoreMCR, ScoreMCR>> listTournament;
	private final List<TournamentScoreMCR> listCurrentTables;
	private final List<TournamentScoreMCR> listCurrentTablesTemp;

	private boolean unsaved;

	public UITabPanelInputScoreMCR(
		final TournamentManager<TournamentScoreMCR, ScoreMCR> tournamentManager,
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			config,
			translator
		);
		this.tournamentManager = tournamentManager;

		final JPanel innerPanel = new JPanel();
		innerPanel.setLayout(
			new BorderLayout(
				12,
				12
			)
		);
		{
			final JPanel northPanel = new JPanel(
				new ProportionalGridLayoutInteger(
					1,
					8,
					8,
					8
				)
			);
			final ProportionalGridLayoutConstraint northPanelConstraint = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			northPanelConstraint.y = 0;
			northPanelConstraint.x = 0;
			northPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_TITLE_TOURNAMENT),
					SwingConstants.RIGHT
				),
				northPanelConstraint
			);
			comboTournament = new JComboBox<String>();
			northPanelConstraint.x = 1;
			northPanelConstraint.gridWidth = 3;
			northPanel.add(
				comboTournament,
				northPanelConstraint
			);

			northPanelConstraint.x = 4;
			northPanelConstraint.gridWidth = 1;
			northPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_TITLE_SESSION),
					SwingConstants.RIGHT
				),
				northPanelConstraint
			);
			comboSession = new JComboBox<String>();
			northPanelConstraint.x = 5;
			northPanel.add(
				comboSession,
				northPanelConstraint
			);

			northPanelConstraint.x = 6;
			northPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_TITLE_TABLE),
					SwingConstants.RIGHT
				),
				northPanelConstraint
			);
			comboTable = new JComboBox<String>();
			northPanelConstraint.x = 7;
			northPanel.add(
				comboTable,
				northPanelConstraint
			);

			innerPanel.add(
				northPanel,
				BorderLayout.NORTH
			);
		}

		{
			final JPanel centerPanel = new JPanel();
			centerPanel.setLayout(
				new ProportionalGridLayoutInteger(
					6,
					9,
					4,
					4
				)
			);
			final ProportionalGridLayoutConstraint centerPanelConstraint = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);
			final Dimension labelSize = new Dimension(
				112,
				32
			);

			centerPanelConstraint.y = 0;
			centerPanelConstraint.x = 0;
			centerPanelConstraint.gridWidth = 1;
			centerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_MCR_TITLE_RANKING),
					SwingConstants.CENTER
				),
				centerPanelConstraint
			);

			centerPanelConstraint.x = 1;
			centerPanelConstraint.gridWidth = 3;
			centerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_MCR_TITLE_PLAYER),
					SwingConstants.CENTER
				),
				centerPanelConstraint
			);

			centerPanelConstraint.x = 4;
			centerPanelConstraint.gridWidth = 1;
			centerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_MCR_TITLE_SUBSTITUTED),
					SwingConstants.CENTER
				),
				centerPanelConstraint
			);

			centerPanelConstraint.x = 5;
			centerPanelConstraint.gridWidth = 1;
			centerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_MCR_TITLE_TABLE_POINT),
					SwingConstants.CENTER
				),
				centerPanelConstraint
			);

			centerPanelConstraint.x = 6;
			centerPanelConstraint.gridWidth = 1;
			centerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_MCR_TITLE_PENALTY),
					SwingConstants.CENTER
				),
				centerPanelConstraint
			);

			centerPanelConstraint.x = 7;
			centerPanelConstraint.gridWidth = 1;
			centerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_MCR_TITLE_TOTAL_POINT),
					SwingConstants.CENTER
				),
				centerPanelConstraint
			);

			centerPanelConstraint.x = 8;
			centerPanelConstraint.gridWidth = 1;
			centerPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_MCR_TITLE_MATCH_POINT),
					SwingConstants.CENTER
				),
				centerPanelConstraint
			);

			labelPlayerRankings = new ArrayList<JLabel>(NUMBER_OF_PLAYERS);
			labelPlayers = new ArrayList<JLabel>(NUMBER_OF_PLAYERS);
			comboBoxSubstitutions = new ArrayList<JComboBox<String>>(NUMBER_OF_PLAYERS);
			spinnerPlayerTablePoints = new ArrayList<JSpinner>(NUMBER_OF_PLAYERS);
			spinnerPlayerPenalties = new ArrayList<JSpinner>(NUMBER_OF_PLAYERS);
			labelPlayerTotalPoints = new ArrayList<JLabel>(NUMBER_OF_PLAYERS);
			labelPlayerMatchPoints = new ArrayList<JLabel>(NUMBER_OF_PLAYERS);
			final ChangeListener scoreSpinnerChangeListener = (final ChangeEvent e) -> {
				hideSaveButton();
			};
			final FocusListener spinnerFocusListener = new SelectAllTextFocusListener(0);

			for (int playerIndex = 0; playerIndex < NUMBER_OF_PLAYERS; playerIndex++) {
				centerPanelConstraint.y = 1 + playerIndex;

				final JLabel labelPlayerRanking = new JLabel(
					"",
					SwingConstants.CENTER
				);
				labelPlayerRanking.setBorder(
					BorderFactory.createCompoundBorder(
						BorderFactory.createLoweredSoftBevelBorder(),
						BorderFactory.createEmptyBorder(
							0,
							4,
							0,
							4
						)
					)
				);
				labelPlayerRanking.setPreferredSize(labelSize);
				labelPlayerRankings.add(
					playerIndex,
					labelPlayerRanking
				);
				centerPanelConstraint.x = 0;
				centerPanelConstraint.gridWidth = 1;
				centerPanel.add(
					labelPlayerRanking,
					centerPanelConstraint
				);

				final JLabel labelPlayer = new JLabel();
				labelPlayers.add(
					playerIndex,
					labelPlayer
				);
				labelPlayer.setBorder(
					BorderFactory.createCompoundBorder(
						BorderFactory.createLoweredSoftBevelBorder(),
						BorderFactory.createEmptyBorder(
							0,
							4,
							0,
							4
						)
					)
				);
				centerPanelConstraint.x = 1;
				centerPanelConstraint.gridWidth = 3;
				centerPanel.add(
					labelPlayer,
					centerPanelConstraint
				);

				final String[] substitutions = new String[STRING_SUBSTITUTION.length];
				for (int index = 0; index < STRING_SUBSTITUTION.length; index++) {
					substitutions[index] = translator.translate(STRING_SUBSTITUTION[index]);
				}
				final JComboBox<String> comboBoxSubstitution = new JComboBox<String>(substitutions);
				comboBoxSubstitutions.add(
					playerIndex,
					comboBoxSubstitution
				);
				centerPanelConstraint.x = 4;
				centerPanelConstraint.gridWidth = 1;
				centerPanel.add(
					comboBoxSubstitution,
					centerPanelConstraint
				);

				final JSpinner spinnerPlayerTablePoint = new JSpinner(
					new SpinnerNumberModel(
						0,
						-2000,
						2000,
						1
					)
				);
				final JSpinner.NumberEditor scoreNumberEditor = new JSpinner.NumberEditor(
					spinnerPlayerTablePoint,
					"0"
				);
				scoreNumberEditor.getTextField().addFocusListener(spinnerFocusListener);
				spinnerPlayerTablePoint.setEditor(scoreNumberEditor);
				spinnerPlayerTablePoints.add(
					playerIndex,
					spinnerPlayerTablePoint
				);
				spinnerPlayerTablePoint.addChangeListener(scoreSpinnerChangeListener);
				centerPanelConstraint.x = 5;
				centerPanelConstraint.gridWidth = 1;
				centerPanel.add(
					spinnerPlayerTablePoint,
					centerPanelConstraint
				);

				final JSpinner spinnerPlayerPenalty = new JSpinner(
					new SpinnerNumberModel(
						0,
						0,
						1000,
						1
					)
				);
				final JSpinner.NumberEditor penaltyNumberEditor = new JSpinner.NumberEditor(
					spinnerPlayerPenalty,
					"0"
				);
				penaltyNumberEditor.getTextField().addFocusListener(spinnerFocusListener);
				spinnerPlayerPenalty.setEditor(penaltyNumberEditor);
				spinnerPlayerPenalties.add(
					playerIndex,
					spinnerPlayerPenalty
				);
				spinnerPlayerPenalty.addChangeListener(scoreSpinnerChangeListener);
				centerPanelConstraint.x = 6;
				centerPanelConstraint.gridWidth = 1;
				centerPanel.add(
					spinnerPlayerPenalty,
					centerPanelConstraint
				);

				final JLabel labelPlayerTotalPoint = new JLabel(
					"",
					SwingConstants.RIGHT
				);
				labelPlayerTotalPoint.setBorder(
					BorderFactory.createCompoundBorder(
						BorderFactory.createLoweredSoftBevelBorder(),
						BorderFactory.createEmptyBorder(
							0,
							4,
							0,
							4
						)
					)
				);
				labelPlayerTotalPoint.setPreferredSize(labelSize);
				labelPlayerTotalPoints.add(
					playerIndex,
					labelPlayerTotalPoint
				);
				centerPanelConstraint.x = 7;
				centerPanelConstraint.gridWidth = 1;
				centerPanel.add(
					labelPlayerTotalPoint,
					centerPanelConstraint
				);

				final JLabel labelPlayerMatchPoint = new JLabel(
					"",
					SwingConstants.RIGHT
				);
				labelPlayerMatchPoint.setBorder(
					BorderFactory.createCompoundBorder(
						BorderFactory.createLoweredSoftBevelBorder(),
						BorderFactory.createEmptyBorder(
							0,
							4,
							0,
							4
						)
					)
				);
				labelPlayerMatchPoint.setPreferredSize(labelSize);
				labelPlayerMatchPoints.add(
					playerIndex,
					labelPlayerMatchPoint
				);
				centerPanelConstraint.x = 8;
				centerPanelConstraint.gridWidth = 1;
				centerPanel.add(
					labelPlayerMatchPoint,
					centerPanelConstraint
				);
			}

			labelScoreError = new JLabel(
				"",
				SwingConstants.RIGHT
			);
			labelScoreError.setBorder(
				BorderFactory.createCompoundBorder(
					BorderFactory.createLineBorder(Color.BLACK),
					BorderFactory.createEmptyBorder(
						0,
						4,
						0,
						4
					)
				)
			);
			labelScoreError.setForeground(Color.RED);
			centerPanelConstraint.y = 5;
			centerPanelConstraint.x = 4;
			centerPanelConstraint.gridWidth = 1;
			centerPanel.add(
				labelScoreError,
				centerPanelConstraint
			);

			innerPanel.add(
				centerPanel,
				BorderLayout.CENTER
			);
		}

		{
			southPanel = new JPanel();
			southPanel.setOpaque(false);
			southPanel.setBackground(UNSAVED_BACKGROUND_COLOR);
			southPanel.setLayout(
				new ProportionalGridLayoutInteger(
					1,
					7,
					0,
					0
				)
			);
			final ProportionalGridLayoutConstraint southPanelConstraint = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			buttonCalculate = new JButton(translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_BUTTON_CALCULATE));
			southPanelConstraint.x = 1;
			southPanel.add(
				buttonCalculate,
				southPanelConstraint
			);

			buttonSave = new JButton(translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_BUTTON_SAVE));
			southPanelConstraint.x = 3;
			southPanel.add(
				buttonSave,
				southPanelConstraint
			);

			buttonReset = new JButton(translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_BUTTON_RESET));
			southPanelConstraint.x = 5;
			southPanel.add(
				buttonReset,
				southPanelConstraint
			);

			innerPanel.add(
				southPanel,
				BorderLayout.SOUTH
			);
		}

		setLayout(new GridBagLayout());
		final GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.anchor = GridBagConstraints.NORTH;
		c.fill = GridBagConstraints.NONE;
		add(
			innerPanel,
			c
		);

		listTournament = new ArrayList<Tournament<TournamentScoreMCR, ScoreMCR>>();
		listCurrentTables = new ArrayList<TournamentScoreMCR>();
		listCurrentTablesTemp = new ArrayList<TournamentScoreMCR>();

		tournamentComboBoxActionListener = (final ActionEvent e) -> {
			changeTournament();
		};
		sessionTableComboBoxActionListener = (final ActionEvent e) -> {
			changeTable();
		};

		buttonCalculate.addActionListener((final ActionEvent e) -> {
			calculateMatchPoint();
		});
		buttonSave.addActionListener((final ActionEvent e) -> {
			saveScore();
		});
		buttonReset.addActionListener((final ActionEvent e) -> {
			reset();
		});
	}

	@Override
	public String getTabName() {
		return translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_TAB_NAME);
	}

	@Override
	public void remoteRefresh() {
		refresh(false);
	}

	@Override
	public boolean hasUnsavedChanges() {
		return unsaved;
	}

	@Override
	public void refresh(final boolean hard) {
		int selectedTournamentIndex = comboTournament.getSelectedIndex();
		Tournament<TournamentScoreMCR, ScoreMCR> selectedTournament = null;
		if (selectedTournamentIndex >= 0) {
			selectedTournament = listTournament.get(selectedTournamentIndex);
		}

		comboTournament.removeActionListener(tournamentComboBoxActionListener);
		comboTournament.removeAllItems();
		listTournament.clear();
		for (final Tournament<TournamentScoreMCR, ScoreMCR> tournament : tournamentManager.getAllTournament()) {
			if (!tournament.isArchived()) {
				listTournament.add(tournament);
			}
		}

		if (listTournament.size() > 0) {
			Collections.sort(listTournament);

			selectedTournamentIndex = -1;
			for (int index = 0; index < listTournament.size(); index++) {
				final Tournament<TournamentScoreMCR, ScoreMCR> tournament = listTournament.get(index);
				if (tournament == selectedTournament) {
					selectedTournamentIndex = index;
				}
				comboTournament.addItem(tournament.getName());
			}

			if (selectedTournamentIndex >= 0) {
				comboTournament.setSelectedIndex(selectedTournamentIndex);
				if (hard) {
					changeTournament();
				} else {
					changeTable();
				}
			} else {
				comboTournament.setSelectedIndex(0);
				changeTournament();
			}
		} else {
			changeTournament();
		}
		comboTournament.addActionListener(tournamentComboBoxActionListener);
	}

	private void changeTournament() {
		comboSession.removeActionListener(sessionTableComboBoxActionListener);
		comboTable.removeActionListener(sessionTableComboBoxActionListener);
		comboSession.removeAllItems();
		comboTable.removeAllItems();

		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		if (selectedTournamentIndex >= 0) {
			final Tournament<TournamentScoreMCR, ScoreMCR> selectedTournament = listTournament.get(selectedTournamentIndex);
			for (int session = 1; session <= selectedTournament.getNbExistingSessions(); session++) {
				comboSession.addItem(SESSION_ID_FORMAT.format(session));
			}
			for (int table = 1; table <= selectedTournament.getNbTables(); table++) {
				comboTable.addItem(TABLE_ID_FORMAT.format(table));
			}
			comboTable.setSelectedIndex(0);
			comboSession.setSelectedIndex(0);
		}

		changeTable();
		comboSession.addActionListener(sessionTableComboBoxActionListener);
		comboTable.addActionListener(sessionTableComboBoxActionListener);
	}

	private void changeTable() {
		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		final int selectedSession = comboSession.getSelectedIndex() + 1;
		final int selectedTable = comboTable.getSelectedIndex() + 1;
		if (selectedTournamentIndex >= 0 && selectedSession >= 1 && selectedTable >= 1) {
			final Tournament<TournamentScoreMCR, ScoreMCR> tournament = listTournament.get(selectedTournamentIndex);
			listCurrentTables.clear();
			listCurrentTablesTemp.clear();
			for (final TournamentScoreMCR table : tournament.getTables()) {
				if (table.getSessionId() == selectedSession && table.getTableId() == selectedTable) {
					listCurrentTables.add(table);
					listCurrentTablesTemp.add(
						new TournamentScoreMCR(
							tournament,
							0,
							0,
							0,
							null,
							0,
							0,
							0,
							0,
							0,
							0,
							false
						)
					);
				}
			}
			Collections.sort(listCurrentTables);

			for (int seatIndex = 0; seatIndex < listCurrentTables.size(); seatIndex++) {
				final TournamentScoreMCR table = listCurrentTables.get(seatIndex);
				final TournamentPlayer tournamentPlayer = table.getTournamentPlayer();

				labelPlayerRankings.get(seatIndex).setText(Integer.toString(table.getRanking()));
				labelPlayers.get(seatIndex).setText(tournamentPlayer.getIDPlayerName(translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_TITLE_PLAYER_EMPTY)));
				comboBoxSubstitutions.get(seatIndex).setSelectedIndex(table.getSubstituted());
				spinnerPlayerTablePoints.get(seatIndex).setValue(table.getTablePoint());
				spinnerPlayerPenalties.get(seatIndex).setValue(table.getPenalty());
				labelPlayerTotalPoints.get(seatIndex).setText(Integer.toString(table.getTotalPoint()));
				labelPlayerMatchPoints.get(seatIndex).setText(
					fraction(
						table.getMatchPoint(),
						12
					)
				);
			}
			labelScoreError.setText("");
		} else {
			for (int seatIndex = 0; seatIndex < NUMBER_OF_PLAYERS; seatIndex++) {
				labelPlayerRankings.get(seatIndex).setText("");
				labelPlayers.get(seatIndex).setText("");
				comboBoxSubstitutions.get(seatIndex).setSelectedIndex(0);
				spinnerPlayerTablePoints.get(seatIndex).setValue(0);
				spinnerPlayerPenalties.get(seatIndex).setValue(0);
				labelPlayerTotalPoints.get(seatIndex).setText("");
				labelPlayerMatchPoints.get(seatIndex).setText("");
			}
			labelScoreError.setText("");
		}

		unsaved = false;
		southPanel.setOpaque(false);
		hideSaveButton();
		repaint();
	}

	private void calculateMatchPoint() {
		hideSaveButton();

		final List<TournamentScoreMCR> substitutedPlayerScoreList = new ArrayList<TournamentScoreMCR>();
		final List<TournamentScoreMCR> normalPlayerScoreList = new ArrayList<TournamentScoreMCR>();

		int totalScore = 0;
		int playerIndex;
		for (playerIndex = 0; playerIndex < NUMBER_OF_PLAYERS; playerIndex++) {
			final TournamentScoreMCR table = listCurrentTablesTemp.get(playerIndex);
			table.setSubstituted(comboBoxSubstitutions.get(playerIndex).getSelectedIndex());

			switch (table.getSubstituted()) {
				case 0:
					normalPlayerScoreList.add(table);
					break;
				case 1:
					substitutedPlayerScoreList.add(table);
					break;
			}

			// Check game score
			table.setTablePoint((int) spinnerPlayerTablePoints.get(playerIndex).getValue());
			table.setPenalty((int) spinnerPlayerPenalties.get(playerIndex).getValue());
			table.setTotalPoint(table.getTablePoint() - table.getPenalty());
			totalScore += table.getTablePoint();
		}

		// Check total score
		if (substitutedPlayerScoreList.size() == 0 && totalScore != 0) {
			new UIMessageDialog(
				(JFrame) SwingUtilities.getWindowAncestor(this),
				translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_MCR_CHECK_SCORE_TABLE_SCORE_SUM_MESSAGE),
				translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_MCR_CHECK_SCORE_TABLE_SCORE_SUM_TITLE),
				MessageDialogMessageType.WARNING,
				MessageDialogOptionsType.OK,
				MessageDialogOption.OK,
				translator
			).showOptionDialog();
			labelScoreError.setText(Integer.toString(totalScore));
		} else {
			labelScoreError.setText("");
		}

		// Sort score list
		Collections.sort(
			normalPlayerScoreList,
			(
				final TournamentScoreMCR o1,
				final TournamentScoreMCR o2
			) -> {
				return -Integer.compare(
					o1.getTotalPoint(),
					o2.getTotalPoint()
				);
			}
		);

		playerIndex = 0;
		while (playerIndex < normalPlayerScoreList.size()) {
			int equalityPlayerIndex = playerIndex;
			int totalMatchPoint = 0;
			while (equalityPlayerIndex < normalPlayerScoreList.size() && normalPlayerScoreList.get(playerIndex).getTablePoint() == normalPlayerScoreList.get(equalityPlayerIndex).getTablePoint()) {
				totalMatchPoint += MATCH_POINTS[equalityPlayerIndex];
				equalityPlayerIndex++;
			}

			totalMatchPoint /= equalityPlayerIndex - playerIndex;
			for (int index = playerIndex; index < equalityPlayerIndex; index++) {
				final TournamentScoreMCR playerScore = normalPlayerScoreList.get(index);
				playerScore.setMatchPoint(totalMatchPoint);
				playerScore.setRanking(playerIndex + 1);
			}
			playerIndex = equalityPlayerIndex;
		}

		for (playerIndex = 0; playerIndex < substitutedPlayerScoreList.size(); playerIndex++) {
			final TournamentScoreMCR playerScore = substitutedPlayerScoreList.get(playerIndex);
			playerScore.setRanking(SUBSTITUTED_MATCH_POINT_INDEX + 1);
			playerScore.setTablePoint(0);
			playerScore.setPenalty(0);
			playerScore.setTotalPoint(0);
			playerScore.setMatchPoint(MATCH_POINTS[SUBSTITUTED_MATCH_POINT_INDEX]);
		}

		// Update display
		for (int index = 0; index < NUMBER_OF_PLAYERS; index++) {
			final TournamentScoreMCR playerScore = listCurrentTablesTemp.get(index);
			spinnerPlayerTablePoints.get(index).setValue(playerScore.getTablePoint());
			labelPlayerRankings.get(index).setText(Integer.toString(playerScore.getRanking()));
			labelPlayerTotalPoints.get(index).setText(Integer.toString(playerScore.getTotalPoint()));
			labelPlayerMatchPoints.get(index).setText(
				fraction(
					playerScore.getMatchPoint(),
					12
				)
			);
		}

		unsaved = true;
		southPanel.setOpaque(true);
		showSaveButton();
		repaint();
	}

	private void hideSaveButton() {
		buttonSave.setEnabled(false);
	}

	private void showSaveButton() {
		buttonSave.setEnabled(true);
	}

	private void saveScore() {
		boolean result = true;
		for (int playerIndex = 0; playerIndex < listCurrentTables.size(); playerIndex++) {
			final TournamentScoreMCR newTable = listCurrentTablesTemp.get(playerIndex);
			final TournamentScoreMCR table = listCurrentTables.get(playerIndex);
			table.setSubstituted(newTable.getSubstituted());
			table.setRanking(newTable.getRanking());
			table.setTablePoint(newTable.getTablePoint());
			table.setPenalty(newTable.getPenalty());
			table.setTotalPoint(newTable.getTotalPoint());
			table.setMatchPoint(newTable.getMatchPoint());
			table.setScoreRecorded(true);
			result = result && tournamentManager.updateTournamentScore(table);
		}

		if (result) {
			new UIMessageDialog(
				(JFrame) SwingUtilities.getWindowAncestor(this),
				translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_SAVE_RESULT_SCORE_SAVED_MESSAGE),
				translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_SAVE_RESULT_SCORE_SAVED_TITLE),
				MessageDialogMessageType.INFORMATION,
				MessageDialogOptionsType.OK,
				MessageDialogOption.OK,
				translator
			).showOptionDialog();
			informChange();
			unsaved = false;
			southPanel.setOpaque(false);
			repaint();
		} else {
			new UIMessageDialog(
				(JFrame) SwingUtilities.getWindowAncestor(this),
				translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_SAVE_RESULT_SCORE_NOT_SAVED_MESSAGE),
				translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_SAVE_RESULT_SCORE_NOT_SAVED_TITLE),
				MessageDialogMessageType.ERROR,
				MessageDialogOptionsType.OK,
				MessageDialogOption.OK,
				translator
			).showOptionDialog();
		}
	}

	private void reset() {
		final MessageDialogOption answer = new UIMessageDialog(
			(JFrame) SwingUtilities.getWindowAncestor(this),
			translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_RESET_SCORE_CONFIRM_MESSAGE),
			translator.translate(UIText.GUI_PRIMARY_INPUT_SCORE_RESET_SCORE_CONFIRM_TITLE),
			MessageDialogMessageType.QUESTION,
			MessageDialogOptionsType.YES_NO,
			MessageDialogOption.NO,
			translator
		).showOptionDialog();
		if (answer == MessageDialogOption.YES) {
			boolean result = true;
			for (int playerIndex = 0; playerIndex < listCurrentTables.size(); playerIndex++) {
				final TournamentScoreMCR table = listCurrentTables.get(playerIndex);
				table.setSubstituted(0);
				table.setRanking(1);
				table.setTablePoint(0);
				table.setPenalty(0);
				table.setTotalPoint(0);
				table.setMatchPoint(0);
				table.setScoreRecorded(false);
				result = result && tournamentManager.updateTournamentScore(table);

				labelPlayerRankings.get(playerIndex).setText("");
				spinnerPlayerTablePoints.get(playerIndex).setValue(0);
				spinnerPlayerPenalties.get(playerIndex).setValue(0);
				labelPlayerTotalPoints.get(playerIndex).setText("");
				labelPlayerMatchPoints.get(playerIndex).setText("");
			}

			labelScoreError.setText("");
			hideSaveButton();
			informChange();
			unsaved = false;
			southPanel.setOpaque(false);
			repaint();
		}
	}
}
