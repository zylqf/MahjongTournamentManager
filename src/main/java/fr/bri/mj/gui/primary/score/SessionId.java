/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.primary.score;

import java.text.DecimalFormat;

public class SessionId {

	private static final DecimalFormat SESSION_ID_FORMAT = new DecimalFormat("00");

	private final int session;
	private final String sessionString;

	public SessionId(
		final int session,
		final String sessionTitle
	) {
		this.session = session;
		sessionString = sessionTitle
			+ " "
			+ SESSION_ID_FORMAT.format(session);
	}

	public int getSession() {
		return session;
	}

	@Override
	public String toString() {
		return sessionString;
	}
}
