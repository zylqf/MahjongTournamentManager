/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.primary.managetournament;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;

import fr.bri.awt.ProportionalGridLayoutConstraint;
import fr.bri.awt.ProportionalGridLayoutInteger;
import fr.bri.mj.data.score.aggregated.QualificationMode;
import fr.bri.mj.data.score.aggregated.ranking.calculator.single.RankingCalculatorSingleFactory;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.single.RankingScoreSingleIndividual;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentFactory;
import fr.bri.mj.data.tournament.TournamentScore;
import fr.bri.mj.dataaccess.TournamentManager;
import fr.bri.mj.gui.common.UITabPanelWithWaterMark;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.dialog.UIMessageDialog;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogMessageType;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOption;
import fr.bri.mj.gui.dialog.UIMessageDialog.MessageDialogOptionsType;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.swing.JDatePickerYearMonthTime;

public class UITabPanelManageTournament<ScoreClassType extends TournamentScore<ScoreClassType, ScoreEnumType>, ScoreEnumType extends Enum<ScoreEnumType>> extends UITabPanelWithWaterMark {

	private static final long serialVersionUID = 4434648898971318812L;

	private final TournamentFactory<ScoreClassType, ScoreEnumType> tournamentFactory;
	private final RankingCalculatorSingleFactory<ScoreClassType, ScoreEnumType> calculatorSingleFactory;
	private final ScoreEnumType rankingScoreType;

	private final TournamentManager<ScoreClassType, ScoreEnumType> tournamentManager;
	private final SimpleDateFormat dateFormat;

	private final JComboBox<String> comboTournament;
	private final ActionListener comboBoxTournamentActionListener;
	private final JTextField textModifyTournamentName;
	private final JTextField textModifyTournamentLeagueName;
	private final JRadioButton buttonTournamentOpen;
	private final JRadioButton buttonTournamentArchived;
	private final ButtonGroup buttonGroupTournamentArchived;

	private final JLabel labelQualificaton;
	private final JLabel labelNbSessions;
	private final JLabel labelNbExistingSession;
	private final JLabel labelNbRoundRobinSessions;
	private final JButton buttonAddRankingBasedSession;
	private final JButton buttonDeleteLastSession;

	private final JPanel panelTournamentPlanning;
	private final List<JLabel> listLabelDateTime;
	private final List<JButton> listButtonChangeDateTime;
	private final List<Date> listTournamentPlanning;

	private final JButton buttonModifyTournament;
	private final JButton buttonDeleteTournament;

	private final List<Tournament<ScoreClassType, ScoreEnumType>> listTournament;

	private boolean unsaved;

	public UITabPanelManageTournament(
		final TournamentManager<ScoreClassType, ScoreEnumType> tournamentManager,
		final UIConfig config,
		final UITextTranslator translator,
		final TournamentFactory<ScoreClassType, ScoreEnumType> tournamentFactory,
		final RankingCalculatorSingleFactory<ScoreClassType, ScoreEnumType> calculatorSingleFactory,
		final ScoreEnumType rankingScoreType
	) {
		super(
			config,
			translator
		);
		this.tournamentFactory = tournamentFactory;
		this.calculatorSingleFactory = calculatorSingleFactory;
		this.rankingScoreType = rankingScoreType;

		this.tournamentManager = tournamentManager;
		dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		final JPanel tournamentPanel = new JPanel();
		tournamentPanel.setOpaque(false);
		tournamentPanel.setLayout(new GridBagLayout());
		tournamentPanel.setMinimumSize(
			new Dimension(
				800,
				0
			)
		);
		final GridBagConstraints tournamentPanelC = new GridBagConstraints(
			0,
			0,
			1,
			1,
			1.0,
			0.0,
			GridBagConstraints.CENTER,
			GridBagConstraints.HORIZONTAL,
			new Insets(
				16,
				0,
				16,
				0
			),
			0,
			0
		);

		{
			final JPanel tournamentSelectPanel = new JPanel();
			tournamentSelectPanel.setBorder(
				BorderFactory.createTitledBorder(
					BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_TITLE_SELECT_TOURNAMENT)
				)
			);
			tournamentSelectPanel.setLayout(
				new ProportionalGridLayoutInteger(
					1,
					5,
					2,
					2
				)
			);
			final ProportionalGridLayoutConstraint tournamentC = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			tournamentC.y = 0;
			tournamentC.x = 0;
			tournamentC.gridWidth = 1;
			tournamentSelectPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_TITLE_TOURNAMENT),
					SwingConstants.RIGHT
				),
				tournamentC
			);

			tournamentC.x = 1;
			tournamentC.gridWidth = 4;
			comboTournament = new JComboBox<String>();
			tournamentSelectPanel.add(
				comboTournament,
				tournamentC
			);

			tournamentPanelC.gridy = 0;
			tournamentPanel.add(
				tournamentSelectPanel,
				tournamentPanelC
			);
		}

		{
			final JPanel tournamentNamePanel = new JPanel();
			tournamentNamePanel.setBorder(
				BorderFactory.createTitledBorder(
					BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_TITLE_MODIFY_TOURNAMENT_NAME)
				)
			);
			tournamentNamePanel.setLayout(
				new ProportionalGridLayoutInteger(
					2,
					5,
					2,
					2
				)
			);
			final ProportionalGridLayoutConstraint tournamentC = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			tournamentC.y = 0;
			tournamentC.x = 0;
			tournamentC.gridWidth = 1;
			tournamentNamePanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_TITLE_TOURNAMENT_NAME),
					SwingConstants.RIGHT
				),
				tournamentC
			);

			tournamentC.x = 1;
			tournamentC.gridWidth = 4;
			textModifyTournamentName = new JTextField();
			tournamentNamePanel.add(
				textModifyTournamentName,
				tournamentC
			);

			tournamentC.y = 1;
			tournamentC.x = 0;
			tournamentC.gridWidth = 1;
			tournamentNamePanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_TITLE_TOURNAMENT_LEAGUE_NAME),
					SwingConstants.RIGHT
				),
				tournamentC
			);

			tournamentC.x = 1;
			tournamentC.gridWidth = 4;
			textModifyTournamentLeagueName = new JTextField();
			tournamentNamePanel.add(
				textModifyTournamentLeagueName,
				tournamentC
			);

			tournamentPanelC.gridy = 1;
			tournamentPanel.add(
				tournamentNamePanel,
				tournamentPanelC
			);
		}

		{
			final JPanel tournamentArchivedPanel = new JPanel();
			tournamentArchivedPanel.setBorder(
				BorderFactory.createTitledBorder(
					BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_TITLE_ARCHIVE_TOURNAMENT)
				)
			);
			tournamentArchivedPanel.setLayout(
				new ProportionalGridLayoutInteger(
					1,
					2,
					2,
					2
				)
			);
			final ProportionalGridLayoutConstraint tournamentC = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			tournamentC.y = 0;
			tournamentC.x = 0;
			tournamentC.gridWidth = 1;
			buttonTournamentArchived = new JRadioButton(translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_TITLE_ARCHIVE_YES));
			tournamentArchivedPanel.add(
				buttonTournamentArchived,
				tournamentC
			);

			tournamentC.x = 1;
			tournamentC.gridWidth = 1;
			buttonTournamentOpen = new JRadioButton(translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_TITLE_ARCHIVE_NO));
			tournamentArchivedPanel.add(
				buttonTournamentOpen,
				tournamentC
			);

			buttonGroupTournamentArchived = new ButtonGroup();
			buttonGroupTournamentArchived.add(buttonTournamentArchived);
			buttonGroupTournamentArchived.add(buttonTournamentOpen);

			tournamentPanelC.gridy = 2;
			tournamentPanel.add(
				tournamentArchivedPanel,
				tournamentPanelC
			);
		}

		{
			final JPanel tournamentSessionPanel = new JPanel();
			tournamentSessionPanel.setBorder(
				BorderFactory.createTitledBorder(
					BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_TITLE_MODIFY_TOURNAMENT_SESSION)
				)
			);
			tournamentSessionPanel.setLayout(
				new ProportionalGridLayoutInteger(
					3,
					6,
					2,
					2
				)
			);
			final ProportionalGridLayoutConstraint tournamentC = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			tournamentC.y = 0;
			tournamentC.x = 0;
			tournamentC.gridWidth = 1;
			tournamentSessionPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_TITLE_WITH_QUALIFICATION),
					SwingConstants.RIGHT
				),
				tournamentC
			);

			tournamentC.x = 1;
			tournamentC.gridWidth = 1;
			labelQualificaton = new JLabel();
			tournamentSessionPanel.add(
				labelQualificaton,
				tournamentC
			);

			tournamentC.y = 1;
			tournamentC.x = 0;
			tournamentC.gridWidth = 1;
			tournamentSessionPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_TITLE_SESSIONS),
					SwingConstants.RIGHT
				),
				tournamentC
			);

			tournamentC.x = 1;
			tournamentC.gridWidth = 1;
			labelNbSessions = new JLabel();
			tournamentSessionPanel.add(
				labelNbSessions,
				tournamentC
			);

			tournamentC.x = 2;
			tournamentC.gridWidth = 1;
			tournamentSessionPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_TITLE_EXISTINGS_SESSIONS),
					SwingConstants.RIGHT
				),
				tournamentC
			);

			tournamentC.x = 3;
			tournamentC.gridWidth = 1;
			labelNbExistingSession = new JLabel();
			tournamentSessionPanel.add(
				labelNbExistingSession,
				tournamentC
			);

			tournamentC.x = 4;
			tournamentC.gridWidth = 1;
			tournamentSessionPanel.add(
				new JLabel(
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_TITLE_ROUND_ROBIN_SESSIONS),
					SwingConstants.RIGHT
				),
				tournamentC
			);

			tournamentC.x = 5;
			tournamentC.gridWidth = 1;
			labelNbRoundRobinSessions = new JLabel();
			tournamentSessionPanel.add(
				labelNbRoundRobinSessions,
				tournamentC
			);

			tournamentC.y = 2;
			tournamentC.x = 0;
			tournamentC.gridWidth = 2;
			buttonDeleteLastSession = new JButton(translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_BUTTON_DELETE_LAST_SESSION));
			tournamentSessionPanel.add(
				buttonDeleteLastSession,
				tournamentC
			);

			tournamentC.x = 4;
			tournamentC.gridWidth = 2;
			buttonAddRankingBasedSession = new JButton(translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_BUTTON_ADD_NEW_SESSION));
			tournamentSessionPanel.add(
				buttonAddRankingBasedSession,
				tournamentC
			);

			tournamentPanelC.gridy = 3;
			tournamentPanel.add(
				tournamentSessionPanel,
				tournamentPanelC
			);
		}

		{
			panelTournamentPlanning = new JPanel();
			panelTournamentPlanning.setBorder(
				BorderFactory.createTitledBorder(
					BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_TITLE_MODIFY_PLANNING)
				)
			);

			tournamentPanelC.gridy = 4;
			tournamentPanel.add(
				panelTournamentPlanning,
				tournamentPanelC
			);
		}

		{
			final JPanel panelSaveDeleteTournament = new JPanel();
			panelSaveDeleteTournament.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
			panelSaveDeleteTournament.setLayout(
				new ProportionalGridLayoutInteger(
					1,
					4,
					2,
					2
				)
			);
			final ProportionalGridLayoutConstraint tournamentC = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);

			tournamentC.y = 0;
			tournamentC.x = 0;
			tournamentC.gridWidth = 1;
			buttonDeleteTournament = new JButton(translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_BUTTON_DELETE_TOURNAMENT));
			panelSaveDeleteTournament.add(
				buttonDeleteTournament,
				tournamentC
			);

			tournamentC.x = 3;
			tournamentC.gridWidth = 1;
			buttonModifyTournament = new JButton(translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_BUTTON_MODIFY_TOURNAMENT));
			panelSaveDeleteTournament.add(
				buttonModifyTournament,
				tournamentC
			);

			tournamentPanelC.gridy = 5;
			tournamentPanel.add(
				panelSaveDeleteTournament,
				tournamentPanelC
			);
		}

		final JPanel supportPanel = new JPanel(new GridBagLayout());
		supportPanel.setOpaque(false);
		final GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.gridheight = 1;
		c.gridwidth = 1;
		c.weightx = 1.0;
		c.weighty = 1.0;
		c.anchor = GridBagConstraints.NORTH;
		c.fill = GridBagConstraints.NONE;
		supportPanel.add(
			tournamentPanel,
			c
		);

		final JScrollPane scroll = new JScrollPane(
			supportPanel,
			ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER
		);
		scroll.getVerticalScrollBar().setUnitIncrement(16);
		scroll.setOpaque(false);
		scroll.getViewport().setOpaque(false);

		setLayout(new BorderLayout());
		add(
			scroll,
			BorderLayout.CENTER
		);

		comboBoxTournamentActionListener = (final ActionEvent e) -> {
			displayTournament();
		};
		comboTournament.addActionListener(comboBoxTournamentActionListener);
		buttonModifyTournament.addActionListener((final ActionEvent e) -> {
			modifyTournament();
		});
		buttonDeleteTournament.addActionListener((final ActionEvent e) -> {
			deleteTournament();
		});
		buttonAddRankingBasedSession.addActionListener((final ActionEvent e) -> {
			addRankingBasedSession();
		});
		buttonDeleteLastSession.addActionListener((final ActionEvent e) -> {
			deleteLastSession();
		});

		listLabelDateTime = new ArrayList<JLabel>();
		listButtonChangeDateTime = new ArrayList<JButton>();
		listTournamentPlanning = new ArrayList<Date>();
		listTournament = new ArrayList<Tournament<ScoreClassType, ScoreEnumType>>();
	}

	@Override
	public String getTabName() {
		return translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_TAB_NAME);
	}

	@Override
	public void remoteRefresh() {
		refresh(false);
	}

	@Override
	public void refresh(final boolean hard) {
		int selectedTournamentIndex = comboTournament.getSelectedIndex();
		Tournament<ScoreClassType, ScoreEnumType> selectedTournament = null;
		if (selectedTournamentIndex >= 0) {
			selectedTournament = listTournament.get(selectedTournamentIndex);
		}
		listTournament.clear();

		comboTournament.removeActionListener(comboBoxTournamentActionListener);
		comboTournament.removeAllItems();

		final Set<Tournament<ScoreClassType, ScoreEnumType>> existingTournaments = tournamentManager.getAllTournament();
		if (existingTournaments.size() > 0) {
			listTournament.addAll(existingTournaments);
			Collections.sort(listTournament);

			selectedTournamentIndex = 0;
			for (int index = 0; index < listTournament.size(); index++) {
				final Tournament<ScoreClassType, ScoreEnumType> tournament = listTournament.get(index);
				if (tournament == selectedTournament) {
					selectedTournamentIndex = index;
				}
				comboTournament.addItem(tournament.getName());
			}

			comboTournament.setSelectedIndex(selectedTournamentIndex);
		}

		displayTournament();
		comboTournament.addActionListener(comboBoxTournamentActionListener);
	}

	@Override
	public boolean hasUnsavedChanges() {
		return unsaved;
	}

	private void displayTournament() {
		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		invalidate();
		textModifyTournamentName.setText("");
		textModifyTournamentLeagueName.setText("");

		labelNbSessions.setText("");
		labelNbExistingSession.setText("");
		buttonDeleteLastSession.setEnabled(false);
		buttonAddRankingBasedSession.setEnabled(false);

		panelTournamentPlanning.removeAll();
		listLabelDateTime.clear();
		listButtonChangeDateTime.clear();

		if (selectedTournamentIndex != -1) {
			final Tournament<ScoreClassType, ScoreEnumType> tournament = listTournament.get(selectedTournamentIndex);
			textModifyTournamentName.setText(tournament.getName());
			textModifyTournamentLeagueName.setText(tournament.getLeagueName());
			if (tournament.isArchived()) {
				buttonGroupTournamentArchived.setSelected(
					buttonTournamentArchived.getModel(),
					true
				);
			} else {
				buttonGroupTournamentArchived.setSelected(
					buttonTournamentOpen.getModel(),
					true
				);
			}

			if (tournament.isWithQualification()) {
				labelQualificaton.setText(translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_TITLE_WITH_QUALIFICATION_YES));
			} else {
				labelQualificaton.setText(translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_TITLE_WITH_QUALIFICATION_NO));
			}
			labelNbSessions.setText(Integer.toString(tournament.getNbSessions()));
			labelNbExistingSession.setText(Integer.toString(tournament.getNbExistingSessions()));
			labelNbRoundRobinSessions.setText(Integer.toString(tournament.getNbRoundRobinSessions()));

			listTournamentPlanning.clear();
			for (final Integer sessionId : tournament.getPlannings().keySet()) {
				listTournamentPlanning.add(tournament.getPlannings().get(sessionId));
			}
			panelTournamentPlanning.setLayout(
				new ProportionalGridLayoutInteger(
					tournament.getNbSessions(),
					6,
					2,
					2
				)
			);
			final ProportionalGridLayoutConstraint planningC = new ProportionalGridLayoutConstraint(
				0,
				1,
				0,
				1
			);
			for (int session = 0; session < tournament.getNbSessions(); session++) {
				planningC.y = session;

				planningC.x = 0;
				planningC.gridWidth = 1;
				final JLabel labelSessionNumber = new JLabel(
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_TITLE_SESSION)
						+ " "
						+ SESSION_ID_FORMAT.format(session + 1),
					SwingConstants.RIGHT
				);
				panelTournamentPlanning.add(
					labelSessionNumber,
					planningC
				);

				planningC.x = 1;
				planningC.gridWidth = 4;
				final JLabel labelSessionDateTime = new JLabel(
					dateFormat.format(listTournamentPlanning.get(session)),
					SwingConstants.CENTER
				);
				panelTournamentPlanning.add(
					labelSessionDateTime,
					planningC
				);
				listLabelDateTime.add(labelSessionDateTime);

				planningC.x = 5;
				planningC.gridWidth = 1;
				final JButton buttonChangeDateTime = new JButton(translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_BUTTON_MODIFY_PLANNING));
				panelTournamentPlanning.add(
					buttonChangeDateTime,
					planningC
				);
				listButtonChangeDateTime.add(buttonChangeDateTime);
				final int sessionIndex = session;
				buttonChangeDateTime.addActionListener((final ActionEvent e) -> {
					changeDateTime(
						sessionIndex,
						buttonChangeDateTime
					);
				});
			}

			buttonDeleteLastSession.setEnabled(tournament.getNbExistingSessions() > tournament.getNbRoundRobinSessions());
			boolean allScoreRecorded = true;
			for (final ScoreClassType table : tournament.getTables()) {
				allScoreRecorded = allScoreRecorded && table.isScoreRecorded();
			}
			buttonAddRankingBasedSession.setEnabled(allScoreRecorded && tournament.getNbExistingSessions() < tournament.getNbSessions());
		}
		validate();
		repaint();
		unsaved = false;
	}

	private void changeDateTime(
		final int session,
		final JComponent component
	) {
		final JDatePickerYearMonthTime datePicker = new JDatePickerYearMonthTime(
			SwingUtilities.getWindowAncestor(this),
			Calendar.MONDAY,
			listTournamentPlanning.get(session)
		);
		final Rectangle bounds = component.getGraphicsConfiguration().getBounds();
		final Point labelDateScreenLocation = component.getLocationOnScreen();
		final int x = Math.min(
			labelDateScreenLocation.x,
			bounds.x + bounds.width - datePicker.getWidth()
		);
		final int y = Math.min(
			labelDateScreenLocation.y,
			bounds.y + bounds.height - datePicker.getHeight()
		);

		datePicker.setLocation(
			x,
			y
		);
		datePicker.setVisible(true);
		final Date selectedDate = datePicker.getSelectedDate();
		if (selectedDate != null) {
			listTournamentPlanning.set(
				session,
				selectedDate
			);
			listLabelDateTime.get(session).setText(dateFormat.format(selectedDate));

			int sessionIndex = session - 1;
			while (sessionIndex >= 0 && listTournamentPlanning.get(sessionIndex).after(selectedDate)) {
				listTournamentPlanning.set(
					sessionIndex,
					selectedDate
				);
				listLabelDateTime.get(sessionIndex).setText(dateFormat.format(selectedDate));
				sessionIndex--;
			}
			sessionIndex = session + 1;
			while (sessionIndex < listTournamentPlanning.size() && listTournamentPlanning.get(sessionIndex).before(selectedDate)) {
				listTournamentPlanning.set(
					sessionIndex,
					selectedDate
				);
				listLabelDateTime.get(sessionIndex).setText(dateFormat.format(selectedDate));
				sessionIndex++;
			}
			unsaved = true;
		}
	}

	// 01st, 08th, 09th and 16th player on the table 1
	// 02nd, 07th, 10th and 15th player on the table 2
	// 03rd, 06th, 11th and 14th player on the table 3
	// 04th, 05th, 12th and 13th player on the table 4
	private static final int QUARTER_FINAL_TABLE[] = {
		0,
		7,
		8,
		15,
		1,
		6,
		9,
		14,
		2,
		5,
		10,
		13,
		3,
		4,
		11,
		12
	};

	// 1st, 4th, 5th and 8th player on the table 1
	// 2nd, 3rd, 6th and 7th player on the table 2
	private static final int SEMI_FINAL_TABLE[] = {
		0,
		3,
		4,
		7,
		1,
		2,
		5,
		6
	};

	// 1st, 2nd, 3rd and 4th player on the table 1
	private static final int FINAL_TABLE[] = {
		0,
		1,
		2,
		3
	};

	private void addRankingBasedSession() {
		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		if (selectedTournamentIndex != -1) {
			final MessageDialogOption answer = new UIMessageDialog(
				(JFrame) SwingUtilities.getWindowAncestor(this),
				translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_ADD_NEW_SESSION_CONFIRM_MESSAGE),
				translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_ADD_NEW_SESSION_CONFIRM_TITLE),
				MessageDialogMessageType.QUESTION,
				MessageDialogOptionsType.YES_NO,
				MessageDialogOption.NO,
				translator
			).showOptionDialog();
			if (answer == MessageDialogOption.YES) {
				final Tournament<ScoreClassType, ScoreEnumType> tournament = listTournament.get(selectedTournamentIndex);
				final int existingSessions = tournament.getNbExistingSessions();
				final int newSessionNumber = existingSessions + 1;
				// No qualification or Qualification sessions
				if (!tournament.isWithQualification() || newSessionNumber <= tournament.getNbSessions() - 6) {
					final List<RankingScoreSingleIndividual> rankingList = calculatorSingleFactory.getRankingCalculator(
						rankingScoreType,
						QualificationMode.WITHOUT
					).getRankingScoreIndividual(
						tournament,
						existingSessions
					);
					for (int rankingIndex = 0; rankingIndex < rankingList.size(); rankingIndex++) {
						final ScoreClassType score = tournamentFactory.createTournamentScore(
							tournament,
							newSessionNumber,
							rankingIndex / 4 + 1,
							rankingIndex % 4 + 1,
							rankingList.get(rankingIndex).getTournamentPlayer(),
							false
						);
						tournament.getTables().add(score);
					}
				} else if (newSessionNumber == tournament.getNbSessions() - 5) {
					// 1/4 final session 1
					final List<RankingScoreSingleIndividual> rankingList = calculatorSingleFactory.getRankingCalculator(
						rankingScoreType,
						QualificationMode.WITH
					).getRankingScoreIndividual(
						tournament,
						existingSessions
					);
					for (int playerIndex = 0; playerIndex < QUARTER_FINAL_TABLE.length; playerIndex++) {
						tournament.getTables().add(
							tournamentFactory.createTournamentScore(
								tournament,
								newSessionNumber,
								playerIndex / 4 + 1,
								playerIndex % 4 + 1,
								rankingList.get(QUARTER_FINAL_TABLE[playerIndex]).getTournamentPlayer(),
								false
							)
						);
					}
					for (int rankingIndex = QUARTER_FINAL_TABLE.length; rankingIndex < rankingList.size(); rankingIndex++) {
						final ScoreClassType score = tournamentFactory.createTournamentScore(
							tournament,
							newSessionNumber,
							rankingIndex / 4 + 1,
							rankingIndex % 4 + 1,
							rankingList.get(rankingIndex).getTournamentPlayer(),
							false
						);
						tournament.getTables().add(score);
					}
				} else if (newSessionNumber == tournament.getNbSessions() - 4) {
					// 1/4 final session 2
					final Set<ScoreClassType> newScores = new HashSet<ScoreClassType>();
					for (final ScoreClassType score : tournament.getTables()) {
						if (score.getSessionId() == tournament.getNbSessions() - 5) {
							if (score.getTableId() >= 1 && score.getTableId() <= 4) {
								final ScoreClassType newScore = tournamentFactory.createTournamentScore(
									tournament,
									newSessionNumber,
									score.getTableId(),
									score.getTablePlayerId(),
									score.getTournamentPlayer(),
									false
								);
								newScores.add(newScore);
							}
						}
					}
					tournament.getTables().addAll(newScores);
					final List<RankingScoreSingleIndividual> rankingList = calculatorSingleFactory.getRankingCalculator(
						rankingScoreType,
						QualificationMode.WITH
					).getRankingScoreIndividual(
						tournament,
						existingSessions
					);
					for (int rankingIndex = QUARTER_FINAL_TABLE.length; rankingIndex < rankingList.size(); rankingIndex++) {
						final ScoreClassType newScore = tournamentFactory.createTournamentScore(
							tournament,
							newSessionNumber,
							rankingIndex / 4 + 1,
							rankingIndex % 4 + 1,
							rankingList.get(rankingIndex).getTournamentPlayer(),
							false
						);
						tournament.getTables().add(newScore);
					}
				} else if (newSessionNumber == tournament.getNbSessions() - 3) {
					// 1/2 final session 1
					final List<RankingScoreSingleIndividual> rankingList = calculatorSingleFactory.getRankingCalculator(
						rankingScoreType,
						QualificationMode.WITH
					).getRankingScoreIndividual(
						tournament,
						existingSessions
					);
					for (int playerIndex = 0; playerIndex < SEMI_FINAL_TABLE.length; playerIndex++) {
						tournament.getTables().add(
							tournamentFactory.createTournamentScore(
								tournament,
								newSessionNumber,
								playerIndex / 4 + 1,
								playerIndex % 4 + 1,
								rankingList.get(SEMI_FINAL_TABLE[playerIndex]).getTournamentPlayer(),
								false
							)
						);
					}
					for (int rankingIndex = SEMI_FINAL_TABLE.length; rankingIndex < rankingList.size(); rankingIndex++) {
						final ScoreClassType newScore = tournamentFactory.createTournamentScore(
							tournament,
							newSessionNumber,
							rankingIndex / 4 + 1,
							rankingIndex % 4 + 1,
							rankingList.get(rankingIndex).getTournamentPlayer(),
							false
						);
						tournament.getTables().add(newScore);
					}
				} else if (newSessionNumber == tournament.getNbSessions() - 2) {
					// 1/2 final session 2
					final Set<ScoreClassType> newScores = new HashSet<ScoreClassType>();
					for (final ScoreClassType score : tournament.getTables()) {
						if (score.getSessionId() == tournament.getNbSessions() - 3) {
							if (score.getTableId() >= 1 && score.getTableId() <= 2) {
								final ScoreClassType newScore = tournamentFactory.createTournamentScore(
									tournament,
									newSessionNumber,
									score.getTableId(),
									score.getTablePlayerId(),
									score.getTournamentPlayer(),
									false
								);
								newScores.add(newScore);
							}
						}
					}
					tournament.getTables().addAll(newScores);
					final List<RankingScoreSingleIndividual> rankingList = calculatorSingleFactory.getRankingCalculator(
						rankingScoreType,
						QualificationMode.WITH
					).getRankingScoreIndividual(
						tournament,
						existingSessions
					);
					for (int rankingIndex = SEMI_FINAL_TABLE.length; rankingIndex < rankingList.size(); rankingIndex++) {
						final ScoreClassType newScore = tournamentFactory.createTournamentScore(
							tournament,
							newSessionNumber,
							rankingIndex / 4 + 1,
							rankingIndex % 4 + 1,
							rankingList.get(rankingIndex).getTournamentPlayer(),
							false
						);
						tournament.getTables().add(newScore);
					}
				} else if (newSessionNumber == tournament.getNbSessions() - 1) {
					// Final session 1
					final List<RankingScoreSingleIndividual> rankingList = calculatorSingleFactory.getRankingCalculator(
						rankingScoreType,
						QualificationMode.WITH
					).getRankingScoreIndividual(
						tournament,
						existingSessions
					);
					for (int playerIndex = 0; playerIndex < FINAL_TABLE.length; playerIndex++) {
						tournament.getTables().add(
							tournamentFactory.createTournamentScore(
								tournament,
								newSessionNumber,
								playerIndex / 4 + 1,
								playerIndex % 4 + 1,
								rankingList.get(FINAL_TABLE[playerIndex]).getTournamentPlayer(),
								false
							)
						);
					}
					for (int rankingIndex = FINAL_TABLE.length; rankingIndex < rankingList.size(); rankingIndex++) {
						final ScoreClassType newScore = tournamentFactory.createTournamentScore(
							tournament,
							newSessionNumber,
							rankingIndex / 4 + 1,
							rankingIndex % 4 + 1,
							rankingList.get(rankingIndex).getTournamentPlayer(),
							true
						);
						tournament.getTables().add(newScore);
					}
				} else if (newSessionNumber == tournament.getNbSessions()) {
					// Final session 2
					final Set<ScoreClassType> newScores = new HashSet<ScoreClassType>();
					for (final ScoreClassType score : tournament.getTables()) {
						if (score.getSessionId() == tournament.getNbSessions() - 1) {
							if (score.getTableId() == 1) {
								final ScoreClassType newScore = tournamentFactory.createTournamentScore(
									tournament,
									newSessionNumber,
									score.getTableId(),
									score.getTablePlayerId(),
									score.getTournamentPlayer(),
									false
								);
								newScores.add(newScore);
							}
						}
					}
					tournament.getTables().addAll(newScores);
					final List<RankingScoreSingleIndividual> rankingList = calculatorSingleFactory.getRankingCalculator(
						rankingScoreType,
						QualificationMode.WITH
					).getRankingScoreIndividual(
						tournament,
						existingSessions
					);
					for (int rankingIndex = FINAL_TABLE.length; rankingIndex < rankingList.size(); rankingIndex++) {
						final ScoreClassType newScore = tournamentFactory.createTournamentScore(
							tournament,
							newSessionNumber,
							rankingIndex / 4 + 1,
							rankingIndex % 4 + 1,
							rankingList.get(rankingIndex).getTournamentPlayer(),
							true
						);
						tournament.getTables().add(newScore);
					}
				}

				final boolean result = tournamentManager.updateTournament(tournament);
				if (result) {
					new UIMessageDialog(
						(JFrame) SwingUtilities.getWindowAncestor(this),
						translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_ADD_NEW_SESSION_RESULT_SESSION_ADDED_MESSAGE),
						translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_ADD_NEW_SESSION_RESULT_SESSION_ADDED_TITLE),
						MessageDialogMessageType.INFORMATION,
						MessageDialogOptionsType.OK,
						MessageDialogOption.OK,
						translator
					).showOptionDialog();
					refresh(false);
					informChange();
					unsaved = false;
				} else {
					new UIMessageDialog(
						(JFrame) SwingUtilities.getWindowAncestor(this),
						translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_ADD_NEW_SESSION_RESULT_SESSION_NOT_ADDED_MESSAGE),
						translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_ADD_NEW_SESSION_RESULT_SESSION_NOT_ADDED_TITLE),
						MessageDialogMessageType.ERROR,
						MessageDialogOptionsType.OK,
						MessageDialogOption.OK,
						translator
					).showOptionDialog();
				}
			}
		}
	}

	private void deleteLastSession() {
		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		if (selectedTournamentIndex != -1) {
			MessageDialogOption answer = new UIMessageDialog(
				(JFrame) SwingUtilities.getWindowAncestor(this),
				translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_DELETE_LAST_SESSION_CONFIRM_1_MESSAGE),
				translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_DELETE_LAST_SESSION_CONFIRM_1_TITLE),
				MessageDialogMessageType.QUESTION,
				MessageDialogOptionsType.YES_NO,
				MessageDialogOption.NO,
				translator
			).showOptionDialog();
			if (answer == MessageDialogOption.YES) {
				answer = new UIMessageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_DELETE_LAST_SESSION_CONFIRM_2_MESSAGE),
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_DELETE_LAST_SESSION_CONFIRM_2_TITLE),
					MessageDialogMessageType.QUESTION,
					MessageDialogOptionsType.YES_NO,
					MessageDialogOption.NO,
					translator
				).showOptionDialog();
				if (answer == MessageDialogOption.YES) {
					final Tournament<ScoreClassType, ScoreEnumType> tournament = listTournament.get(selectedTournamentIndex);
					final int lastSession = tournament.getNbExistingSessions();
					final List<ScoreClassType> scoreToDelete = new ArrayList<ScoreClassType>();
					for (final ScoreClassType score : tournament.getTables()) {
						if (score.getSessionId() == lastSession) {
							scoreToDelete.add(score);
						}
					}
					tournament.getTables().removeAll(scoreToDelete);

					final boolean result = tournamentManager.updateTournament(tournament);
					if (result) {
						new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_DELETE_LAST_SESSION_RESULT_SESSION_DELETED_MESSAGE),
							translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_DELETE_LAST_SESSION_RESULT_SESSION_DELETED_TITLE),
							MessageDialogMessageType.INFORMATION,
							MessageDialogOptionsType.OK,
							MessageDialogOption.OK,
							translator
						).showOptionDialog();
						refresh(false);
						informChange();
						unsaved = false;
					} else {
						new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_DELETE_LAST_SESSION_RESULT_SESSION_NOT_DELETED_MESSAGE),
							translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_DELETE_LAST_SESSION_RESULT_SESSION_NOT_DELETED_TITLE),
							MessageDialogMessageType.ERROR,
							MessageDialogOptionsType.OK,
							MessageDialogOption.OK,
							translator
						).showOptionDialog();
					}
				}
			}
		}
	}

	private void modifyTournament() {
		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		if (selectedTournamentIndex != -1) {
			final Tournament<ScoreClassType, ScoreEnumType> tournament = listTournament.get(selectedTournamentIndex);
			final String tournamentName = textModifyTournamentName.getText().trim();
			if (tournamentName.length() > 0) {
				tournament.setName(tournamentName);
				final String tournamentLeagueName = textModifyTournamentLeagueName.getText().trim();
				tournament.setLeagueName(tournamentLeagueName);
				if (buttonTournamentArchived.isSelected()) {
					tournament.setArchived(true);
				} else if (buttonTournamentOpen.isSelected()) {
					tournament.setArchived(false);
				}
				for (int sessionIndex = 0; sessionIndex < listTournamentPlanning.size(); sessionIndex++) {
					tournament.getPlannings().put(
						sessionIndex + 1,
						listTournamentPlanning.get(sessionIndex)
					);
				}

				final boolean result = tournamentManager.updateTournament(tournament);
				if (result) {
					new UIMessageDialog(
						(JFrame) SwingUtilities.getWindowAncestor(this),
						translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_MODIFY_RESULT_TOURNAMENT_MODIFIED_MESSAGE),
						translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_MODIFY_RESULT_TOURNAMENT_MODIFIED_TITLE),
						MessageDialogMessageType.INFORMATION,
						MessageDialogOptionsType.OK,
						MessageDialogOption.OK,
						translator
					).showOptionDialog();
					refresh(false);
					informChange();
					unsaved = false;
				} else {
					new UIMessageDialog(
						(JFrame) SwingUtilities.getWindowAncestor(this),
						translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_MODIFY_RESULT_TOURNAMENT_NOT_MODIFIED_MESSAGE),
						translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_MODIFY_RESULT_TOURNAMENT_NOT_MODIFIED_TITLE),
						MessageDialogMessageType.ERROR,
						MessageDialogOptionsType.OK,
						MessageDialogOption.OK,
						translator
					).showOptionDialog();
				}
			} else {
				new UIMessageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_MODIFY_RESULT_TOURNAMENT_NAME_EMPTY_MESSAGE),
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_MODIFY_RESULT_TOURNAMENT_NAME_EMPTY_TITLE),
					MessageDialogMessageType.ERROR,
					MessageDialogOptionsType.OK,
					MessageDialogOption.OK,
					translator
				).showOptionDialog();
			}
		}
	}

	private void deleteTournament() {
		final int selectedTournamentIndex = comboTournament.getSelectedIndex();
		if (selectedTournamentIndex != -1) {
			MessageDialogOption answer = new UIMessageDialog(
				(JFrame) SwingUtilities.getWindowAncestor(this),
				translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_DELETE_CONFIRM_1_MESSAGE),
				translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_DELETE_CONFIRM_1_TITLE),
				MessageDialogMessageType.QUESTION,
				MessageDialogOptionsType.YES_NO,
				MessageDialogOption.NO,
				translator
			).showOptionDialog();
			if (answer == MessageDialogOption.YES) {
				answer = new UIMessageDialog(
					(JFrame) SwingUtilities.getWindowAncestor(this),
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_DELETE_CONFIRM_2_MESSAGE),
					translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_DELETE_CONFIRM_2_TITLE),
					MessageDialogMessageType.QUESTION,
					MessageDialogOptionsType.YES_NO,
					MessageDialogOption.NO,
					translator
				).showOptionDialog();
				if (answer == MessageDialogOption.YES) {
					final Tournament<ScoreClassType, ScoreEnumType> tournament = listTournament.get(selectedTournamentIndex);
					if (tournamentManager.deleteTournament(tournament)) {
						new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_DELETE_RESULT_TOURNAMENT_DELETED_MESSAGE),
							translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_DELETE_RESULT_TOURNAMENT_DELETED_TITLE),
							MessageDialogMessageType.INFORMATION,
							MessageDialogOptionsType.OK,
							MessageDialogOption.OK,
							translator
						).showOptionDialog();
						refresh(false);
						informChange();
						unsaved = false;
					} else {
						new UIMessageDialog(
							(JFrame) SwingUtilities.getWindowAncestor(this),
							translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_DELETE_RESULT_TOURNAMENT_NOT_DELETED_MESSAGE),
							translator.translate(UIText.GUI_PRIMARY_MANAGE_TOURNAMENT_DELETE_RESULT_TOURNAMENT_NOT_DELETED_TITLE),
							MessageDialogMessageType.ERROR,
							MessageDialogOptionsType.OK,
							MessageDialogOption.OK,
							translator
						).showOptionDialog();
					}
				}
			}
		}
	}
}
