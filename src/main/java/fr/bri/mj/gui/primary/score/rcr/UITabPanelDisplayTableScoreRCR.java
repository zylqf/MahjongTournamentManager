/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.primary.score.rcr;

import java.util.EnumMap;
import java.util.Map;

import fr.bri.mj.data.score.raw.comparator.ScoreComparator;
import fr.bri.mj.data.score.raw.comparator.ScoreComparatorRCRFactory;
import fr.bri.mj.data.score.raw.getter.ScoreGetter;
import fr.bri.mj.data.score.raw.getter.ScoreGetterRCRFactory;
import fr.bri.mj.data.score.raw.setter.ScoreSetter;
import fr.bri.mj.data.score.raw.setter.ScoreSetterRCRFactory;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.rcr.ScoreRCR;
import fr.bri.mj.data.tournament.rcr.TournamentScoreRCR;
import fr.bri.mj.dataaccess.TournamentManager;
import fr.bri.mj.gui.config.UIConfig;
import fr.bri.mj.gui.language.UITextTranslator;
import fr.bri.mj.gui.primary.score.UIDisplayTableScoreChart;
import fr.bri.mj.gui.primary.score.UITabPanelDisplayTableScore;

public class UITabPanelDisplayTableScoreRCR extends UITabPanelDisplayTableScore<TournamentScoreRCR, ScoreRCR> {

	private static final long serialVersionUID = -8735623154767097338L;

	private static final ScoreRCR SCORES_TO_COMPARE[] = {
		ScoreRCR.FINAL_SCORE
	};

	private final Map<ScoreRCR, ScoreComparator<TournamentScoreRCR, ScoreRCR>> mapComparators;
	private final Map<ScoreRCR, ScoreGetter<TournamentScoreRCR, ScoreRCR>> mapGetters;
	private final Map<ScoreRCR, ScoreSetter<TournamentScoreRCR, ScoreRCR>> mapSetters;

	public UITabPanelDisplayTableScoreRCR(
		final TournamentManager<TournamentScoreRCR, ScoreRCR> dataAccessAdaptor,
		final UIConfig config,
		final UITextTranslator translator
	) {
		super(
			dataAccessAdaptor,
			config,
			translator
		);

		mapComparators = new EnumMap<ScoreRCR, ScoreComparator<TournamentScoreRCR, ScoreRCR>>(ScoreRCR.class);
		mapGetters = new EnumMap<ScoreRCR, ScoreGetter<TournamentScoreRCR, ScoreRCR>>(ScoreRCR.class);
		mapSetters = new EnumMap<ScoreRCR, ScoreSetter<TournamentScoreRCR, ScoreRCR>>(ScoreRCR.class);

		for (final ScoreRCR score : SCORES_TO_COMPARE) {
			mapComparators.put(
				score,
				ScoreComparatorRCRFactory.getScoreAccessorRCR(score)
			);
			mapGetters.put(
				score,
				ScoreGetterRCRFactory.getScoreGetterRCR(score)
			);
			mapSetters.put(
				score,
				ScoreSetterRCRFactory.getScoreSetterRCR(score)
			);
		}
	}

	@Override
	protected void updateHighestLowestScore(
		final Tournament<TournamentScoreRCR, ScoreRCR> tournament,
		final int session
	) {
		scoreHighest = new TournamentScoreRCR(
			null,
			session,
			0,
			0,
			null,
			0,
			0,
			Integer.MIN_VALUE,
			Integer.MIN_VALUE,
			Integer.MIN_VALUE,
			Integer.MIN_VALUE,
			false
		);
		scoreLowest = new TournamentScoreRCR(
			null,
			session,
			0,
			0,
			null,
			0,
			0,
			Integer.MAX_VALUE,
			Integer.MAX_VALUE,
			Integer.MAX_VALUE,
			Integer.MAX_VALUE,
			false
		);

		for (final TournamentScoreRCR table : tournament.getTables()) {
			if (table.getSessionId() == session) {
				for (final ScoreRCR score : SCORES_TO_COMPARE) {
					if (mapComparators.get(score).compare(
						table,
						scoreHighest
					) > 0) {
						mapSetters.get(score).set(
							scoreHighest,
							mapGetters.get(score).get(table)
						);
					}
					if (mapComparators.get(score).compare(
						table,
						scoreLowest
					) < 0) {
						mapSetters.get(score).set(
							scoreLowest,
							mapGetters.get(score).get(table)
						);
					}
				}
			}
		}
	}

	@Override
	protected UIDisplayTableScoreChart<TournamentScoreRCR, ScoreRCR> createDisplay() {
		return new UIDisplayTableScoreChartRCR(
			scoreHighest,
			scoreLowest,
			translator
		);
	}
}
