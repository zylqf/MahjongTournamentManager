/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.icon;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import fr.bri.mj.data.player.CountryCode;

public class IconLoaderCountry {

	private static final String ICON_FOLDER_URL = "fr/bri/mj/country/";
	private static final String ICON_FILE_EMPTY = "999";
	private static final String ICON_FILE_EXT = ".png";

	private static final Map<CountryCode, Map<Integer, Image>> ICON_BUFFER = new HashMap<CountryCode, Map<Integer, Image>>();
	private static final DecimalFormat decimalFormat = new DecimalFormat("000");

	public static Image getIconImageCountry(
		final CountryCode country,
		final int size
	) {
		Map<Integer, Image> mapIconCountry = null;
		if (ICON_BUFFER.containsKey(country)) {
			mapIconCountry = ICON_BUFFER.get(country);
		} else {
			mapIconCountry = new HashMap<Integer, Image>();
			ICON_BUFFER.put(
				country,
				mapIconCountry
			);
		}

		if (mapIconCountry.containsKey(size)) {
			return mapIconCountry.get(size);
		} else {
			URL iconFile = null;
			if (country != null && !country.isDummy()) {
				iconFile = ClassLoader.getSystemResource(ICON_FOLDER_URL + decimalFormat.format(country.getCountryCodeNumeric()) + ICON_FILE_EXT);
				if (iconFile == null) {
					iconFile = ClassLoader.getSystemResource(ICON_FOLDER_URL + ICON_FILE_EMPTY + ICON_FILE_EXT);
				}
			} else {
				iconFile = ClassLoader.getSystemResource(ICON_FOLDER_URL + ICON_FILE_EMPTY + ICON_FILE_EXT);
			}

			Image iconImage = null;
			if (iconFile != null) {
				try {
					iconImage = ImageIO.read(iconFile);
					iconImage = iconImage.getScaledInstance(
						size,
						size,
						Image.SCALE_SMOOTH
					);
				} catch (final IOException e) {
					iconImage = new BufferedImage(
						0,
						0,
						BufferedImage.TYPE_BYTE_BINARY
					);
				}
			} else {
				iconImage = new BufferedImage(
					0,
					0,
					BufferedImage.TYPE_BYTE_BINARY
				);
			}
			mapIconCountry.put(
				size,
				iconImage
			);
			return iconImage;
		}
	}
}
