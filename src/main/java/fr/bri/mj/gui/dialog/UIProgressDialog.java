package fr.bri.mj.gui.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;

import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;

public class UIProgressDialog extends JDialog {

	private static final long serialVersionUID = 7861660830903455374L;

	private final JFrame parent;
	private final int maxValue;

	private final JLabel labelText;
	private final JProgressBar progressBar;

	private ComponentShownListener componentShownListener;

	public UIProgressDialog(
		final JFrame parent,
		final int maxValue,
		final UITextTranslator translator
	) {
		super(
			parent,
			true
		);
		this.parent = parent;
		this.maxValue = maxValue;

		final Container pane = getContentPane();
		pane.setLayout(new BorderLayout());

		final JPanel panelSupport = new JPanel(
			new BorderLayout(
				8,
				8
			)
		);
		pane.add(
			panelSupport,
			BorderLayout.CENTER
		);

		panelSupport.setOpaque(true);
		panelSupport.setBackground(Color.WHITE);
		panelSupport.setBorder(
			BorderFactory.createCompoundBorder(
				BorderFactory.createLineBorder(Color.BLACK),
				BorderFactory.createEmptyBorder(
					16,
					64,
					16,
					64
				)
			)
		);

		labelText = new JLabel(
			translator.translate(UIText.GUI_PROGRESS_WAITING),
			SwingConstants.CENTER
		);
		labelText.setOpaque(true);
		labelText.setBackground(Color.WHITE);
		labelText.setBorder(
			BorderFactory.createEmptyBorder(
				0,
				64,
				0,
				64
			)

		);
		panelSupport.add(
			labelText,
			BorderLayout.CENTER
		);

		progressBar = new JProgressBar(
			SwingConstants.HORIZONTAL,
			0,
			maxValue
		);
		progressBar.setOpaque(true);
		panelSupport.add(
			progressBar,
			BorderLayout.SOUTH
		);

		addComponentListener(new DialogShownListener());
		setUndecorated(true);
		pack();
	}

	public void setComponentShownListener(final ComponentShownListener componentShownListener) {
		this.componentShownListener = componentShownListener;
	}

	public void showProgressDialog() {
		final Dimension size = getSize();
		if (parent != null && (parent.getExtendedState() & Frame.ICONIFIED) == 0) {
			final Point parentLocation = parent.getLocationOnScreen();
			final Dimension parentSize = parent.getSize();
			final int x = parentLocation.x + (parentSize.width - size.width) / 2;
			final int y = parentLocation.y + (parentSize.height - size.height) / 2;
			setLocation(
				x,
				y
			);
		} else {
			final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			final int x = (screenSize.width - size.width) / 2;
			final int y = (screenSize.height - size.height) / 2;
			setLocation(
				x,
				y
			);
		}
		setVisible(true);
		dispose();
	}

	public void setProgress(final int progress) {
		if (progress >= 0 && progress <= maxValue) {
			progressBar.setValue(progress);
			repaint();
		}
	}

	private class DialogShownListener extends ComponentAdapter {

		@Override
		public void componentShown(final ComponentEvent e) {
			if (componentShownListener != null) {
				componentShownListener.componentShown(e);
			}
		}
	}
}
