/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.gui.dialog;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import fr.bri.mj.gui.icon.IconLoaderMessageType;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;

public class UIMessageDialog extends JDialog {

	private static final long serialVersionUID = -8406457619417322084L;

	public enum MessageDialogMessageType {
		INFORMATION("information"),
		WARNING("warning"),
		ERROR("error"),
		QUESTION("question");

		private final String name;

		private MessageDialogMessageType(final String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	public enum MessageDialogOptionsType {
		OK(
			new MessageDialogOption[] {
				MessageDialogOption.OK
			}
		),
		OK_CANCEL(
			new MessageDialogOption[] {
				MessageDialogOption.OK,
				MessageDialogOption.CANCEL
			}
		),
		YES_NO(
			new MessageDialogOption[] {
				MessageDialogOption.YES,
				MessageDialogOption.NO
			}
		),
		YES_NO_CANCEL(
			new MessageDialogOption[] {
				MessageDialogOption.YES,
				MessageDialogOption.NO,
				MessageDialogOption.CANCEL
			}
		);

		private final MessageDialogOption[] options;

		private MessageDialogOptionsType(final MessageDialogOption[] options) {
			this.options = options;
		}

		private MessageDialogOption[] getOptions() {
			return options;
		}
	}

	public enum MessageDialogOption {
		OK(UIText.GUI_DIALOG_BUTTON_OK),
		YES(UIText.GUI_DIALOG_BUTTON_YES),
		NO(UIText.GUI_DIALOG_BUTTON_NO),
		CANCEL(UIText.GUI_DIALOG_BUTTON_CANCEL),
		CLOSED(null);

		private final UIText uiText;

		private MessageDialogOption(final UIText uiText) {
			this.uiText = uiText;
		}

		private UIText getUIText() {
			return uiText;
		}
	}

	private final JFrame parent;
	private JButton buttonDefaultOption;
	private MessageDialogOption selectedOption;

	public UIMessageDialog(
		final JFrame parent,
		final String message,
		final String title,
		final MessageDialogMessageType messageType,
		final MessageDialogOptionsType optionsType,
		final MessageDialogOption defaultOption,
		final UITextTranslator translator
	) {
		super(
			parent,
			title,
			true
		);
		this.parent = parent;

		final int nbOptions = optionsType.getOptions().length;

		final Container pane = getContentPane();
		pane.setLayout(new GridBagLayout());
		final GridBagConstraints constraints = new GridBagConstraints();
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.fill = GridBagConstraints.NONE;
		constraints.ipadx = 0;
		constraints.ipady = 0;

		{
			final Image iconImage = IconLoaderMessageType.getIconImage(messageType);
			final JLabel labelIcon = new JLabel(
				new ImageIcon(iconImage),
				SwingConstants.CENTER
			);

			constraints.gridx = 0;
			constraints.gridy = 0;
			constraints.gridwidth = 1;
			constraints.gridheight = 1;
			constraints.insets = new Insets(
				16,
				16,
				0,
				0
			);

			pane.add(
				labelIcon,
				constraints
			);
		}

		{
			final String htmlMessage = "<html><body style='text-align: center'>"
				+ String.join(
					"<br/>",
					message.split("\\|")
				)
				+ "</html>";
			final JLabel labelMessage = new JLabel(
				htmlMessage,
				SwingConstants.CENTER
			);

			constraints.gridx = 1;
			constraints.gridy = 0;
			constraints.gridwidth = nbOptions;
			constraints.gridheight = 1;
			constraints.insets = new Insets(
				16,
				16,
				0,
				16
			);

			pane.add(
				labelMessage,
				constraints
			);
		}

		{
			final Dimension buttonSize = new Dimension(
				80,
				24
			);

			final MessageDialogOption options[] = optionsType.getOptions();
			int defaultOptionIndex = 0;
			for (int optionIndex = 0; optionIndex < options.length; optionIndex++) {
				if (options[optionIndex] == defaultOption) {
					defaultOptionIndex = optionIndex;
					break;
				}
			}
			for (int optionIndex = 0; optionIndex < options.length; optionIndex++) {
				final MessageDialogOption option = options[optionIndex];
				final JButton button = new JButton(translator.translate(option.getUIText()));
				button.setPreferredSize(buttonSize);

				constraints.gridx = optionIndex + 1;
				constraints.gridy = 1;
				constraints.gridwidth = 1;
				constraints.gridheight = 1;
				constraints.insets = new Insets(
					16,
					16,
					16,
					16
				);

				pane.add(
					button,
					constraints
				);

				button.addActionListener((final ActionEvent e) -> {
					selectedOption = option;
					setVisible(false);
				});

				if (optionIndex == defaultOptionIndex) {
					buttonDefaultOption = button;
				}
			}
		}

		pack();
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setResizable(false);
	}

	public MessageDialogOption showOptionDialog() {
		selectedOption = MessageDialogOption.CLOSED;
		final Dimension size = getSize();
		if (parent != null && (parent.getExtendedState() & Frame.ICONIFIED) == 0) {
			final Point parentLocation = parent.getLocationOnScreen();
			final Dimension parentSize = parent.getSize();
			final int x = parentLocation.x + (parentSize.width - size.width) / 2;
			final int y = parentLocation.y + (parentSize.height - size.height) / 2;
			setLocation(
				x,
				y
			);
		} else {
			final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			final int x = (screenSize.width - size.width) / 2;
			final int y = (screenSize.height - size.height) / 2;
			setLocation(
				x,
				y
			);
		}
		SwingUtilities.invokeLater(() -> {
			buttonDefaultOption.requestFocusInWindow();
		});
		setVisible(true);
		dispose();
		return selectedOption;
	}
}
