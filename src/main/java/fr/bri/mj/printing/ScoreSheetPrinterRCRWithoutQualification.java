/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.printing;

import java.awt.Color;
import java.io.File;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType0Font;

import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.data.tournament.rcr.ScoreRCR;
import fr.bri.mj.data.tournament.rcr.TournamentScoreRCR;
import fr.bri.mj.gui.language.UITextTranslator;

public class ScoreSheetPrinterRCRWithoutQualification {

	private static final Color FONT_COLOR = Color.BLACK;

	private static final PaperSizeType FORM_SIZE = PaperSizeType.A6;
	private static final float DOT_PER_MM = 72.0f / 25.4f;
	private static final float MARGIN = 10.0f * DOT_PER_MM;
	private static final float FONT_SIZE = 11.5f;

	public boolean saveScoreSheet(
		final Tournament<TournamentScoreRCR, ScoreRCR> tournament,
		final PaperSizeType paperSize,
		final PaperSizeType formSize,
		final boolean drawLineBetweenForms,
		final ScoreFormPrinterRCR scoreFormPrinter,
		final File pdfFile,
		final UITextTranslator translator
	) {
		final int nbSessions = tournament.getNbSessions();
		final int nbExistingSessions = tournament.getNbExistingSessions();
		final int nbTables = tournament.getNbTables();

		final TournamentPlayer[][][] plan = new TournamentPlayer[nbExistingSessions][nbTables][4];
		final SortedSet<TournamentScoreRCR> plans = new TreeSet<TournamentScoreRCR>(tournament.getTables());
		for (final TournamentScoreRCR table : plans) {
			plan[table.getSessionId() - 1][table.getTableId() - 1][table.getTablePlayerId() - 1] = table.getTournamentPlayer();
		}

		final int sizeDiff = formSize.getSize() - paperSize.getSize();
		final float pageWidth = (sizeDiff % 2 == 0
			? PaperSize.getHeight(paperSize)
			: PaperSize.getWidth(paperSize)) * DOT_PER_MM;
		final float pageHeight = (sizeDiff % 2 == 0
			? PaperSize.getWidth(paperSize)
			: PaperSize.getHeight(paperSize)) * DOT_PER_MM;

		final float fontSize = (float) (FONT_SIZE * Math.pow(
			2.0,
			(FORM_SIZE.getSize() - formSize.getSize()) * 0.5
		));
		final int nbFormsPerPage = 1 << sizeDiff;
		final int nbFormsPerRow = 1 << sizeDiff / 2;
		final int nbFormsPerColumn = 1 << (sizeDiff + 1) / 2;

		final float formAreaWidth = pageWidth / nbFormsPerRow;
		final float formAreaHeight = pageHeight / nbFormsPerColumn;
		final float formWidth = formAreaWidth - MARGIN * 2.0f;
		final float formHeight = formAreaHeight - MARGIN * 2.0f;

		final int nbForms = nbSessions * nbTables;
		final int nbExistingForms = nbExistingSessions * nbTables;
		final int nbPages = (nbForms + nbFormsPerPage - 1) / nbFormsPerPage;
		final int nbFormsToDraw = nbPages * nbFormsPerPage;

		try {
			final PDDocument document = new PDDocument();
			final PDType0Font font = PDType0Font.load(
				document,
				ClassLoader.getSystemResourceAsStream("fr/bri/mj/font/OxygenMono-Regular.ttf")
			);
			PDPage page = null;
			PDPageContentStream stream = null;

			for (int formTotalIndex = 0; formTotalIndex < nbFormsToDraw; formTotalIndex++) {
				final int pageIndex = formTotalIndex / nbFormsPerPage;
				final int formOnPageIndex = formTotalIndex % nbFormsPerPage;
				final int formOnPageRow = formOnPageIndex % nbFormsPerRow;
				final int formOnPageColumn = formOnPageIndex / nbFormsPerRow;

				final int formIndex = formOnPageIndex * nbPages + pageIndex;
				final int sessionIndex = formIndex / nbTables;
				final int tableIndex = formIndex % nbTables;
				final TournamentPlayer[] tablePlan = formIndex < nbExistingForms
					? plan[sessionIndex][tableIndex]
					: null;

				// Create new page
				if (formOnPageIndex == 0) {
					if (stream != null) {
						stream.close();
					}
					page = new PDPage(
						new PDRectangle(
							pageWidth,
							pageHeight
						)
					);
					document.addPage(page);
					stream = new PDPageContentStream(
						document,
						page,
						AppendMode.OVERWRITE,
						true
					);
					stream.setFont(
						font,
						fontSize
					);

					if (drawLineBetweenForms) {
						for (int columnIndex = 1; columnIndex < nbFormsPerColumn; columnIndex++) {
							stream.moveTo(
								MARGIN,
								formAreaHeight * columnIndex
							);
							stream.lineTo(
								pageWidth - MARGIN,
								formAreaHeight * columnIndex
							);
							stream.setNonStrokingColor(FONT_COLOR);
							stream.stroke();
						}

						for (int rowIndex = 1; rowIndex < nbFormsPerRow; rowIndex++) {
							stream.moveTo(
								formAreaWidth * rowIndex,
								MARGIN
							);
							stream.lineTo(
								formAreaWidth * rowIndex,
								pageHeight - MARGIN
							);
							stream.setNonStrokingColor(FONT_COLOR);
							stream.stroke();
						}
					}
				}

				final float formTopX = formOnPageRow * formAreaWidth + MARGIN;
				final float formTopY = pageHeight - formOnPageColumn * formAreaHeight - MARGIN;
				scoreFormPrinter.drawForm(
					translator,
					stream,
					font,
					fontSize,
					formTopX,
					formTopY,
					formWidth,
					formHeight,
					tableIndex + 1,
					nbTables,
					sessionIndex + 1,
					nbSessions,
					tablePlan
				);
			}
			if (stream != null) {
				stream.close();
			}
			document.save(pdfFile);
			document.close();
			return true;
		} catch (final Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
