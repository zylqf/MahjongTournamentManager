/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.printing;

public class PaperSize {

	private static final double A0_HEIGHT = 1000.0 * Math.pow(
		2.0,
		0.25
	);

	private static final double B0_HEIGHT = 1000.0 * Math.pow(
		2.0,
		0.5
	);

	public static float getWidth(final PaperSizeType type) {
		switch (type.getSeries()) {
			case A_SERIES:
				return calcualte(
					A0_HEIGHT,
					type.getSize() + 1
				);
			case B_SERIES:
				return calcualte(
					B0_HEIGHT,
					type.getSize() + 1
				);
			default:
				return 0.0f;
		}
	}

	public static float getHeight(final PaperSizeType type) {
		switch (type.getSeries()) {
			case A_SERIES:
				return calcualte(
					A0_HEIGHT,
					type.getSize()
				);
			case B_SERIES:
				return calcualte(
					B0_HEIGHT,
					type.getSize()
				);
			default:
				return 0.0f;
		}
	}

	private static float calcualte(
		final double size0,
		final int size
	) {
		return (float) (size0 * Math.pow(
			2.0,
			-0.5 * size
		));
	}
}
