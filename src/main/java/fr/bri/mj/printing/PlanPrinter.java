/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.printing;

import java.awt.Color;
import java.io.File;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType0Font;

import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.data.tournament.TournamentScore;
import fr.bri.mj.gui.language.UIText;
import fr.bri.mj.gui.language.UITextTranslator;

public class PlanPrinter {

	private static final int COLUMN_WIDTH_WEIGHT_TOTAL = 12;
	private static final int[] COLUMN_WIDTH_WEIGHT = {
		2,
		2,
		2,
		3,
		3
	};
	private static final UIText TEXT_TITLE[] = {
		UIText.PDF_PLAN_TITLE_SESSION,
		UIText.PDF_PLAN_TITLE_TABLE,
		UIText.PDF_PLAN_TITLE_TIME,
		UIText.PDF_PLAN_TITLE_SCORE,
		UIText.PDF_PLAN_TITLE_TOTAL
	};

	private static final Color FONT_COLOR = Color.BLACK;

	private static final int NB_LINES_PAGE = 22;
	private static final int NB_LINES_MARGE = 2;

	private static final int NB_LINES_SINGLE_NORNAL_MODE = NB_LINES_PAGE - NB_LINES_MARGE * 2;
	private static final int MAX_NB_SESSIONS_SINGLE_NORMAL_MODE = NB_LINES_SINGLE_NORNAL_MODE - 2;
	private static final int NB_LINES_HALF_MODE = NB_LINES_PAGE / 2 - NB_LINES_MARGE * 2;
	private static final int MAX_NB_SESSIONS_HALF_MODE = NB_LINES_HALF_MODE - 2;

	private static final PaperSizeType FORM_SIZE = PaperSizeType.A6;
	private static final float DOT_PER_MM = 72.0f / 25.4f;
	private static final float MARGIN = PaperSize.getWidth(FORM_SIZE) * DOT_PER_MM * NB_LINES_MARGE / NB_LINES_PAGE;
	private static final float CELL_INNER_MARGIN = 2.0f;

	private static final float FONT_SIZE = 10.0f;

	private static final DecimalFormat SESSION_ID_FORMAT = new DecimalFormat("00");
	private static final DecimalFormat TABLE_ID_FORMAT = new DecimalFormat("00");

	public static boolean savePlan(
		final Tournament<?, ?> tournament,
		final PaperSizeType paperSize,
		final PaperSizeType formSize,
		final boolean drawLineBetweenForms,
		final File pdfFile,
		final UITextTranslator translator
	) {
		final int nbSessions = Math.min(
			tournament.getNbSessions(),
			MAX_NB_SESSIONS_SINGLE_NORMAL_MODE
		);
		final boolean halfMode = nbSessions <= MAX_NB_SESSIONS_HALF_MODE;

		final DateFormat dateFormat = new SimpleDateFormat("HH:mm");
		final DecimalFormat numberFormat = new DecimalFormat("#0");
		final Map<Integer, String> planningText = new HashMap<Integer, String>();
		final Map<Integer, Date> plannings = tournament.getPlannings();
		for (final Integer sessionId : plannings.keySet()) {
			planningText.put(
				sessionId,
				dateFormat.format(plannings.get(sessionId))
			);
		}

		final Set<TournamentScore<?, ?>> plan = new HashSet<TournamentScore<?, ?>>(tournament.getTables());
		final Map<TournamentPlayer, Map<Integer, Integer>> playerTablePlans = new HashMap<TournamentPlayer, Map<Integer, Integer>>();
		for (final TournamentScore<?, ?> table : plan) {
			final TournamentPlayer tournamentPlayer = table.getTournamentPlayer();
			if (!playerTablePlans.containsKey(tournamentPlayer)) {
				playerTablePlans.put(
					tournamentPlayer,
					new HashMap<Integer, Integer>()
				);
			}
			playerTablePlans.get(tournamentPlayer).put(
				table.getSessionId(),
				table.getTableId()
			);
		}

		final List<TournamentPlayer> playerList = new ArrayList<TournamentPlayer>(playerTablePlans.keySet());
		Collections.shuffle(playerList);

		final int sizeDiff = formSize.getSize() - paperSize.getSize();
		final float pageWidth = (sizeDiff % 2 == 0
			? PaperSize.getHeight(paperSize)
			: PaperSize.getWidth(paperSize)) * DOT_PER_MM;
		final float pageHeight = (sizeDiff % 2 == 0
			? PaperSize.getWidth(paperSize)
			: PaperSize.getHeight(paperSize)) * DOT_PER_MM;

		final float fontSize = (float) (FONT_SIZE * Math.pow(
			2.0,
			(FORM_SIZE.getSize() - formSize.getSize()) * 0.5
		));

		final int nbFormsPerPage;
		final int nbFormsPerRow;
		final int nbFormsPerColumn;
		if (halfMode) {
			nbFormsPerPage = 1 << sizeDiff + 1;
			nbFormsPerRow = 1 << sizeDiff / 2;
			nbFormsPerColumn = 1 << (sizeDiff + 1) / 2 + 1;
		} else {
			nbFormsPerPage = 1 << sizeDiff;
			nbFormsPerRow = 1 << sizeDiff / 2;
			nbFormsPerColumn = 1 << (sizeDiff + 1) / 2;
		}

		final float formAreaWidth = pageWidth / nbFormsPerRow;
		final float formAreaHeight = pageHeight / nbFormsPerColumn;
		final float formWidth = formAreaWidth - MARGIN * 2;
		final float formHeight = formAreaHeight - MARGIN * 2;

		final float cellWidthPart = formWidth / COLUMN_WIDTH_WEIGHT_TOTAL;
		final float cellWidth[] = new float[COLUMN_WIDTH_WEIGHT.length + 1];
		for (int index = 0; index < COLUMN_WIDTH_WEIGHT.length; index++) {
			cellWidth[index] = cellWidthPart * COLUMN_WIDTH_WEIGHT[index];
		}

		final float cellHeight;
		if (halfMode) {
			cellHeight = formHeight / NB_LINES_HALF_MODE;
		} else {
			cellHeight = formHeight / NB_LINES_SINGLE_NORNAL_MODE;
		}

		final int nbForms = playerList.size();
		final int nbLines = nbSessions + 2;

		try {
			final PDDocument document = new PDDocument();
			final PDType0Font font = PDType0Font.load(
				document,
				ClassLoader.getSystemResourceAsStream("fr/bri/mj/font/OxygenMono-Regular.ttf")
			);
			final float fontSizeQuater = font.getFontDescriptor().getFontBoundingBox().getHeight() * FONT_SIZE / 4000f;

			PDPage page = null;
			PDPageContentStream stream = null;

			for (int formIndex = 0; formIndex < nbForms; formIndex++) {
				final int formOnPageIndex = formIndex % nbFormsPerPage;
				final int formOnPageRow = formOnPageIndex % nbFormsPerRow;
				final int formOnPageColumn = formOnPageIndex / nbFormsPerRow;

				if (formOnPageIndex == 0) {
					if (stream != null) {
						stream.close();
					}
					page = new PDPage(
						new PDRectangle(
							pageWidth,
							pageHeight
						)
					);
					document.addPage(page);
					stream = new PDPageContentStream(
						document,
						page,
						AppendMode.OVERWRITE,
						true
					);
					stream.setFont(
						font,
						fontSize
					);

					if (drawLineBetweenForms) {
						for (int columnIndex = 1; columnIndex < nbFormsPerColumn; columnIndex++) {
							stream.moveTo(
								MARGIN,
								formAreaHeight * columnIndex
							);
							stream.lineTo(
								pageWidth - MARGIN,
								formAreaHeight * columnIndex
							);
							stream.setNonStrokingColor(FONT_COLOR);
							stream.stroke();
						}

						for (int rowIndex = 1; rowIndex < nbFormsPerRow; rowIndex++) {
							stream.moveTo(
								formAreaWidth * rowIndex,
								MARGIN
							);
							stream.lineTo(
								formAreaWidth * rowIndex,
								pageHeight - MARGIN
							);
							stream.setNonStrokingColor(FONT_COLOR);
							stream.stroke();
						}
					}
				}

				final float formTopLeftX = formOnPageRow * formAreaWidth + MARGIN;
				final float formTopLeftY = pageHeight - formOnPageColumn * formAreaHeight - MARGIN;

				// Draw row lines
				for (int rowIndex = 1; rowIndex <= nbLines; rowIndex++) {
					final float y = formTopLeftY - rowIndex * cellHeight;
					stream.moveTo(
						formTopLeftX,
						y
					);
					stream.lineTo(
						formTopLeftX + formWidth,
						y
					);
					stream.stroke();
				}

				// Draw column lines
				final float columnTopY = formTopLeftY - cellHeight;
				final float columnBottomY = formTopLeftY - cellHeight * nbLines;
				float columnX = formTopLeftX;
				for (int columnIndex = 0; columnIndex <= 5; columnIndex++) {
					stream.moveTo(
						columnX,
						columnTopY
					);
					stream.lineTo(
						columnX,
						columnBottomY
					);
					stream.stroke();
					columnX = columnX + cellWidth[columnIndex];
				}

				final TournamentPlayer tournamentPlayer = playerList.get(formIndex);
				// Name
				{
					final float textBaseY = formTopLeftY - cellHeight / 2 - fontSizeQuater;
					final float textBaseX = formTopLeftX + CELL_INNER_MARGIN;
					stream.beginText();
					stream.newLineAtOffset(
						textBaseX,
						textBaseY
					);
					final StringBuilder name = new StringBuilder(numberFormat.format(tournamentPlayer.getTournamentPlayerId()));
					name.append(" ");
					name.append(tournamentPlayer.getPlayerName(translator.translate(UIText.PDF_PLAN_TITLE_PLAYER_EMPTY)));
					stream.showText(name.toString());
					stream.endText();
				}

				// Titles
				{
					final float textBaseY = formTopLeftY - cellHeight - cellHeight / 2 - fontSizeQuater;
					float cellX = formTopLeftX;
					for (int titleIndex = 0; titleIndex < TEXT_TITLE.length; titleIndex++) {
						final String title = translator.translate(TEXT_TITLE[titleIndex]);
						final float textBaseX = cellX + (cellWidth[titleIndex] - font.getStringWidth(title) * FONT_SIZE / 1000f) / 2f;
						stream.beginText();
						stream.newLineAtOffset(
							textBaseX,
							textBaseY
						);
						stream.showText(title);
						stream.endText();
						cellX = cellX + cellWidth[titleIndex];
					}
				}

				final Map<Integer, Integer> playerTablePlan = playerTablePlans.get(tournamentPlayer);
				{
					for (int sessionIndex = 1; sessionIndex <= nbSessions; sessionIndex++) {
						final float textBaseY = formTopLeftY - cellHeight * (sessionIndex + 1) - cellHeight / 2 - fontSizeQuater;
						// Session
						{
							final String textSession = SESSION_ID_FORMAT.format(sessionIndex);
							final float textBaseX = formTopLeftX + (cellWidth[0] - font.getStringWidth(textSession) * FONT_SIZE / 1000f) / 2f;
							stream.beginText();
							stream.newLineAtOffset(
								textBaseX,
								textBaseY
							);
							stream.showText(textSession);
							stream.endText();
						}

						// Table
						{
							final String textTable = playerTablePlan.containsKey(sessionIndex)
								? TABLE_ID_FORMAT.format(playerTablePlan.get(sessionIndex))
								: "-";
							final float textBaseX = formTopLeftX + cellWidth[0] + (cellWidth[1] - font.getStringWidth(textTable) * FONT_SIZE / 1000f) / 2f;
							stream.beginText();
							stream.newLineAtOffset(
								textBaseX,
								textBaseY
							);
							stream.showText(textTable);
							stream.endText();
						}

						// Time
						{
							final float textBaseX = formTopLeftX + cellWidth[0] + cellWidth[1] + (cellWidth[2] - font.getStringWidth(planningText.get(sessionIndex)) * FONT_SIZE / 1000f) / 2;
							stream.beginText();
							stream.newLineAtOffset(
								textBaseX,
								textBaseY
							);
							stream.showText(planningText.get(sessionIndex));
							stream.endText();
						}
					}
				}
			}
			stream.close();
			document.save(pdfFile);
			document.close();
			return true;
		} catch (final Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
