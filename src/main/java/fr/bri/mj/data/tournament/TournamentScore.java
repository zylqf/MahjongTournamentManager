/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.tournament;

public abstract class TournamentScore<ScoreClassType extends TournamentScore<ScoreClassType, ScoreEnumType>, ScoreEnumType extends Enum<ScoreEnumType>> implements Comparable<TournamentScore<ScoreClassType, ScoreEnumType>> {

	private final Tournament<ScoreClassType, ScoreEnumType> tournament;
	private final int sessionId;
	private final int tableId;
	private final int tablePlayerId;
	private final TournamentPlayer tournamentPlayer;
	private boolean scoreRecorded;

	private int substituted;
	private int ranking;

	protected TournamentScore(
		final Tournament<ScoreClassType, ScoreEnumType> tournament,
		final int sessionId,
		final int tableId,
		final int tablePlayerId,
		final TournamentPlayer tournamentPlayer,
		final boolean scoreRecorded,
		final int substituted,
		final int ranking
	) {
		this.tournament = tournament;
		this.sessionId = sessionId;
		this.tableId = tableId;
		this.tablePlayerId = tablePlayerId;
		this.tournamentPlayer = tournamentPlayer;
		this.scoreRecorded = scoreRecorded;
		this.substituted = substituted;
		this.ranking = ranking;
	}

	public Tournament<ScoreClassType, ScoreEnumType> getTournament() {
		return tournament;
	}

	public int getSessionId() {
		return sessionId;
	}

	public int getTableId() {
		return tableId;
	}

	public int getTablePlayerId() {
		return tablePlayerId;
	}

	public TournamentPlayer getTournamentPlayer() {
		return tournamentPlayer;
	}

	public boolean isScoreRecorded() {
		return scoreRecorded;
	}

	public void setScoreRecorded(final boolean scoreRecorded) {
		this.scoreRecorded = scoreRecorded;
	}

	public int getSubstituted() {
		return substituted;
	}

	public void setSubstituted(final int substituted) {
		this.substituted = substituted;
	}

	public int getRanking() {
		return ranking;
	}

	public void setRanking(final int ranking) {
		this.ranking = ranking;
	}

	@Override
	public int compareTo(final TournamentScore<ScoreClassType, ScoreEnumType> o) {
		int result = Integer.compare(
			tournament.getId(),
			o.tournament.getId()
		);
		if (result == 0) {
			result = Integer.compare(
				sessionId,
				o.sessionId
			);
			if (result == 0) {
				result = Integer.compare(
					tableId,
					o.tableId
				);
				if (result == 0) {
					result = Integer.compare(
						tablePlayerId,
						o.tablePlayerId
					);
				}
			}
		}
		return result;
	}
}
