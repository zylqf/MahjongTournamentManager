/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.tournament;

import java.util.Date;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

public abstract class Tournament<ScoreClassType extends TournamentScore<ScoreClassType, ScoreEnumType>, ScoreEnumType extends Enum<ScoreEnumType>> implements Comparable<Tournament<ScoreClassType, ScoreEnumType>> {

	private final int id;
	private final int nbSessions;
	private final int nbRoundRobinSessions;
	private final int nbTables;

	private String name;
	private String leagueName;
	private final boolean withQualification;
	private boolean archived;
	private final SortedMap<Integer, Date> plannings;
	private final SortedSet<TournamentPlayer> tournamentPlayers;
	private final SortedSet<ScoreClassType> tables;

	public Tournament(
		final int id,
		final String name,
		final String leagueName,
		final int nbSessions,
		final int nbRoundRobinSessions,
		final int nbTables,
		final boolean withQualification,
		final boolean archived
	) {
		this.id = id;
		this.name = name;
		this.leagueName = leagueName;
		this.nbSessions = nbSessions;
		this.nbRoundRobinSessions = nbRoundRobinSessions;
		this.nbTables = nbTables;
		this.withQualification = withQualification;
		this.archived = archived;
		plannings = new TreeMap<Integer, Date>();
		tournamentPlayers = new TreeSet<TournamentPlayer>();
		tables = new TreeSet<ScoreClassType>();
	}

	public int getId() {
		return id;
	}

	public int getNbSessions() {
		return nbSessions;
	}

	public int getNbTables() {
		return nbTables;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getLeagueName() {
		return leagueName;
	}

	public void setLeagueName(final String leagueName) {
		this.leagueName = leagueName;
	}

	public boolean isWithQualification() {
		return withQualification;
	}

	public int getNbRoundRobinSessions() {
		return nbRoundRobinSessions;
	}

	public boolean isArchived() {
		return archived;
	}

	public void setArchived(final boolean archived) {
		this.archived = archived;
	}

	public int getNbExistingSessions() {
		int nbExistingSessions = 1;
		for (final ScoreClassType score : tables) {
			nbExistingSessions = Math.max(
				nbExistingSessions,
				score.getSessionId()
			);
		}
		return nbExistingSessions;
	}

	public SortedMap<Integer, Date> getPlannings() {
		return plannings;
	}

	public SortedSet<TournamentPlayer> getTournamentPlayers() {
		return tournamentPlayers;
	}

	public SortedSet<ScoreClassType> getTables() {
		return tables;
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public int hashCode() {
		return id;
	}

	@Override
	public boolean equals(final Object obj) {
		return obj != null && obj instanceof Tournament && ((Tournament<?, ?>) obj).id == id;
	}

	@Override
	public int compareTo(final Tournament<ScoreClassType, ScoreEnumType> o) {
		return -Integer.compare(
			id,
			o.id
		);
	}
}
