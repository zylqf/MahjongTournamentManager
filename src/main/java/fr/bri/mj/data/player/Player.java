/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.player;

public class Player implements Comparable<Player> {

	private final int id;

	private String playerFirstName;
	private String playerLastName;
	private CountryCode playerNationality;
	private String club;
	private String license;

	public Player(final int id) {
		this.id = id;
	}

	public Player(
		final int id,
		final String playerFirstName,
		final String playerLastName,
		final CountryCode playerNationality,
		final String club,
		final String license
	) {
		this.id = id;
		this.playerFirstName = playerFirstName;
		this.playerLastName = playerLastName;
		this.playerNationality = playerNationality;
		this.club = club;
		this.license = license;
	}

	public int getId() {
		return id;
	}

	public String getPlayerFirstName() {
		return playerFirstName;
	}

	public void setPlayerFirstName(final String playerFirstName) {
		if (playerFirstName != null) {
			this.playerFirstName = playerFirstName;
		}
	}

	public String getPlayerLastName() {
		return playerLastName;
	}

	public void setPlayerLastName(final String playerLastName) {
		if (playerLastName != null) {
			this.playerLastName = playerLastName;
		}
	}

	public CountryCode getPlayerNationality() {
		return playerNationality;
	}

	public void setPlayerNationality(final CountryCode playerNationality) {
		if (playerNationality != null) {
			this.playerNationality = playerNationality;
		}
	}

	public String getClub() {
		return club;
	}

	public void setClub(final String club) {
		this.club = club;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(final String license) {
		this.license = license;
	}

	@Override
	public int hashCode() {
		return id;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj != null && obj instanceof Player) {
			final Player another = (Player) obj;
			return id == another.id;
		} else {
			return false;
		}
	}

	@Override
	public int compareTo(final Player o) {
		return Integer.compare(
			id,
			o.id
		);
	}
}
