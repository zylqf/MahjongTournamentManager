/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.score.aggregated.ranking.calculator.multi;

import java.util.EnumMap;

import fr.bri.mj.data.score.aggregated.ranking.comparator.RankingScoreComparatorFactory;
import fr.bri.mj.data.score.aggregated.ranking.comparator.RankingScoreOrder;
import fr.bri.mj.data.score.raw.filter.ScoreFilterRCRFactory;
import fr.bri.mj.data.score.raw.getter.ScoreGetterRCRFactory;
import fr.bri.mj.data.tournament.rcr.ScoreRCR;
import fr.bri.mj.data.tournament.rcr.TournamentScoreRCR;

public class RankingCalculatorMultiRCRFactory implements RankingCalculatorMultiFactory<TournamentScoreRCR, ScoreRCR> {

	private static RankingCalculatorMultiRCRFactory instance = new RankingCalculatorMultiRCRFactory();

	public static RankingCalculatorMultiRCRFactory getInstance() {
		return instance;
	}

	private final EnumMap<ScoreRCR, RankingCalculatorMulti<TournamentScoreRCR, ScoreRCR>> buffer;

	private RankingCalculatorMultiRCRFactory() {
		buffer = new EnumMap<ScoreRCR, RankingCalculatorMulti<TournamentScoreRCR, ScoreRCR>>(ScoreRCR.class);
	}

	@Override
	public RankingCalculatorMulti<TournamentScoreRCR, ScoreRCR> getRankingCalculator(final ScoreRCR scoreType) {
		if (scoreType != null) {
			if (!buffer.containsKey(scoreType)) {
				switch (scoreType) {
					case FINAL_SCORE:
						buffer.put(
							scoreType,
							new RankingCalculatorMultiBase<TournamentScoreRCR, ScoreRCR>(
								ScoreFilterRCRFactory.getScoreFilterAll(),
								ScoreGetterRCRFactory.getScoreGetterRCR(ScoreRCR.FINAL_SCORE),
								null,
								null,
								RankingScoreComparatorFactory.getRankingScoreComparator(
									RankingScoreOrder.DESCENDANT,
									RankingScoreOrder.IGNORE,
									RankingScoreOrder.IGNORE,
									RankingScoreOrder.IGNORE,
									RankingScoreOrder.ASCENDANT
								)
							)
						);
						break;
					case GAME_SCORE:
						buffer.put(
							scoreType,
							new RankingCalculatorMultiBase<TournamentScoreRCR, ScoreRCR>(
								ScoreFilterRCRFactory.getScoreFilterAll(),
								ScoreGetterRCRFactory.getScoreGetterRCR(ScoreRCR.GAME_SCORE),
								ScoreGetterRCRFactory.getScoreGetterRCR(ScoreRCR.FINAL_SCORE),
								null,
								RankingScoreComparatorFactory.getRankingScoreComparator(
									RankingScoreOrder.DESCENDANT,
									RankingScoreOrder.DESCENDANT,
									RankingScoreOrder.IGNORE,
									RankingScoreOrder.IGNORE,
									RankingScoreOrder.ASCENDANT
								)
							)
						);
						break;
					case UMA_SCORE:
						buffer.put(
							scoreType,
							new RankingCalculatorMultiBase<TournamentScoreRCR, ScoreRCR>(
								ScoreFilterRCRFactory.getScoreFilterAll(),
								ScoreGetterRCRFactory.getScoreGetterRCR(ScoreRCR.UMA_SCORE),
								ScoreGetterRCRFactory.getScoreGetterRCR(ScoreRCR.FINAL_SCORE),
								null,
								RankingScoreComparatorFactory.getRankingScoreComparator(
									RankingScoreOrder.DESCENDANT,
									RankingScoreOrder.DESCENDANT,
									RankingScoreOrder.IGNORE,
									RankingScoreOrder.IGNORE,
									RankingScoreOrder.ASCENDANT
								)
							)
						);
						break;
				}
			}
			return buffer.get(scoreType);
		} else {
			return null;
		}
	}
}
