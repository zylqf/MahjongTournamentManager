/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.score.aggregated.sessionscore.score;

import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class SessionScoreTeam implements SessionScore {

	private final SortedSet<SessionScoreIndividual> scores;

	private final String teamName;
	private int score;
	private boolean isUpToDate;

	public SessionScoreTeam(final String teamName) {
		this.teamName = teamName;
		scores = new TreeSet<SessionScoreIndividual>();
	}

	public Set<SessionScoreIndividual> getScores() {
		return scores;
	}

	@Override
	public String getName(final String emptyPlayer) {
		return teamName;
	}

	@Override
	public int getScore() {
		return score;
	}

	public void setScore(final int score) {
		this.score = score;
	}

	public void addScore(final int score) {
		this.score += score;
	}

	@Override
	public boolean isUpToDate() {
		return isUpToDate;
	}

	public void setUpToDate(final boolean isUpToDate) {
		this.isUpToDate = isUpToDate;
	}

	@Override
	public int hashCode() {
		int hash = 1;
		for (final SessionScoreIndividual score : scores) {
			hash = hash * 31 + score.hashCode();
		}
		return hash;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj != null && obj instanceof SessionScoreTeam) {
			return hashCode() == obj.hashCode();
		} else {
			return false;
		}
	}
}
