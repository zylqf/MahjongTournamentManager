/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.score.aggregated.sessionscore.score;

import fr.bri.mj.data.tournament.TournamentPlayer;

public class SessionScoreIndividual implements SessionScore, Comparable<SessionScoreIndividual> {

	private final TournamentPlayer tournamentPlayer;
	private int score;
	private boolean isUpToDate;

	public SessionScoreIndividual(final TournamentPlayer tournamentPlayer) {
		this.tournamentPlayer = tournamentPlayer;
	}

	public TournamentPlayer getTournamentPlayer() {
		return tournamentPlayer;
	}

	@Override
	public String getName(final String emptyPlayer) {
		return tournamentPlayer.getPlayerName(emptyPlayer);
	}

	@Override
	public int getScore() {
		return score;
	}

	public void setScore(final int score) {
		this.score = score;
	}

	public void addScore(final int score) {
		this.score += score;
	}

	@Override
	public boolean isUpToDate() {
		return isUpToDate;
	}

	public void setUpToDate(final boolean isUpToDate) {
		this.isUpToDate = isUpToDate;
	}

	@Override
	public int hashCode() {
		return tournamentPlayer.getTournamentPlayerId();
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj != null && obj instanceof SessionScoreIndividual) {
			return hashCode() == obj.hashCode();
		} else {
			return false;
		}
	}

	@Override
	public int compareTo(final SessionScoreIndividual o) {
		if (o != null) {
			return Integer.compare(
				tournamentPlayer.getTournamentPlayerId(),
				o.tournamentPlayer.getTournamentPlayerId()
			);
		} else {
			return 1;
		}
	}
}
