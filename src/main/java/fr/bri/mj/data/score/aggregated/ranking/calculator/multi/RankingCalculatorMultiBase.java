/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.score.aggregated.ranking.calculator.multi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.bri.mj.data.player.Player;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.RankingScore;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.multi.RankingScoreMulti;
import fr.bri.mj.data.score.raw.filter.ScoreFilter;
import fr.bri.mj.data.score.raw.getter.ScoreGetter;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.data.tournament.TournamentScore;

class RankingCalculatorMultiBase<ScoreClassType extends TournamentScore<ScoreClassType, ScoreEnumType>, ScoreEnumType extends Enum<ScoreEnumType>> implements RankingCalculatorMulti<ScoreClassType, ScoreEnumType> {

	private final ScoreFilter<ScoreClassType, ScoreEnumType> scoreFilter;
	private final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter1;
	private final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter2;
	private final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter3;
	private final Comparator<RankingScore> rankingScoreComparator;

	RankingCalculatorMultiBase(
		final ScoreFilter<ScoreClassType, ScoreEnumType> scoreFilter,
		final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter1,
		final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter2,
		final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter3,
		final Comparator<RankingScore> rankingScoreComparator
	) {
		this.scoreFilter = scoreFilter;
		this.scoreGetter1 = scoreGetter1;
		this.scoreGetter2 = scoreGetter2;
		this.scoreGetter3 = scoreGetter3;
		this.rankingScoreComparator = rankingScoreComparator;
	}

	@Override
	public List<RankingScoreMulti> getRankingScore(
		final Set<Tournament<ScoreClassType, ScoreEnumType>> tournaments,
		final boolean strict
	) {
		final Map<Player, RankingScoreMulti> mapPlayerRanking = new HashMap<Player, RankingScoreMulti>();
		final Map<Player, Integer> mapPlayerParticipation = new HashMap<Player, Integer>();
		for (final Tournament<ScoreClassType, ScoreEnumType> tournament : tournaments) {
			final List<RankingScoreMulti> ranking = calculate(tournament);
			for (int index = 0; index < ranking.size(); index++) {
				final RankingScoreMulti score = ranking.get(index);
				final Player player = score.getPlayer();
				if (!mapPlayerRanking.containsKey(player)) {
					mapPlayerRanking.put(
						player,
						score
					);
					mapPlayerParticipation.put(
						player,
						1
					);
				} else {
					final RankingScoreMulti totalScore = mapPlayerRanking.get(player);
					totalScore.addTotalScore(score.getTotalScore());
					totalScore.addTotalScore2(score.getTotalScore2());
					totalScore.addTotalScore3(score.getTotalScore3());
					totalScore.andUpToDate(score.isUpToDate());
					mapPlayerParticipation.put(
						player,
						mapPlayerParticipation.get(player) + 1
					);
				}
			}
		}

		if (strict) {
			for (final Player player : mapPlayerParticipation.keySet()) {
				if (mapPlayerParticipation.get(player) != tournaments.size()) {
					mapPlayerRanking.remove(player);
				}
			}
		} else {
			for (final Player player : mapPlayerParticipation.keySet()) {
				if (mapPlayerParticipation.get(player) != tournaments.size()) {
					mapPlayerRanking.get(player).setUpToDate(false);
				}
			}
		}

		final List<RankingScoreMulti> totalRanking = new ArrayList<RankingScoreMulti>(mapPlayerRanking.values());
		if (totalRanking.size() > 0) {
			Collections.sort(
				totalRanking,
				rankingScoreComparator
			);

			totalRanking.get(0).setRanking(1);
			for (int index = 1; index < totalRanking.size(); index++) {
				final RankingScoreMulti score = totalRanking.get(index);
				final RankingScoreMulti lastScore = totalRanking.get(index - 1);
				if (rankingScoreComparator.compare(
					score,
					lastScore
				) > 0) {
					score.setRanking(index + 1);
				} else {
					score.setRanking(lastScore.getRanking());
				}
			}
		}

		return totalRanking;
	}

	private List<RankingScoreMulti> calculate(final Tournament<ScoreClassType, ScoreEnumType> tournament) {
		final Map<Player, RankingScoreMulti> mapPlayerToRankingScore = new HashMap<Player, RankingScoreMulti>();
		for (final ScoreClassType table : tournament.getTables()) {
			final TournamentPlayer tournamentPlayer = table.getTournamentPlayer();
			final Player player = tournamentPlayer.getPlayer();
			if (player != null && scoreFilter.filter(table)) {
				if (!mapPlayerToRankingScore.containsKey(player)) {
					final RankingScoreMulti score = new RankingScoreMulti(player);
					score.setUpToDate(true);
					mapPlayerToRankingScore.put(
						player,
						score
					);
				}

				final RankingScoreMulti score = mapPlayerToRankingScore.get(player);
				score.addTotalScore(scoreGetter1.get(table));
				if (scoreGetter2 != null) {
					score.addTotalScore2(scoreGetter2.get(table));
				}
				if (scoreGetter3 != null) {
					score.addTotalScore3(scoreGetter3.get(table));
				}
				score.andUpToDate(table.isScoreRecorded());
			}
		}
		return new ArrayList<RankingScoreMulti>(mapPlayerToRankingScore.values());
	}
}
