/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.score.aggregated.ranking.rankingscore.multi;

import fr.bri.mj.data.player.Player;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.RankingScore;

public class RankingScoreMulti implements RankingScore {

	private final Player player;
	private int ranking;
	private int totalScore;
	private int totalScore2;
	private int totalScore3;
	private int totalScore4;
	private boolean isUpToDate;

	public RankingScoreMulti(final Player player) {
		this.player = player;
	}

	public Player getPlayer() {
		return player;
	}

	@Override
	public String getName(final String emptyPlayer) {
		final StringBuilder name = new StringBuilder();
		name.append(player.getPlayerFirstName());
		name.append(" ");
		name.append(player.getPlayerLastName());
		return name.toString();
	}

	@Override
	public int getRanking() {
		return ranking;
	}

	public void setRanking(final int ranking) {
		this.ranking = ranking;
	}

	@Override
	public int getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(final int totalScore) {
		this.totalScore = totalScore;
	}

	public void addTotalScore(final int totalScore) {
		this.totalScore += totalScore;
	}

	@Override
	public int getTotalScore2() {
		return totalScore2;
	}

	public void setTotalScore2(final int totalScore2) {
		this.totalScore2 = totalScore2;
	}

	public void addTotalScore2(final int totalScore2) {
		this.totalScore2 += totalScore2;
	}

	@Override
	public int getTotalScore3() {
		return totalScore3;
	}

	public void setTotalScore3(final int totalScore3) {
		this.totalScore3 = totalScore3;
	}

	public void addTotalScore3(final int totalScore3) {
		this.totalScore3 += totalScore3;
	}

	@Override
	public int getTotalScore4() {
		return totalScore4;
	}

	public void setTotalScore4(final int totalScore4) {
		this.totalScore4 = totalScore4;
	}

	public void addTotalScore4(final int totalScore4) {
		this.totalScore4 += totalScore4;
	}

	@Override
	public boolean isUpToDate() {
		return isUpToDate;
	}

	public void setUpToDate(final boolean isUpToDate) {
		this.isUpToDate = isUpToDate;
	}

	public void andUpToDate(final boolean isUpToDate) {
		this.isUpToDate = this.isUpToDate && isUpToDate;
	}

	@Override
	public int hashCode() {
		return player.getId();
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj != null && obj instanceof RankingScoreMulti) {
			return hashCode() == obj.hashCode();
		} else {
			return false;
		}
	}
}
