/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.score.aggregated.ranking.calculator.multi;

import java.util.EnumMap;

import fr.bri.mj.data.score.aggregated.ranking.comparator.RankingScoreComparatorFactory;
import fr.bri.mj.data.score.aggregated.ranking.comparator.RankingScoreOrder;
import fr.bri.mj.data.score.raw.filter.ScoreFilterMCRFactory;
import fr.bri.mj.data.score.raw.getter.ScoreGetterMCRFactory;
import fr.bri.mj.data.tournament.mcr.ScoreMCR;
import fr.bri.mj.data.tournament.mcr.TournamentScoreMCR;

public class RankingCalculatorMultiMCRFactory implements RankingCalculatorMultiFactory<TournamentScoreMCR, ScoreMCR> {

	private static RankingCalculatorMultiMCRFactory instance = new RankingCalculatorMultiMCRFactory();

	public static RankingCalculatorMultiMCRFactory getInstance() {
		return instance;
	}

	private final EnumMap<ScoreMCR, RankingCalculatorMulti<TournamentScoreMCR, ScoreMCR>> buffer = new EnumMap<ScoreMCR, RankingCalculatorMulti<TournamentScoreMCR, ScoreMCR>>(ScoreMCR.class);

	@Override
	public RankingCalculatorMulti<TournamentScoreMCR, ScoreMCR> getRankingCalculator(final ScoreMCR mode) {
		if (mode != null) {
			if (!buffer.containsKey(mode)) {
				switch (mode) {
					case MATCH_POINT:
						buffer.put(
							mode,
							new RankingCalculatorMultiBase<TournamentScoreMCR, ScoreMCR>(
								ScoreFilterMCRFactory.getScoreFilterAll(),
								ScoreGetterMCRFactory.getScoreGetterMCR(ScoreMCR.MATCH_POINT),
								ScoreGetterMCRFactory.getScoreGetterMCR(ScoreMCR.TABLE_POINT),
								null,
								RankingScoreComparatorFactory.getRankingScoreComparator(
									RankingScoreOrder.DESCENDANT,
									RankingScoreOrder.DESCENDANT,
									RankingScoreOrder.IGNORE,
									RankingScoreOrder.IGNORE,
									RankingScoreOrder.ASCENDANT
								)
							)
						);
						break;
					case TABLE_POINT:
						buffer.put(
							mode,
							new RankingCalculatorMultiBase<TournamentScoreMCR, ScoreMCR>(
								ScoreFilterMCRFactory.getScoreFilterAll(),
								ScoreGetterMCRFactory.getScoreGetterMCR(ScoreMCR.TABLE_POINT),
								ScoreGetterMCRFactory.getScoreGetterMCR(ScoreMCR.MATCH_POINT),
								null,
								RankingScoreComparatorFactory.getRankingScoreComparator(
									RankingScoreOrder.DESCENDANT,
									RankingScoreOrder.DESCENDANT,
									RankingScoreOrder.IGNORE,
									RankingScoreOrder.IGNORE,
									RankingScoreOrder.ASCENDANT
								)
							)
						);
						break;
				}
			}
			return buffer.get(mode);
		} else {
			return null;
		}
	}
}
