/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.score.aggregated.ranking.calculator.single;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import fr.bri.mj.data.score.aggregated.ranking.rankingscore.RankingScore;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.single.RankingScoreSingleIndividual;
import fr.bri.mj.data.score.aggregated.ranking.rankingscore.single.RankingScoreSingleTeam;
import fr.bri.mj.data.score.raw.filter.ScoreFilter;
import fr.bri.mj.data.score.raw.getter.ScoreGetter;
import fr.bri.mj.data.tournament.Tournament;
import fr.bri.mj.data.tournament.TournamentPlayer;
import fr.bri.mj.data.tournament.TournamentScore;

class RankingCalculatorSingleWithoutQualification<ScoreClassType extends TournamentScore<ScoreClassType, ScoreEnumType>, ScoreEnumType extends Enum<ScoreEnumType>> implements RankingCalculatorSingle<ScoreClassType, ScoreEnumType> {

	private final ScoreFilter<ScoreClassType, ScoreEnumType> scoreFilter;
	private final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter1;
	private final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter2;
	private final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter3;
	private final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter4;
	private final Comparator<RankingScore> rankingScoreComparator;

	RankingCalculatorSingleWithoutQualification(
		final ScoreFilter<ScoreClassType, ScoreEnumType> scoreFilter,
		final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter1,
		final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter2,
		final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter3,
		final ScoreGetter<ScoreClassType, ScoreEnumType> scoreGetter4,
		final Comparator<RankingScore> rankingScoreComparator
	) {
		this.scoreFilter = scoreFilter;
		this.scoreGetter1 = scoreGetter1;
		this.scoreGetter2 = scoreGetter2;
		this.scoreGetter3 = scoreGetter3;
		this.scoreGetter4 = scoreGetter4;
		this.rankingScoreComparator = rankingScoreComparator;
	}

	@Override
	public List<RankingScoreSingleIndividual> getRankingScoreIndividual(
		final Tournament<ScoreClassType, ScoreEnumType> tournament,
		final int session
	) {
		final List<RankingScoreSingleIndividual> ranking = calculate(
			tournament,
			session
		);
		if (ranking.size() > 0) {
			Collections.sort(
				ranking,
				rankingScoreComparator
			);

			ranking.get(0).setRanking(1);
			for (int index = 1; index < ranking.size(); index++) {
				final RankingScoreSingleIndividual score = ranking.get(index);
				final RankingScoreSingleIndividual lastScore = ranking.get(index - 1);
				if (rankingScoreComparator.compare(
					score,
					lastScore
				) > 0) {
					score.setRanking(index + 1);
				} else {
					score.setRanking(lastScore.getRanking());
				}
			}
		}

		return ranking;
	}

	@Override
	public List<RankingScoreSingleTeam> getRankingScoreTeam(
		final Tournament<ScoreClassType, ScoreEnumType> tournament,
		final int session
	) {
		final List<RankingScoreSingleIndividual> rankingIndividual = calculate(
			tournament,
			session
		);
		final Map<String, RankingScoreSingleTeam> mapTeamToRankingScore = new TreeMap<String, RankingScoreSingleTeam>();
		for (int index = 0; index < rankingIndividual.size(); index++) {
			final RankingScoreSingleIndividual scoreIndividual = rankingIndividual.get(index);
			final TournamentPlayer tournamentPlayer = scoreIndividual.getTournamentPlayer();
			final String teamName = tournamentPlayer.getTeamName();
			if (!mapTeamToRankingScore.containsKey(teamName)) {
				final RankingScoreSingleTeam scoreTeam = new RankingScoreSingleTeam(teamName);
				scoreTeam.setUpToDate(true);
				mapTeamToRankingScore.put(
					teamName,
					scoreTeam
				);
			}

			final RankingScoreSingleTeam scoreTeam = mapTeamToRankingScore.get(teamName);
			if (scoreIndividual != null) {
				scoreTeam.getScores().add(scoreIndividual);
				scoreTeam.addTotalScore(scoreIndividual.getTotalScore());
				scoreTeam.addTotalScore2(scoreIndividual.getTotalScore2());
				scoreTeam.addTotalScore3(scoreIndividual.getTotalScore3());
				scoreTeam.addTotalScore4(scoreIndividual.getTotalScore4());
				if (!scoreIndividual.isUpToDate()) {
					scoreTeam.setUpToDate(false);
				}
			}
		}

		final List<RankingScoreSingleTeam> ranking = new ArrayList<RankingScoreSingleTeam>(mapTeamToRankingScore.values());
		Collections.sort(
			ranking,
			rankingScoreComparator
		);

		ranking.get(0).setRanking(1);
		for (int index = 1; index < ranking.size(); index++) {
			final RankingScoreSingleTeam score = ranking.get(index);
			final RankingScoreSingleTeam lastScore = ranking.get(index - 1);
			if (rankingScoreComparator.compare(
				score,
				lastScore
			) > 0) {
				score.setRanking(index + 1);
			} else {
				score.setRanking(lastScore.getRanking());
			}
		}

		return ranking;
	}

	private List<RankingScoreSingleIndividual> calculate(
		final Tournament<ScoreClassType, ScoreEnumType> tournament,
		final int session
	) {
		final Map<TournamentPlayer, RankingScoreSingleIndividual> mapPlayerToRankingScore = new TreeMap<TournamentPlayer, RankingScoreSingleIndividual>();
		for (final ScoreClassType table : tournament.getTables()) {
			if (table.getSessionId() <= session && scoreFilter.filter(table)) {
				final TournamentPlayer tournamentPlayer = table.getTournamentPlayer();
				if (!mapPlayerToRankingScore.containsKey(tournamentPlayer)) {
					final RankingScoreSingleIndividual score = new RankingScoreSingleIndividual(tournamentPlayer);
					score.setUpToDate(true);
					mapPlayerToRankingScore.put(
						tournamentPlayer,
						score
					);
				}
				final RankingScoreSingleIndividual score = mapPlayerToRankingScore.get(tournamentPlayer);
				score.addTotalScore(scoreGetter1.get(table));
				if (scoreGetter2 != null) {
					score.addTotalScore2(scoreGetter2.get(table));
				}
				if (scoreGetter3 != null) {
					score.addTotalScore3(scoreGetter3.get(table));
				}
				if (scoreGetter4 != null) {
					score.addTotalScore4(scoreGetter4.get(table));
				}
				score.andUpToDate(table.isScoreRecorded());
			}
		}
		return new ArrayList<RankingScoreSingleIndividual>(mapPlayerToRankingScore.values());
	}
}
