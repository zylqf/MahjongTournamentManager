/*
 * This file is part of Mahjong Tournament Recorder.
 *
 * Mahjong Tournament Recorder is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Mahjong Tournament Recorder is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * Mahjong Tournament Recorder. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.bri.mj.data.score.aggregated.ranking.comparator;

import java.util.Comparator;

import fr.bri.mj.data.score.aggregated.ranking.rankingscore.RankingScore;

class RankingScoreComparator implements Comparator<RankingScore> {

	private final RankingScoreOrder orderScore1;
	private final RankingScoreOrder orderScore2;
	private final RankingScoreOrder orderScore3;
	private final RankingScoreOrder orderScore4;
	private final RankingScoreOrder orderName;

	RankingScoreComparator(
		final RankingScoreOrder orderScore1,
		final RankingScoreOrder orderScore2,
		final RankingScoreOrder orderScore3,
		final RankingScoreOrder orderScore4,
		final RankingScoreOrder orderName
	) {
		this.orderScore1 = orderScore1;
		this.orderScore2 = orderScore2;
		this.orderScore3 = orderScore3;
		this.orderScore4 = orderScore4;
		this.orderName = orderName;
	}

	@Override
	public int compare(
		final RankingScore o1,
		final RankingScore o2
	) {
		int result = 0;
		if (orderScore1 != RankingScoreOrder.IGNORE) {
			result = Integer.compare(
				o1.getTotalScore(),
				o2.getTotalScore()
			) * orderScore1.getOrder();
		}
		if (result == 0) {
			if (orderScore2 != RankingScoreOrder.IGNORE) {
				result = Integer.compare(
					o1.getTotalScore2(),
					o2.getTotalScore2()
				) * orderScore2.getOrder();
			}
			if (result == 0) {
				if (orderScore3 != RankingScoreOrder.IGNORE) {
					result = Integer.compare(
						o1.getTotalScore3(),
						o2.getTotalScore3()
					) * orderScore3.getOrder();
				}
				if (result == 0) {
					if (orderScore4 != RankingScoreOrder.IGNORE) {
						result = Integer.compare(
							o1.getTotalScore4(),
							o2.getTotalScore4()
						) * orderScore4.getOrder();
					}
					if (result == 0) {
						if (orderName != RankingScoreOrder.IGNORE) {
							result = o1.getName("").compareTo(o1.getName("")) * orderName.getOrder();
						}
					}
				}
			}
		}
		return result;
	}
}
